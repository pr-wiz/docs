# 基于Abp框架后台开发Vue前端
- [对接ABP基本思路](https://www.cnblogs.com/xhznl/p/13508356.html)
- [用户登录&菜单权限](https://www.cnblogs.com/xhznl/p/13517332.html)
- [使用扩展实体](https://www.cnblogs.com/xhznl/p/13529942.html)
- [国际化](https://www.cnblogs.com/xhznl/p/13554571.html)
- [身份认证管理&租户管理](https://www.cnblogs.com/xhznl/p/13566246.html)


- [登录1](https://www.cnblogs.com/william-xu/p/11249800.html)
- [登录2](https://www.cnblogs.com/william-xu/p/11274431.html)
- [管理Identity](https://www.cnblogs.com/william-xu/p/11652115.html)