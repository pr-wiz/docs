# API Host 入门
本教程介绍了如何创建一个新的 ASP.NET Core API Host . 配置并运行它.

## 设置你的开发环境
创建第一个项目之前,需要正确的设置你的开发环境.

### 预先要求
你需要安装以下工具:

- [Visual Studio 2019](https://visualstudio.microsoft.com/vs/) (v16.8+) for Windows / [Visual Studio for Mac](https://visualstudio.microsoft.com/vs/mac/). [^1]

- [.NET Core 5.0+](https://www.microsoft.com/net/download/dotnet-core/)

- [Node v12 or v14](https://nodejs.org/)

- [Yarn v1.20+ (not v2)](https://classic.yarnpkg.com/en/docs/install) [^2] 或 npm v6+ (与Node一起安装)

[^1] 只要支持.NET Core和ASP.NET Core,就可以使用其他编辑器代替Visual Studio. [↩](https://docs.abp.io/en/abp/latest/Getting-Started-Setup-Environment?UI=MVC&DB=EF&Tiered=No#a-editor)

[^2] Yarn v2 的工作方式不同,不受支持. [↩](https://docs.abp.io/en/abp/latest/Getting-Started-Setup-Environment?UI=MVC&DB=EF&Tiered=No#a-yarn)

## 安装ABP CLI
[ABP CLI](https://docs.abp.io/en/abp/latest/CLI)是一个命令行界面，用于自动执行基于 ABP 的解决方案的一些常见任务。

你需要使用以下命令安排ABP CLI:
```
dotnet tool install -g Volo.Abp.Cli
```
如果你已经安装,你可以使用以下命令更新到最新版本:
```
dotnet tool update -g Volo.Abp.Cli
```

## 创建新项目

使用ABP CLI创建一个新项目

> 或者，你可以通过从页面中轻松选择所有选项，从[ABP 框架网站](https://abp.io/get-started)**创建和下载**项目。

使用ABP CLI的 `new` 命令创建新项目:
```
abp new Acme.BookStore --ui none
```
你可以使用不同级别的命令空间; 例如: BookStore, Acme.BookStore 或者 Acme.Retail.BookStore.

## 解决方案结构

### 默认结构
创建项目后你会有以下解决方案目录和文件:
![avatar](images/solution-files-no-ui.png)


在Visual Studio中打开 .sln 文件时,将看到以下解决方案结构:

![avatar](images/vs-app-solution-structure.png)

### 分层结构
如果你创建新项目指定了 `--separate-identity-server` 选项,CLI会创建分层解决方案. 分层结构的目的是`将Identity Server和API host部署到不同的服务器`:
创建项目后你会有以下解决方案目录和文件:
![avatar](images/solution-files-no-ui.png)


在Visual Studio中打开 .sln 文件时,将看到以下解决方案结构:

![avatar](images/vs-app-solution-structure2.png)


> **关于解决方案中的项目**
> 根据你的UI,数据库和其他选项,你的解决方案的结构可能略有不同.

该解决方案具有分层结构(基于[Domain Driven Design](https://docs.abp.io/zh-Hans/abp/latest/Domain-Driven-Design)), 并包含配置好的的单元&集成测试项目.

集成测试项目已配置为可与 `EF Core` & `SQLite` 内存 database同时使用.

> 请参阅[应用程序模板文档](../setup-templates/application.md)详细了解解决方案结构.

## 创建数据库
### 连接字符串
检查 `.HttpApi.Host` 项目下 `appsettings.json` 文件中的 链接字符串:
```
"ConnectionStrings": {
  "Default": "Server=localhost;Database=BookStore;Trusted_Connection=True"
}
```
该解决方案配置为**Entity Framework Core**与**MS SQL Server**一起使用. EF Core支持[各种](https://docs.microsoft.com/en-us/ef/core/providers/)数据库提供程序,因此你可以使用任何受支持的DBMS. 请参阅[Entity Framework集成文档](https://docs.abp.io/en/abp/latest/Entity-Framework-Core)了解如何切换到另一个DBMS.

### 数据库连接字符串
查看`.HttpApi.Host` 项目下 `appsettings.json`文件中的 连接字符串:
```
{
  "ConnectionStrings": {
    "Default": "Server=localhost;Database=BookStore;Trusted_Connection=True"
  }
}
```
解决方案使用 **Entity Framework Core** 和 **MS SQL Server**. EF Core支持[各种](https://docs.microsoft.com/en-us/ef/core/providers/)数据库提供程序,因此你可以根据实际需要使用其他DBMS. 如果需要,请更改连接字符串.

## 应用迁移
该解决方案使用[Entity Framework Core Code First 迁移](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli). 你需要应用迁移来创建数据库,有两种方法迁移数据库.

### 使用DbMigrator应用程序应用迁移
该解决方案包含一个控制台应用程序(在此示例中名为`Acme.BookStore.DbMigrator`),可以创建数据库,应用迁移和初始化数据. 它对开发和生产环境都很有用.

> `.DbMigrator`项目有自己的`appsettings.json`. 因此,如果你更改了上面的连接字符串,则还应更改此字符串.

右键单击`.DbMigrator`项目并选择 **设置为启动项目**:

![avatar](images/set-as-startup-project.png)

按F5(或Ctrl + F5)运行应用程序. 它将具有如下所示的输出:

![avatar](images/db-migrator-app.png)

### 使用EF Core Update-Database命令
Ef Core具有`Update-Database`命令, 可根据需要创建数据库并应用挂起的迁移. 右键单击`.HttpApi.Host`项目并选择**设置为启动项目**:

右键单击`.HttpApi.Host`项目并选择设置为启动项目:

![avatar](images/set-as-startup-project.png)

打开**包管理器控制台(Package Manager Console)**, 选择`.EntityFrameworkCore.DbMigrations`项目作为**默认项目**并运行`Update-Database`命令:

![avatar](images/package-manager-console-update-database.png)

这将基于配置的连接字符串创建新数据库.

> 使用`.DbMigrator`工具是**建议的方法**, 因为它能初始化初始数据能够正确运行Web应用程序.
> 
> 如果你只是使用 `Update-Database` 命令,你会得到一个空数据库,所以你无法登录到应用程序因为数据库中没有初始管理用户. 不需要种子数据库时,可以在开发期间使用 `Update-Database` 命令. 但是使用 `.DbMigrator` 应用程序会更简单,你始终可以使用它来迁移模式并为数据库添加种子.

## 运行应用程序
### 默认结构
由于默认结构应用都托管在`.HttpApi.Host`项目, 确保 `.HttpApi.Host` 是启动项目,运行应用程序后会在你的浏览器打开一个 `swagger` 页面.

> 在Visual Studio中使用**Ctrl+F5**(而不是F5)运行应用,如果你不用于调试,这会减少启动时间.

![avatar](images/bookstore-swagger.png)

### 分层结构

- 启动 `.IdentityServer`
- 启动 `.HttpApi.Host`

## 移动开发
当你创建一个新的应用程序时. 可以添加`-m react-native`选项以在解决方案中包含[ react-native](https://reactnative.dev/)项目. 这是一个基础的React Native启动模板,用于开发与基于ABP的后端集成的移动应用程序.

请参阅"[React Native入门](https://docs.abp.io/zh-Hans/abp/latest/Getting-Started-React-Native)"文档了解如何配置和运行React Native应用程序.

## 下一步是什么?
- [HttpApi.Host应用程序开发教程](tutorials/readme.md)