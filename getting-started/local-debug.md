# 前后端本地联调后端配置
## 场景
1. 前端开发运行环境
    - IP: 192.168.20.50
    - 应用端口: 8080
1. 后端开发运行环境
    - IP: 192.168.88.4
    - 身份认证服务端口: 44304
    - 模块业务服务端口: 44342
## 后端默认运行
- 运行方式
    1. 身份认证和模块业务都以 `https` 方式运行
    1. 主机地址使用 `localhost`
- 问题
    1. 默认运行方式偏本机运行, 当前端客户端调用服务时, 就会存在以下问题:
        - `https` 证书问题, 目录开发证书为自签发, 调用时需要配置证书信任
        - `localhost` 问题, 没有使用明确的主机, 导致只能在本机使用
## 后端配置调整
1. 身份认证和模块业务调整为以 `http` 方式运行
1. 主机地址使用 本地域名 访问(在前端和后端开发机器 `C:\Windows\System32\drivers\etc\hosts` 文件中都加上以下域名解析)
    - 后端开发主机 `192.168.88.4` => `devapihost`
    - 前端开发主机 `192.168.20.50` => `devuihost`

1. 增加 Portal_Js 客户端配置, 此客户端指的是 前端应用客户端
    - 在 `IdentityServer` 项目的 `appsettings.json` 的 `IdentityServer.Clients` 节点下添加
        ```
        "Portal_Js": {
            "ClientId": "Portal_Js",
            "ClientSecret": "1q2w3e*",
            "RootUrl": "http://devuihost:8080",
            "RedirectUri": "http://devuihost:8080/callBack.html",
            "PostLogoutRedirectUri": "http://devuihost:8080/#/"
        }
        ```
        - ClientId 固定为 `Portal_Js`
        - ClientSecret 固定为 `1q2w3e*`
        - RootUrl 前端应用根路径
        - RedirectUri 登录成功后跳转到前端的地址
        - PostLogoutRedirectUri 退出成功后跳转到前端的地址
    - 在 `IdentityServer` 项目的基础数据类 `IdentityServerDataSeedContributor` 中实现 `Portal_Js` 支持
1. 调整 `IdentityServer` 和 `HttpApi.Host` 项目启动方式
    - 将项目 `Properties/launchSettings.json` 文件中的 `https://localhost` 替换为 `http://devapihost`
    - 将项目 `appsettings.json` 文件中的 `https://localhost` 替换为 `http://devapihost`
    - 确认项目 `appsettings.json` 文件 `App.CorsOrigins` 中包含 前端客户端RootUrl `http://devuihost:8080`

## 启动方式
### 后端
1. 以非调试模式[CTRL+F5] 运行 `IdentityServer` (非iisexpress模式)
1. 以非调试模式[CTRL+F5] 运行 `HttpApi.Host` (非iisexpress模式)
### 前端
1. 配置 url中的地址都以域名代替
1. 本地以域名方式(http://devuihost:8080) 启动应用