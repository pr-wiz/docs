# 入门
本章介绍如何创建应用程序
- [开发环境准备](dev-env.md)
- [no-ui-webapp](no-ui-webapp.md): APIHost应用程序模板.
- [console](../setup-templates/console.md): 控制台模板.
- [WPF](../setup-templates/wpf.md): WPF模板.
- [empty-webapp](empty-webapp.md): Web应用程序模板.