# 开发环境
## 工具
1. `VS2022`
    - 内网安装
        - 新安装
            1. 直接通过共享目录安装 `\\fs01.wiz.top\share01\tools\dotnet\vs_layout\2022`
            1. 安装证书
                安装 `certificates`目录下的3个证书(双击安装）
            1. 安装 vs2022
                双击 `vs_enterprise.exe`，按提示安装
        - 更新
            1. 删除原安装
            1. 参考新安装
1. `git`
    - 内网安装
        1. 共享目录 `\\fs01.wiz.top\share01\tools\git`
        1. 安装 `Git-2.33.0.2-64-bit.exe`
        1. 安装 `TortoiseGit-2.12.0.0-64bit.msi`
        1. 安装 `TortoiseGit-LanguagePack-2.12.0.0-64bit-zh_CN.msi`

1. `redis`
    1. 安装包地址: `\\fs01.wiz.top\share01\tools\Redis-x64-5.0.14.1.zip`
    1. 解压即可使用

1. `谷歌浏览器 v98`
    - 需要安装新版 谷歌浏览器
    - 安装包地址：`\\fs01.wiz.top\share01\tools\browser`
1. `ABP CLI`
    - 安装方法
        ```
        dotnet tool install -g Volo.Abp.Cli
        ```
    - 升级方法
        ```
        dotnet tool update -g Volo.Abp.Cli
        ```

1. `AbpHelper`
    - 安装方法
        ```
        dotnet tool install EasyAbp.AbpHelper -g
        ```
    - 升级方法
        ```
        dotnet tool update EasyAbp.AbpHelper -g
        ```
        - 内网
            ```
            dotnet tool uninstall EasyAbp.AbpHelper -g
            dotnet tool install EasyAbp.AbpHelper -g
            ```

1. Redis
    - 分布式部署时需要(模块开发时需要)

1. MySql

## 配置
1. `Nuget` 配置
    - 新增或将 **%AppData%\NuGet\NuGet.Config** 文件的内容替换为:
        ```
        <?xml version="1.0" encoding="utf-8"?>
        <configuration>
        <packageSources>    
            <clear />
            <add key="公司内部源" value="https://nexus.wiz.top/repository/nuget-v3-group/index.json" />    
        </packageSources>  
        </configuration>
        ```
1. 配置 setProfileEnvironment 为 true
    1. 打开  %windir%/system32/inetsrv/config 文件夹
    1. 打开 applicationHost.config 文件
    1. 查找 `<system.applicationHost><applicationPools><applicationPoolDefaults><processModel>` 元素
    1. 删除 setProfileEnvironment 属性将属性的值显式设置为 true。
        ```
        <applicationPools>				
            <applicationPoolDefaults>
                <processModel identityType="ApplicationPoolIdentity" loadUserProfile="true" setProfileEnvironment="true" />
            </applicationPoolDefaults>
        </applicationPools>
        ```

1. 开发机器域名配置
    1. 后端
        - 后端开发机器的域名配置为 devapihost, 不要使用 localhost
            - 用管理员身份打开文件 C:\Windows\System32\drivers\etc\hosts
            - 配置一条 域名解析记录, 其中 127.0.0.1 为本机IP地址
                ```
                127.0.0.1 devapihost
                ```        
        - 启动应用以域名方式 devapihost:xxxx 形式启动
        - 模块中快速更改为 域名 启动的方法
            - 在 vs 中 CTRL-SHIFT-H
            - 查找 https://localhost
            - 替换为 http://devapihost
            - 重新编译启动
    1. 前端
        - 前端开发机器域名配置为 devuihost, 不要使用 localhost
            - 用管理员身份打开文件 C:\Windows\System32\drivers\etc\hosts
            - 配置一条 域名解析记录, 其中 127.0.0.1 为本机IP地址
                ```
                127.0.0.1 devuihost
                ```
            - 当需要直接调用后端开发人员电脑进行联调时, 还需要配置一条 devapihost 的解析
                ```
                192.168.20.21 devapihost
                ```
                - 192.168.20.21 为对应后端开发人员电脑的IP地址