# API Host应用程序开发教程 - 第五章: 领域层

## 介绍
在前面的部分中，我们已经使用 ABP 基础架构轻松构建了一些服务；

- 使用[CrudAppService](https://docs.abp.io/en/abp/latest/Application-Services)基类，而不是手动开发应用程序服务标准的创建、读取、更新和删除操作.
- 使用通用存储库来完全自动化数据库层.

关于Authors:

- 我们将**手动完成一些操作**，以展示您在需要时如何进行操作.
- 我们将实施一些**领域驱动设计 (DDD) 最佳实践**.

> 开发将逐层进行，以一次集中在单个层上.在实际项目中，您将按功能（垂直）开发应用程序功能，如前几部分所述.通过这种方式，您将体验两种方法.

## Author 实体
在`Acme.BookStore.Domain`项目中创建一个文件夹（命名空间）`Authors`, 并在其中添加一个`Author`类：
```
using System;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Entities.Auditing;

namespace Acme.BookStore.Authors
{
    public class Author : FullAuditedAggregateRoot<Guid>
    {
        public string Name { get; private set; }
        public DateTime BirthDate { get; set; }
        public string ShortBio { get; set; }

        private Author()
        {
            /* This constructor is for deserialization / ORM purpose */
        }

        internal Author(
            Guid id,
            [NotNull] string name,
            DateTime birthDate,
            [CanBeNull] string shortBio = null)
            : base(id)
        {
            SetName(name);
            BirthDate = birthDate;
            ShortBio = shortBio;
        }

        internal Author ChangeName([NotNull] string name)
        {
            SetName(name);
            return this;
        }

        private void SetName([NotNull] string name)
        {
            Name = Check.NotNullOrWhiteSpace(
                name, 
                nameof(name), 
                maxLength: AuthorConsts.MaxNameLength
            );
        }
    }
}
```
- 继承自`FullAuditedAggregateRoot<Guid>`, 使实体实现软删除（这意味着当您删除它时，它不会在数据库中删除，而只是标记为已删除）并具有所有[审计](https://docs.abp.io/en/abp/latest/Entities)属性.
- `private set` 限制只能从这个类中设置Name这个属性.有两种设置名称的方法（在这两种情况下，我们都验证名称）：
    - 在构造函数中，同时创建一个新Author.
    - 使用ChangeName方法更新名称.
- 构造函数和`ChangeName`方法都是`internal`, 仅能在领域层使用这些方法，将后面说明使用AuthorManager.
- `Check`类 是一个 ABP 框架实用程序类，可帮助您检查方法参数（在无效情况下它会引发ArgumentException）.

`AuthorConsts`是一个简单的类，位于项目`Acme.BookStore.Domain.Shared`的`Authors`命名空间（文件夹）下：
```
namespace Acme.BookStore.Authors
{
    public static class AuthorConsts
    {
        public const int MaxNameLength = 64;
    }
}
```
在`Acme.BookStore.Domain.Shared`项目中创建了这个类，因为我们稍后将在[数据传输对象](https://docs.abp.io/en/abp/latest/Data-Transfer-Objects)(DTO)上重用它.

## AuthorManager：领域服务
`Author`构造函数和`ChangeName`方法是`internal`，因此它们只能在领域层中使用. 在项目Acme.BookStore.Domain的Authors文件夹（命名空间）中创建类`AuthorManager`：
```
using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Services;

namespace Acme.BookStore.Authors
{
    public class AuthorManager : DomainService
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorManager(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public async Task<Author> CreateAsync(
            [NotNull] string name,
            DateTime birthDate,
            [CanBeNull] string shortBio = null)
        {
            Check.NotNullOrWhiteSpace(name, nameof(name));

            var existingAuthor = await _authorRepository.FindByNameAsync(name);
            if (existingAuthor != null)
            {
                throw new AuthorAlreadyExistsException(name);
            }

            return new Author(
                GuidGenerator.Create(),
                name,
                birthDate,
                shortBio
            );
        }

        public async Task ChangeNameAsync(
            [NotNull] Author author,
            [NotNull] string newName)
        {
            Check.NotNull(author, nameof(author));
            Check.NotNullOrWhiteSpace(newName, nameof(newName));

            var existingAuthor = await _authorRepository.FindByNameAsync(newName);
            if (existingAuthor != null && existingAuthor.Id != author.Id)
            {
                throw new AuthorAlreadyExistsException(newName);
            }

            author.ChangeName(newName);
        }
    }
}
```
`AuthorManager`强制以受控方式创建 author 和更改 author 姓名. 应用层（后面会介绍）会用到这些方法.

> DDD 提示：除非确实需要并执行一些核心业务规则，否则**不要引入域服务方法**. 对于上面的列子，我们需要此服务能够强制唯一名称约束.

两种方法都会检查是否已经存在具有给定名称的作者并抛出一个特殊的业务异常，在Acme.BookStore.Domain项目（Authors文件夹中）中定义异常类`AuthorAlreadyExistsException`，如下所示：
```
using Volo.Abp;

namespace Acme.BookStore.Authors
{
    public class AuthorAlreadyExistsException : BusinessException
    {
        public AuthorAlreadyExistsException(string name)
            : base(BookStoreDomainErrorCodes.AuthorAlreadyExists)
        {
            WithData("name", name);
        }
    }
}
```
`BusinessException`是一种特殊的异常类型.在需要时抛出领域相关的异常是一个很好的做法.它由 ABP 框架自动处理，并且可以轻松本地化. `WithData(...)`方法用于向异常对象提供附加数据，这些数据稍后将用于本地化消息或用于其他目的.

在`Acme.BookStore.Domain.Shared`项目中打开`BookStoreDomainErrorCodes`，修改如下：
```
namespace Acme.BookStore
{
    public static class BookStoreDomainErrorCodes
    {
        public const string AuthorAlreadyExists = "BookStore:00001";
    }
}
```
应用程序抛出的错误代码用一个唯一的字符串来代表，可由客户端应用程序处理.对于用户，您可能希望对其进行本地化. 打开项目`Acme.BookStore.Domain.Shared`的`Localization/BookStore/en.json`并添加以下条目：
```
"BookStore:00001": "There is already an author with the same name: {name}"
```
无论何时抛出`AuthorAlreadyExistsException`，最终用户都会在 UI 上看到一条有意义的错误消息.

### IAuthorRepository
`AuthorManager`注入了`IAuthorRepository`，因此我们需要定义它. 在项目`Acme.BookStore.Domain`的Authors文件夹（命名空间）中创建这个新接口：
```
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore.Authors
{
    public interface IAuthorRepository : IRepository<Author, Guid>
    {
        Task<Author> FindByNameAsync(string name);

        Task<List<Author>> GetListAsync(
            int skipCount,
            int maxResultCount,
            string sorting,
            string filter = null
        );
    }
}
```
- `IAuthorRepository`扩展了标准`IRepository<Author, Guid>`接口，因此所有标准存储库方法也可用于`IAuthorRepository`.
- `FindByNameAsync`用于`AuthorManager`按姓名查询作者.
- `GetListAsync` 将在应用程序层中用于获取已排序和过滤的作者列表显示在 UI 上.

我们将在下一部分实现这个存储库.

> 这两种方法**似乎都没有必要**，因为标准存储库已经存在`IQueryable`，您可以直接使用它们而不是定义此类自定义方法.你是对的，就像在真正的应用程序中一样.但是，对于这个**“学习”教程**，解释如何在您真正需要时创建自定义存储库方法很有用.

## 结论
这部分涵盖了书店应用程序authors功能的领域层.在此部分中创建/更新的主要文件在下图中突出显示：

![avatar](images/bookstore-author-domain-layer.png)

## 下一章
查看本教程的[下一章](no-ui-api-host-part6.md).