# NO-UI 模块开发教程

## 创建解决方案
创建名为 `Wiz.ModuleDemo` 的新解决方案.
```
abp new Wiz.ModuleDemo -t module --no-ui -v 4.4.4
```
- 公司内网不支持使用 abp cli, 如需要在内网使用, 请在外网创建解决方案再复制到内网。

## 创建Person实体
启动模板中的**领域层**分为两个项目:

- `Wiz.ModuleDemo.Domain`包含你的[实体](https://docs.abp.io/zh-Hans/abp/latest/Entities), [领域服务](https://docs.abp.io/zh-Hans/abp/latest/Domain-Services)和其他核心域对象.
- `Wiz.ModuleDemo.Domain.Shared`包含可与客户共享的常量,枚举或其他域相关对象.
在解决方案的领域层(`Wiz.ModuleDemo.Domain`项目)中定义你的实体.

该应用程序的主要实体是`Person`. 在`Wiz.ModuleDemo.Domain`项目中创建一个 `Persons` 文件夹并在其中添加一个名为 `Person` 的类,如下所示:
```
public class Person : FullAuditedAggregateRoot<Guid>, IMultiTenant
{
    public virtual string Name { get; set; }

    public virtual Gender Gender { get; set; }

    public virtual DateTime Birthday { get; set; }

    public virtual float Height { get; set; }

    public virtual Guid? TenantId { get; }
}

```
- ABP为实体提供了两个基本的基类: `AggregateRoot`和`Entity`. **Aggregate Root**是[领域驱动设计](https://docs.abp.io/zh-Hans/abp/latest/Tutorials/Domain-Driven-Design) 概念之一. 可以视为直接查询和处理的根实体(请参阅[实体文档](https://docs.abp.io/zh-Hans/abp/latest/Entities)).
- `Person`实体继承了`FullAuditedAggregateRoot`, `FullAuditedAggregateRoot`类在`AggregateRoot`类的基础上添加了完整的审计属性(`CreationTime`, `CreatorId`, `LastModificationTime`,`LastModifierId`,`DeletionTime`,`DeleterId`,`IsDeleted` 等). ABP框架自动为你管理这些属性.
- `Person`实体 实现了 `IMultiTenant` 接口, 为多租户实体, 具有 `TenantId` 属性
- `Guid`是`Person`实体的主键类型.

> 为了保持简单,本教程将实体属性保留为 **public get/set** . 如果你想了解关于DDD最佳实践,请参阅[实体文档](https://docs.abp.io/zh-Hans/abp/latest/Entities).

## PersonType枚举
上面所用到了 `Gender` 枚举,在 `Wiz.ModuleDemo.Domain.Shared` 项目创建 `Gender`.
```
public enum Gender
{
    Male,
    Famale
}
```
最终的文件夹/文件结构应该如下所示:

![avatar](images/Personstore-Person-and-Persontype.png)

## 创建各层基础CRUD实现
- 参考 [自动生成CRUD代码教程](no-ui-crud-gen.md)

## 生成EF迁移
### 概述
无界面模块开发的服务是以微服务的方式运行的, 运行了两个Host:
- `*.IdentityServer` 身份认证及公共管理服务
- `*.HttpApi.Host` 模块业务服务

这两个项目都有供 EF迁移用的 `EntityFrameworkCore`, 迁移主要在此两个项目的 `EntityFrameworkCore` 中进行

### 数据库配置
#### *.IdentityServer
1. 配置数据库连接
    - 配置 Default 链接为实际的身份认证及公共管理服务数据库连接地址
1. 切换 `DBMS` (可选)
    - 默认为 `SqlServer`, 如需要切换为其他数据库 则需要做切换调整(仅限当前项目), 参考 [切换为MySql](https://docs.abp.io/zh-Hans/abp/latest/Entity-Framework-Core-MySQL) 

#### *.HttpApi.Host
1. 配置数据库连接
    - 配置 `Default` 链接为实际的身份认证及公共管理服务数据库连接信息
    - 配置 **模块关键字** 链接为模块业务服务数据库连接信息
1. 切换 `DBMS` (可选)
    - 默认为 `SqlServer`, 如需要切换为其他数据库 则需要做切换调整(仅限当前项目), 参考 [切换为MySql](https://docs.abp.io/zh-Hans/abp/latest/Entity-Framework-Core-MySQL) 

### EF迁移(vs2019的**PMC**中操作)
#### *.IdentityServer
1. 切换 `*.IdentityServer` 项目为启动项目
1. 设置 `*.IdentityServer` 为 **PMC** 的当前项目
1. 生成迁移信息(默认已生成)
    - 在 **PMC** 中执行以下脚本
        `Add-Migration Initial -Context IdentityServerHostMigrationsDbContext`
1. 更新迁移信息到数据库
    - 在 **PMC** 中执行以下脚本
        `Update-Database -Context IdentityServerHostMigrationsDbContext`
#### *.HttpApi.Host
1. 切换 `*.HttpApi.Host` 项目为启动项目
1. 设置 `*.HttpApi.Host` 为 PMC 的当前项目
1. 生成迁移信息
    - 在 **PMC** 中执行以下脚本
        `Add-Migration Initial -Context **HttpApiHostMigrationsDbContext`
1. 更新迁移信息到数据库
    - 在 **PMC** 中执行以下脚本
        `Update-Database -Context **HttpApiHostMigrationsDbContext`

注:
- `Add-Migration Initial **` 中的`Initial`是一次迁移标识名称, 可以为任意有效字符, 但一般应约定为:
    - `Initial` 表示初始化EF迁移, 一般用于本模块还没有创建实体(表), 迁移引用模块的实体用
    - `Create_EntityName` 新增 实体后
    - `Update_EntityName` 更新实体后
    - `Update_YYYYMMDD` 某个日期创建
- 每次创建迁移后应及时更新迁移信息到数据库
- 应由专人负责

## 启动应用(开发阶段)
### 前提条件
1. 已创建数据并正确更新了EF迁移到数据库
1. 已在本地启动了 **Redis Server**

### 启动应用
按以下顺序启动应用:
1. 启动 `*.IdentityServer` (**CTLR + F5**)
1. 启动 `*.HttpApi.Host` (**CTLR + F5**)

### 服务端点不存在
- 没有配置自动API控制器
- 配置自动生成 API Controller: 在应用的Host项目的的模块类(**Module)的ConfigureServices方法中加上
    ```
    Configure<AbpAspNetCoreMvcOptions>(options =>
    {
        options.ConventionalControllers.Create(typeof(XXXApplicationModule).Assembly);
    });
    ```

## 添加实体关联 
1. 添加资格证书类
```
public class Certificate : FullAuditedEntity<Guid>
{
    public virtual string CertNo { get; set; }
    public virtual DateTime IssueDate { get; set; }

    public virtual Guid PersonId { get; set; }
}
```
1. 修改 Person 类
    - 加上 资格证书列表 属性
        ```
        public virtual List<Certificate> Certificates { get; protected set; }
        ```
    - 构造方法中实例化列表
        ```
        Certificates = new List<Certificate>();
        ```
1. 生成实体CRUD代码
    - 参考 [自动生成CRUD代码教程](no-ui-crud-gen.md)
    - `abphelper gen crud --skip-db-migrations --skip-ui --skip-view-model --skip-custom-repository --skip-test --no-overwrite Certificate`






