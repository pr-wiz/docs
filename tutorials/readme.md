# 创建项目教程
## 创建API Host
在本系列教程中, 你将构建一个名为 `Acme.BookStore` 的用于管理书籍及其作者列表的基于ABP的API Host程序. 它是使用以下技术开发的:

- **Entity Framework Core** 做为ORM提供程序.
- **vuejs** 做为前端框架

本教程分为以下部分:

[Part 1](no-ui-api-host-part1.md): 创建服务端
[Part 2](no-ui-api-host-part2.md): 动态JavaScript代理和本地化
[Part 3](no-ui-api-host-part3.md): 集成测试
[Part 4](no-ui-api-host-part4.md): 授权
[Part 5](no-ui-api-host-part5.md): 领域层
[Part 6](no-ui-api-host-part6.md): 数据库集成
[Part 7](no-ui-api-host-part7.md): 应用服务层
[Part 8](no-ui-api-host-part8.md): 用户页面

## 创建模块(no-ui)
[创建无界面模块](no-ui-module.md)

## 自动生成代码
- [生成CRUD代码](no-ui-crud-gen.md)