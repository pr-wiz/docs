# API Host应用程序开发教程 - 第七章: 应用程序层

## 介绍
这章说明为之前创建的Author实体创建一个应用程序层.

## IAuthorAppService
我们将首先创建[应用服务](https://docs.abp.io/en/abp/latest/Application-Services)接口和相关的[DTO](https://docs.abp.io/en/abp/latest/Data-Transfer-Objects). 在项目`Acme.BookStore.Application.Contracts`的`Authors`命名空间（文件夹）中创建一个名为`IAuthorAppService` 的新接口：
```
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Acme.BookStore.Authors
{
    public interface IAuthorAppService : IApplicationService
    {
        Task<AuthorDto> GetAsync(Guid id);

        Task<PagedResultDto<AuthorDto>> GetListAsync(GetAuthorListDto input);

        Task<AuthorDto> CreateAsync(CreateAuthorDto input);

        Task UpdateAsync(Guid id, UpdateAuthorDto input);

        Task DeleteAsync(Guid id);
    }
}
```
- `IApplicationService` 是所有应用服务都继承的约定俗成的接口，ABP框架可以识别此服务.
- 定义了对`Author`实体执行 **CRUD** 操作的标准方法.
- `PagedResultDto`是 ABP 框架中预定义的 DTO 类.它有一个`Items`集合和一个`TotalCount`的属性, 用于返回分页结果.
- 首选从`CreateAsync`方法返回`AuthorDto`（对于新创建的Author），此应用程序不使用它 - 只是为了显示不同的用法.

此接口使用下面定义的 DTO（在您的项目创建它们）.

### AuthorDto
```
using System;
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.Authors
{
    public class AuthorDto : EntityDto<Guid>
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }

        public string ShortBio { get; set; }
    }
}
```
- `EntityDto<T>`仅具有给定泛型参数的`Id`属性. 如果不是继承`EntityDto<T>`.您可以自己创建一个`Id`属性，

### GetAuthorListDto
```
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.Authors
{
    public class GetAuthorListDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
```
- `Filter`用于搜索authors.可以设置为`null`（或空字符串）以获取所有作者.
- `PagedAndSortedResultRequestDto`具有标准的分页和排序属性：`int MaxResultCount`,`int SkipCount`和`string Sorting`.

> ABP 框架有这样的基本 DTO 类来简化和标准化你的 DTO.请参阅[DTO 文档](https://docs.abp.io/en/abp/latest/Data-Transfer-Objects)了解所有信息.

### CreateAuthorDto
```
using System;
using System.ComponentModel.DataAnnotations;

namespace Acme.BookStore.Authors
{
    public class CreateAuthorDto
    {
        [Required]
        [StringLength(AuthorConsts.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
        
        public string ShortBio { get; set; }
    }
}
```
**Data annotation**属性可用于验证 DTO.有关详细信息，请参阅[验证文档](https://docs.abp.io/en/abp/latest/Validation).

### UpdateAuthorDto
```
using System;
using System.ComponentModel.DataAnnotations;

namespace Acme.BookStore.Authors
{
    public class UpdateAuthorDto
    {
        [Required]
        [StringLength(AuthorConsts.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
        
        public string ShortBio { get; set; }
    }
}
```

> 我们可以在创建和更新操作之间共享（重用）相同的 DTO.虽然可以这么做，但我们更喜欢为这些操作创建不同的 DTO，因为我们看到它们通常随着时间的推移它们会变更不一样.因此，与紧耦合设计相比，这里的代码重复是合理的.

## AuthorAppService
是时候实现`IAuthorAppService`接口了.在项目Acme.BookStore.Application的Authors命名空间（文件夹）中创建一个名为AuthorAppService类：
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acme.BookStore.Permissions;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore.Authors
{
    [Authorize(BookStorePermissions.Authors.Default)]
    public class AuthorAppService : BookStoreAppService, IAuthorAppService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly AuthorManager _authorManager;

        public AuthorAppService(
            IAuthorRepository authorRepository,
            AuthorManager authorManager)
        {
            _authorRepository = authorRepository;
            _authorManager = authorManager;
        }

        //...SERVICE METHODS WILL COME HERE...
    }
}
```
- [Authorize(BookStorePermissions.Authors.Default)]是一种声明性方式检查当前用户授权的权限（策略）.详见[授权文档](https://docs.abp.io/en/abp/latest/Authorization).
- 派生自`BookStoreAppService`，它是启动模板附带的一个简单基类.它派生自标准`ApplicationService`类.
- 实现了上面定义的`IAuthorAppService`.
- 注入`IAuthorRepository`和`AuthorManager`以在服务方法中使用.

现在，我们将一一介绍服务方法. 请将解释的方法复制到`AuthorAppService`类中.

### GetAsync
```
public async Task<AuthorDto> GetAsync(Guid id)
{
    var author = await _authorRepository.GetAsync(id);
    return ObjectMapper.Map<Author, AuthorDto>(author);
}
```
此方法只是通过实体`Id`获取`Author`实体，使用[对象到对象映射器](https://docs.abp.io/en/abp/latest/Object-To-Object-Mapping)转换为`AuthorDto`.这需要配置`AutoMapper`，后面会解释.

### GetListAsync
```
public async Task<PagedResultDto<AuthorDto>> GetListAsync(GetAuthorListDto input)
{
    if (input.Sorting.IsNullOrWhiteSpace())
    {
        input.Sorting = nameof(Author.Name);
    }

    var authors = await _authorRepository.GetListAsync(
        input.SkipCount,
        input.MaxResultCount,
        input.Sorting,
        input.Filter
    );

    var totalCount = input.Filter == null
        ? await _authorRepository.CountAsync()
        : await _authorRepository.CountAsync(
            author => author.Name.Contains(input.Filter));

    return new PagedResultDto<AuthorDto>(
        totalCount,
        ObjectMapper.Map<List<Author>, List<AuthorDto>>(authors)
    );
}
```
- 默认排序是“by author name”，它在方法的开头完成，以防它不是由客户端发送的.
- `IAuthorRepository.GetListAsync`用于从数据库中获取分页、排序和过滤的作者列表.我们已经在本教程的前一章中实现了它.同样，实际上不需要创建这样的方法，因为我们可以直接查询存储库，但此处用于演示如何创建自定义存储库方法.
- 直接从`AuthorRepository`获取author计数的同时查询author. 如果传入了过滤器，那么将在获取计数时使用它来过滤实体.
- 最后，返回将`Authors`列表转换为`AuthorDto`列表分页结果.

### CreateAsync
```
[Authorize(BookStorePermissions.Authors.Create)]
public async Task<AuthorDto> CreateAsync(CreateAuthorDto input)
{
    var author = await _authorManager.CreateAsync(
        input.Name,
        input.BirthDate,
        input.ShortBio
    );

    await _authorRepository.InsertAsync(author);

    return ObjectMapper.Map<Author, AuthorDto>(author);
}
```
- `CreateAsync`需要`BookStorePermissions.Authors.Create`权限.
- 使用`AuthorManager`（领域服务）创建新Author.
- 用于`IAuthorRepository.InsertAsync`将新Author插入到数据库中.
- 使用`ObjectMapper`返回一个AuthorDto代表新创建的Author.

> DDD 提示：一些开发人员可能会发现将新实体插入到_authorManager.CreateAsync. 我们认为将它留给应用层是一个更好的设计，因为它更好地知道什么时候将它插入到数据库中（也许它需要在插入之前对实体进行额外的工作，如果我们在域服务）.但是，这完全取决于您.

### UpdateAsync
```
[Authorize(BookStorePermissions.Authors.Edit)]
public async Task UpdateAsync(Guid id, UpdateAuthorDto input)
{
    var author = await _authorRepository.GetAsync(id);

    if (author.Name != input.Name)
    {
        await _authorManager.ChangeNameAsync(author, input.Name);
    }

    author.BirthDate = input.BirthDate;
    author.ShortBio = input.ShortBio;

    await _authorRepository.UpdateAsync(author);
}
```

- `UpdateAsync`需要额外的`BookStorePermissions.Authors.Edit`权限.
- 用于`IAuthorRepository.GetAsync`从数据库中获取Author实体.如果不存在给定 `id` 的Author，则`GetAsync`抛出`EntityNotFoundException`异常，这会导致Web 应用程序中的HTTP 404状态代码.始终先获取实体进行更新操作是一种很好的做法.
- 如果客户端要求更改Author姓名，则使用`AuthorManager.ChangeNameAsync`（领域服务方法）更改Author姓名.
- 直接更新BirthDate并且ShortBio, 因为没有任何业务规则来更改这些属性，它们接受任何值.
- 最后，调用`IAuthorRepository.UpdateAsync`方法更新数据库上的实体.

> **EF Core 提示**：Entity Framework Core 具有**change tracking**系统，并在工作单元结束时**automatically saves**对实体的任何更改（您可以简单地认为 ABP 框架会在方法结束时自动调用`SaveChanges`）.因此，即使您没有在方法的末尾调用`_authorRepository.UpdateAsync(...)`，也会按预期工作.如果您以后不考虑更改 EF Core，则可以删除此行.

### DeleteAsync
```
[Authorize(BookStorePermissions.Authors.Delete)]
public async Task DeleteAsync(Guid id)
{
    await _authorRepository.DeleteAsync(id);
}
```
- `DeleteAsync`需要额外的`BookStorePermissions.Authors.Delete`权限.
- 它只是简单地使用存储库的`DeleteAsync`方法.


## 权限定义
此时您还无法编译代码，因为它需要在`BookStorePermissions`类中声明一些常量.

打开项目`Acme.BookStore.Application.Contracts`（在`Permissions`文件夹中）里面的类`BookStorePermissions`，修改内容如下：
```
namespace Acme.BookStore.Permissions
{
    public static class BookStorePermissions
    {
        public const string GroupName = "BookStore";

        public static class Books
        {
            public const string Default = GroupName + ".Books";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }
        
        // *** ADDED a NEW NESTED CLASS ***
        public static class Authors
        {
            public const string Default = GroupName + ".Authors";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }
    }
}
```

然后在同一个项目中打开`BookStorePermissionDefinitionProvider` 并在`Define`方法的末尾添加以下几行：
```
var authorsPermission = bookStoreGroup.AddPermission(
    BookStorePermissions.Authors.Default, L("Permission:Authors"));

authorsPermission.AddChild(
    BookStorePermissions.Authors.Create, L("Permission:Authors.Create"));

authorsPermission.AddChild(
    BookStorePermissions.Authors.Edit, L("Permission:Authors.Edit"));

authorsPermission.AddChild(
    BookStorePermissions.Authors.Delete, L("Permission:Authors.Delete"));
```

最后，将以下条目添加到项目`Acme.BookStore.Domain.Shared`的`Localization/BookStore/en.json`，以本地化权限名称：
```
"Permission:Authors": "Author Management",
"Permission:Authors.Create": "Creating new authors",
"Permission:Authors.Edit": "Editing the authors",
"Permission:Authors.Delete": "Deleting the authors"
```

## 对象到对象映射(Object to Object Mapping)
`AuthorAppService`使用`ObjectMapper`将`Author`对象转换为`AuthorDto`对象. 我们需要在 `AutoMapper` 配置中定义这个映射.

打开项目`Acme.BookStore.Application`中的`BookStoreApplicationAutoMapperProfile`类并将以下行添加到构造函数中：
```
CreateMap<Author, AuthorDto>();
```
## 种子数据(Data Seeder)
正如之前对books所做的那样，最好在数据库中包含一些初始author 实体.这在首次运行应用程序时很有用，但对于自动化测试也非常有用.

在`Acme.BookStore.Domain`项目中打开`BookStoreDataSeederContributor`并使用以下代码更改文件内容：
```
using System;
using System.Threading.Tasks;
using Acme.BookStore.Authors;
using Acme.BookStore.Books;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore
{
    public class BookStoreDataSeederContributor
        : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Book, Guid> _bookRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly AuthorManager _authorManager;

        public BookStoreDataSeederContributor(
            IRepository<Book, Guid> bookRepository,
            IAuthorRepository authorRepository,
            AuthorManager authorManager)
        {
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
            _authorManager = authorManager;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (await _bookRepository.GetCountAsync() <= 0)
            {
                await _bookRepository.InsertAsync(
                    new Book
                    {
                        Name = "1984",
                        Type = BookType.Dystopia,
                        PublishDate = new DateTime(1949, 6, 8),
                        Price = 19.84f
                    },
                    autoSave: true
                );

                await _bookRepository.InsertAsync(
                    new Book
                    {
                        Name = "The Hitchhiker's Guide to the Galaxy",
                        Type = BookType.ScienceFiction,
                        PublishDate = new DateTime(1995, 9, 27),
                        Price = 42.0f
                    },
                    autoSave: true
                );
            }

            // ADDED SEED DATA FOR AUTHORS

            if (await _authorRepository.GetCountAsync() <= 0)
            {
                await _authorRepository.InsertAsync(
                    await _authorManager.CreateAsync(
                        "George Orwell",
                        new DateTime(1903, 06, 25),
                        "Orwell produced literary criticism and poetry, fiction and polemical journalism; and is best known for the allegorical novella Animal Farm (1945) and the dystopian novel Nineteen Eighty-Four (1949)."
                    )
                );

                await _authorRepository.InsertAsync(
                    await _authorManager.CreateAsync(
                        "Douglas Adams",
                        new DateTime(1952, 03, 11),
                        "Douglas Adams was an English author, screenwriter, essayist, humorist, satirist and dramatist. Adams was an advocate for environmentalism and conservation, a lover of fast cars, technological innovation and the Apple Macintosh, and a self-proclaimed 'radical atheist'."
                    )
                );
            }
        }
    }
}
```
现在，您可以运行`.DbMigrator`控制台应用程序迁移`数据库架构`和`初始数据`.

## 测试Author应用服务
最后，我们可以为`IAuthorAppService`写一些测试. 在项目`Acme.BookStore.Application.Tests`的`Authors`命名空间（文件夹）中命名添加一个新类`AuthorAppService_Tests`：
```
using System;
using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace Acme.BookStore.Authors
{ 
    public class AuthorAppService_Tests : BookStoreApplicationTestBase
    {
        private readonly IAuthorAppService _authorAppService;

        public AuthorAppService_Tests()
        {
            _authorAppService = GetRequiredService<IAuthorAppService>();
        }

        [Fact]
        public async Task Should_Get_All_Authors_Without_Any_Filter()
        {
            var result = await _authorAppService.GetListAsync(new GetAuthorListDto());

            result.TotalCount.ShouldBeGreaterThanOrEqualTo(2);
            result.Items.ShouldContain(author => author.Name == "George Orwell");
            result.Items.ShouldContain(author => author.Name == "Douglas Adams");
        }

        [Fact]
        public async Task Should_Get_Filtered_Authors()
        {
            var result = await _authorAppService.GetListAsync(
                new GetAuthorListDto {Filter = "George"});

            result.TotalCount.ShouldBeGreaterThanOrEqualTo(1);
            result.Items.ShouldContain(author => author.Name == "George Orwell");
            result.Items.ShouldNotContain(author => author.Name == "Douglas Adams");
        }

        [Fact]
        public async Task Should_Create_A_New_Author()
        {
            var authorDto = await _authorAppService.CreateAsync(
                new CreateAuthorDto
                {
                    Name = "Edward Bellamy",
                    BirthDate = new DateTime(1850, 05, 22),
                    ShortBio = "Edward Bellamy was an American author..."
                }
            );
            
            authorDto.Id.ShouldNotBe(Guid.Empty);
            authorDto.Name.ShouldBe("Edward Bellamy");
        }

        [Fact]
        public async Task Should_Not_Allow_To_Create_Duplicate_Author()
        {
            await Assert.ThrowsAsync<AuthorAlreadyExistsException>(async () =>
            {
                await _authorAppService.CreateAsync(
                    new CreateAuthorDto
                    {
                        Name = "Douglas Adams",
                        BirthDate = DateTime.Now,
                        ShortBio = "..."
                    }
                );
            });
        }

        //TODO: Test other methods...
    }
}
```
为应用服务方法创建了一些测试，这些测试应该清晰且易于理解.

## 下一步
- 参考[动态JavaScript代理和本地化](no-ui-api-host-part2.md)
- 查看本教程的[下一章](no-ui-api-host-part8.md).

