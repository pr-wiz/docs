# 选项模式介绍
## 作用
- 选项模式使用类来提供对相关设置组的强类型访问。
- 选项类适用于特定业务领域或模块的设置。 
- 通过特定的选项类来隔离配置，应遵循：
    - 接口分隔原则 (ISP) 或封装：依赖于配置设置业务仅依赖于其使用的配置设置。
    - 关注点分离：应用的不同部件的设置不彼此依赖或相互耦合。
## 选项定义
1. 选项类是一个普通的类, 用来存储配置的值
1. 选项类以 Options 为后缀
1. 选项类一般在Domain层定义
1. 选项类一般定义一个常量属性 `SectionPosition`, 用来定义选项对应配置节的名称
1. 示例
    ```
     /// <summary>
    /// IoT配置选项
    /// </summary>
    public class DefaultIoTOptions
    {
        /// <summary>
        /// 配置节点名称
        /// </summary>
        public const string SectionPosition = "DefaultIoT";
        /// <summary>
        /// IoT 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 应用Id
        /// </summary>
        public uint AppId { get; set; }
        /// <summary>
        /// 集团编号
        /// </summary>
        public uint GroupId { get; set; }
        /// <summary>
        /// 电厂编号
        /// </summary>
        public uint FactoryId { get; set; }
    }
    ```
## 选项注册
1. 在定义选项的层(一般为Domain), 应进行选项的默认注册(从配置文件注册)
    - 在模块类的`ConfigureServices` 方法中通过以下方式注册
        ```
        Configure<DefaultIoTOptions>(context.Services.GetConfiguration().GetSection(DefaultIoTOptions.SectionPosition));
        ```
1. 在启动模块注册(可选)
    - 在 `ConfigureServices` 方法中通过以下方式注册
        - 通过配置文件注册选项
            ```IServiceCollection.Configure<TOptions>(configuration.GetSection("configName"));```
            - 此方法读取配置文件相关内容绑定到选项并注册
            - Abp 模块类提供更简单的方法
                ```Configure<TOptions>(configuration.GetSection("configName"));```
        - 通过选项配置委托注册选项
            ```IServiceCollection.Configure<TOptions>(Action<TOptions> configOptions);```
            - 此方法直接设置选项并注册
            - Abp 模块类提供更简单的方法
                ```Configure<TOptions>(Action<TOptions> configOptions);```
1. 选项可以多次注册
    - 注册结果为合并选项属性
    - 如果多次注册同时设置了某个选项属性, 则为最后一次注册的属性值
    - 此方法提供重设选项的功能
    
## 选项注入
1. 通过接口 `IOptions<TOptions>` 注入
    - **不支持**在应用启动后读取配置数据, 即配置文件变更不会更新到选项
    - **不支持**命名选项
    - 注册为`Singleton`且可以注入到**任何服务生存期**
    - 示例(构造方法注入)
        ```
        private readonly PositionOptions _options;
        public Test2Model(IOptions<PositionOptions> options)
        {
            _options = options.Value;
        }
        ```
- 通过接口 `IOptionsSnapshot<TOptions>` 注入
    - **支持**在应用启动后读取配置数据, 每次请求时重新计算选项，因此配置文件变更会更新到选项
    - 注册为`Scope`，无法注入到`Singleton`实例服务。
    - **支持**命名选项
    - 示例(构造方法注入)
        ```
        private readonly MyOptions _snapshotOptions;

        public TestSnapModel(IOptionsSnapshot<MyOptions> snapshotOptions)
        {
            _snapshotOptions = snapshotOptions.Value;
        }
        ```
- 通过接口 `IOptionsMonitor<TOptions>` 注入
    - 用于检索选项并管理 TOptions 实例的选项通知
    - 注册为`Singleton`且可以注入到**任何服务生存期**
    - **支持**命名选项
    - **支持**在应用启动后读取配置数据
    - **支持**更改通知
    - **支持**选择性选项失效 (`IOptionsMonitorCache<TOptions>`)
    - 示例(构造方法注入)
        ```
        private readonly IOptionsMonitor<MyOptions> _optionsDelegate;

        public TestMonitorModel(IOptionsMonitor<MyOptions> optionsDelegate )
        {
            _optionsDelegate = optionsDelegate;
        }
        ```
## 命名选项
1. 所有选项都是命名没选，没有命名的选项会有提供默认名称(string.Empty)
1. 注册
    ```
    public void ConfigureServices(IServiceCollection services)
    {
        services.Configure<TopItemSettings>("Month",
                                        Configuration.GetSection("TopItem:Month"));
        services.Configure<TopItemSettings>("Year",
                                            Configuration.GetSection("TopItem:Year"));
    }
    ```
1. 注入
    ```
    private readonly TopItemSettings _monthTopItem;
    private readonly TopItemSettings _yearTopItem;

    public TestNOModel(IOptionsSnapshot<TopItemSettings> namedOptionsAccessor)
    {
        _monthTopItem = namedOptionsAccessor.Get(TopItemSettings.Month);
        _yearTopItem = namedOptionsAccessor.Get(TopItemSettings.Year);
    }
    ```

## 选项验证(略)

