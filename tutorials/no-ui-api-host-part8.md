# API Host应用程序开发教程 - 第八章: 实体关系

## 介绍
我们为书商店应用程序创建了`Book`和`Author`的功能.但是，目前这些实体之间没有任何关系.

在本教程中，我们将在`Author`和`Book`实体之间建立**1 to N** 的关系.

## 添加与Book实体的关系
在Acme.BookStore.Domain项目中打开Books/Book.cs并向Book实体添加以下属性：
```
public Guid AuthorId { get; set; }
```

> 在本教程中，我们倾向于不向`Book`类中的实体添加`Author`**导航属性**(如: `public Author Author { get; set; }`).这是由于遵循 DDD 最佳实践（规则：仅通过 `id` 引用其他聚合）.但是，您可以为 EF Core 添加这样的**导航属性**并配置它.通过这种方式，您无需在获取带有authors的books时编写连接查询（就像我们将在下面完成的那样），这使您的应用程序代码更简单.

## 数据库和数据迁移
向`Book`实体添加了一个新的必需属性`AuthorId`.但是，对于数据库的现有Book呢？它们目前没有`AuthorId`，当我们尝试运行应用程序时，这将是一个问题.

这是一个**典型的迁移问题**，如何决定取决于您的实际情况；

- 如果您尚未将应用程序发布到生产环境中，您可以删除数据库中现有的书籍，甚至可以删除开发环境中的整个数据库.
- 您可以在数据迁移或初始化数据阶段以编程方式更新现有数据.
- 您可以在数据库上手动处理它.

我们更喜欢`删除数据库`（您可以在包管理器控制台中运行`Drop-Database`），因为这只是一个示例项目，数据丢失并不重要.由于本主题与 ABP 框架无关，因此我们不会深入了解所有场景.

### 更新 EF 集成映射
在项目`EntityFrameworkCoreAcme.BookStore`文件夹下`BookStoreDbContext`类中找到方法`OnModelCreating`，修改部分`EntityFrameworkCorebuilder.Entity<Book>`如下:
```
builder.Entity<Book>(b =>
{
    b.ToTable(BookStoreConsts.DbTablePrefix + "Books", BookStoreConsts.DbSchema);
    b.ConfigureByConvention(); //auto configure for the base class props
    b.Property(x => x.Name).IsRequired().HasMaxLength(128);

    // ADD THE MAPPING FOR THE RELATION
    b.HasOne<Author>().WithMany().HasForeignKey(x => x.AuthorId).IsRequired();
});
```

### 添加新的 EF Core 迁移
启动模板配置为使用[Entity Framework Core Code First Migrations](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/).由于我们已经更改了数据库映射配置，创建一个新的迁移并将更改应用于数据库.

在`Acme.BookStore.EntityFrameworkCore`项目目录中打开命令行终端并键入以下命令：
```
dotnet ef migrations add Added_AuthorId_To_Book
```
这应该在其`Up`方法中使用以下代码创建一个新的迁移类：
```
migrationBuilder.AddColumn<Guid>(
    name: "AuthorId",
    table: "AppBooks",
    nullable: false,
    defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

migrationBuilder.CreateIndex(
    name: "IX_AppBooks_AuthorId",
    table: "AppBooks",
    column: "AuthorId");

migrationBuilder.AddForeignKey(
    name: "FK_AppBooks_AppAuthors_AuthorId",
    table: "AppBooks",
    column: "AuthorId",
    principalTable: "AppAuthors",
    principalColumn: "Id",
    onDelete: ReferentialAction.Cascade);
```
- 向`AppBooks`表中添加一个字段`AuthorId`.
- 在`AuthorId`字段上创建索引.
- 声明`AppAuthors`表的外键.

> 如果您使用的是 Visual Studio，则可能需要在包管理器控制台 (PMC) 中使用`Add-Migration Added_AuthorId_To_Book -Context BookStoreMigrationsDbContext`和`Update-Database -Context BookStoreMigrationsDbContext`命令.在这种情况下，请确保`Acme.BookStore.HttpApi.Host`是启动项目并且`Acme.BookStore.WebAcme.BookStore.EntityFrameworkCore`是PMC 中的默认项目.

### 更改初始化数据
由于`AuthorId`是`Book`实体的必需属性，因此当前的初始化数据代码无法工作.在`Acme.BookStore.Domain`项目中打开`BookStoreDataSeederContributor`，修改如下：
```
using System;
using System.Threading.Tasks;
using Acme.BookStore.Authors;
using Acme.BookStore.Books;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore
{
    public class BookStoreDataSeederContributor
        : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Book, Guid> _bookRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly AuthorManager _authorManager;

        public BookStoreDataSeederContributor(
            IRepository<Book, Guid> bookRepository,
            IAuthorRepository authorRepository,
            AuthorManager authorManager)
        {
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
            _authorManager = authorManager;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (await _bookRepository.GetCountAsync() > 0)
            {
                return;
            }

            var orwell = await _authorRepository.InsertAsync(
                await _authorManager.CreateAsync(
                    "George Orwell",
                    new DateTime(1903, 06, 25),
                    "Orwell produced literary criticism and poetry, fiction and polemical journalism; and is best known for the allegorical novella Animal Farm (1945) and the dystopian novel Nineteen Eighty-Four (1949)."
                )
            );

            var douglas = await _authorRepository.InsertAsync(
                await _authorManager.CreateAsync(
                    "Douglas Adams",
                    new DateTime(1952, 03, 11),
                    "Douglas Adams was an English author, screenwriter, essayist, humorist, satirist and dramatist. Adams was an advocate for environmentalism and conservation, a lover of fast cars, technological innovation and the Apple Macintosh, and a self-proclaimed 'radical atheist'."
                )
            );

            await _bookRepository.InsertAsync(
                new Book
                {
                    AuthorId = orwell.Id, // SET THE AUTHOR
                    Name = "1984",
                    Type = BookType.Dystopia,
                    PublishDate = new DateTime(1949, 6, 8),
                    Price = 19.84f
                },
                autoSave: true
            );

            await _bookRepository.InsertAsync(
                new Book
                {
                    AuthorId = douglas.Id, // SET THE AUTHOR
                    Name = "The Hitchhiker's Guide to the Galaxy",
                    Type = BookType.ScienceFiction,
                    PublishDate = new DateTime(1995, 9, 27),
                    Price = 42.0f
                },
                autoSave: true
            );
        }
    }
}
```
唯一的变化是我们设置了`Book`实体的属性`AuthorId`.

> 在执行`DbMigrator`前删除已存在的`books`或删除数据库. 有关更多信息，请参阅上面的数据库和数据迁移部分.

现在，您可以运行`.DbMigrator`控制台应用程序迁移的数据库架构和初始数据.

## 应用层
我们将更改`BookAppService`以支持`Author`关系.

### 数据传输对象
让我们从 DTO 开始.

#### BookDto
打开项目`BooksAcme.BookStore.Application.Contracts`文件夹`Books`中的`BookDto`类并添加以下属性：
```
public Guid AuthorId { get; set; }
public string AuthorName { get; set; }
```
最后的BookDto课程应该如下：
```
using System;
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.Books
{
    public class BookDto : AuditedEntityDto<Guid>
    {
        public Guid AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string Name { get; set; }

        public BookType Type { get; set; }

        public DateTime PublishDate { get; set; }

        public float Price { get; set; }
    }
}
```

#### CreateUpdateBookDto
打开项目`BooksAcme.BookStore.Application.Contracts`文件夹`Books`中的`CreateUpdateBookDto`类，添加一个属性`AuthorId`，如下：
```
public Guid AuthorId { get; set; }
```

#### AuthorLookupDto
在`Acme.BookStore.Application.Contracts`项目`Books`的文件夹中创建一个新的类`AuthorLookupDto`：
```
using System;
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.Books
{
    public class AuthorLookupDto : EntityDto<Guid>
    {
        public string Name { get; set; }
    }
}
```
这将用于`IBookAppService`的新方法中.

### IBookAppService
打开项目`BooksAcme.BookStore.Application.Contracts`文件夹`Books`中的`IBookAppService`，添加一个新的方法，命名为`ContractsGetAuthorLookupAsync`，如下：
```
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Acme.BookStore.Books
{
    public interface IBookAppService :
        ICrudAppService< //Defines CRUD methods
            BookDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateBookDto> //Used to create/update a book
    {
        // ADD the NEW METHOD
        Task<ListResultDto<AuthorLookupDto>> GetAuthorLookupAsync();
    }
}
```
这个新方法将被 UI 用于获取Author列表并填充下拉列表以选择Book的Author.

### BookAppService
打开项目`BooksAcme.BookStore.Application`文件夹`Books`中的`IBookAppService`，将文件内容替换为如下代码：
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Acme.BookStore.Authors;
using Acme.BookStore.Permissions;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore.Books
{
    [Authorize(BookStorePermissions.Books.Default)]
    public class BookAppService :
        CrudAppService<
            Book, //The Book entity
            BookDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateBookDto>, //Used to create/update a book
        IBookAppService //implement the IBookAppService
    {
        private readonly IAuthorRepository _authorRepository;

        public BookAppService(
            IRepository<Book, Guid> repository,
            IAuthorRepository authorRepository)
            : base(repository)
        {
            _authorRepository = authorRepository;
            GetPolicyName = BookStorePermissions.Books.Default;
            GetListPolicyName = BookStorePermissions.Books.Default;
            CreatePolicyName = BookStorePermissions.Books.Create;
            UpdatePolicyName = BookStorePermissions.Books.Edit;
            DeletePolicyName = BookStorePermissions.Books.Create;
        }

        public override async Task<BookDto> GetAsync(Guid id)
        {
            //Get the IQueryable<Book> from the repository
            var queryable = await Repository.GetQueryableAsync();

            //Prepare a query to join books and authors
            var query = from book in queryable
                join author in _authorRepository on book.AuthorId equals author.Id
                where book.Id == id
                select new { book, author };

            //Execute the query and get the book with author
            var queryResult = await AsyncExecuter.FirstOrDefaultAsync(query);
            if (queryResult == null)
            {
                throw new EntityNotFoundException(typeof(Book), id);
            }

            var bookDto = ObjectMapper.Map<Book, BookDto>(queryResult.book);
            bookDto.AuthorName = queryResult.author.Name;
            return bookDto;
        }

        public override async Task<PagedResultDto<BookDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            //Get the IQueryable<Book> from the repository
            var queryable = await Repository.GetQueryableAsync();

            //Prepare a query to join books and authors
            var query = from book in queryable
                join author in _authorRepository on book.AuthorId equals author.Id
                select new {book, author};

            //Paging
            query = query
                .OrderBy(NormalizeSorting(input.Sorting))
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            //Execute the query and get a list
            var queryResult = await AsyncExecuter.ToListAsync(query);

            //Convert the query result to a list of BookDto objects
            var bookDtos = queryResult.Select(x =>
            {
                var bookDto = ObjectMapper.Map<Book, BookDto>(x.book);
                bookDto.AuthorName = x.author.Name;
                return bookDto;
            }).ToList();

            //Get the total count with another query
            var totalCount = await Repository.GetCountAsync();

            return new PagedResultDto<BookDto>(
                totalCount,
                bookDtos
            );
        }

        public async Task<ListResultDto<AuthorLookupDto>> GetAuthorLookupAsync()
        {
            var authors = await _authorRepository.GetListAsync();

            return new ListResultDto<AuthorLookupDto>(
                ObjectMapper.Map<List<Author>, List<AuthorLookupDto>>(authors)
            );
        }

        private static string NormalizeSorting(string sorting)
        {
            if (sorting.IsNullOrEmpty())
            {
                return $"book.{nameof(Book.Name)}";
            }

            if (sorting.Contains("authorName", StringComparison.OrdinalIgnoreCase))
            {
                return sorting.Replace(
                    "authorName",
                    "author.Name", 
                    StringComparison.OrdinalIgnoreCase
                );
            }

            return $"book.{sorting}";
        }
    }
}
```
让我们看看我们所做的更改：

- 添加`[Authorize(BookStorePermissions.Books.Default)]`以授权我们新添加/覆盖的方法（请记住，当为类声明`authorize` 属性对类的所有方法都有效）.
- 注入`IAuthorRepository`用来查询Author.
- 重写基类`CrudAppService` 的`GetAsync`方法，该方法返回具有给定id的单个对象`BookDto`.
    - 使用简单的 LINQ 表达式连接`books`和`authors`，并一起查询给定book ID.
    - 使用`AsyncExecuter.FirstOrDefaultAsync(...)`执行查询并得到结果.这是一种使用异步 LINQ 扩展而不依赖于数据库提供程序 API 的方法.查看[存储库文档](https://docs.abp.io/en/abp/latest/Repositories)以了解我们使用它的原因.
    - 如果数据库中不存在请求的Book，则抛出`EntityNotFoundException`异常并返回`HTTP 404`（未找到）结果.
    - 最后，使用ObjectMapper 来创建一个BookDto对象，然后手动分配AuthorName.
- 重写基类`CrudAppService` 的`GetListAsync`方法，该方法返回Book列表.逻辑与前面的方法类似，因此可以轻松理解代码.
- 创建了一个新方法：`GetAuthorLookupAsync`. 这个仅仅获取所有作者.UI 使用此方法填充下拉列表并在创建/编辑Book时进行选择.

### 对象到对象映射配置(Object to Object Mapping Configuration)
类`AuthorLookupDto`在`GetAuthorLookupAsync`方法中引入了和使用的对象映射.因此，需要在Acme.BookStore.Application项目文件BookStoreApplicationAutoMapperProfile.cs中添加一个新的映射定义：
```
CreateMap<Author, AuthorLookupDto>();
```

### 单元测试
由于我们对AuthorAppService做了变更, 一些单元测试会失败. 打开项目Acme.BookStore.Application.Tests的Books文件夹中的BookAppService_Tests，修改内容如下：
```
using System;
using System.Linq;
using System.Threading.Tasks;
using Acme.BookStore.Authors;
using Shouldly;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Validation;
using Xunit;

namespace Acme.BookStore.Books
{ 
    public class BookAppService_Tests : BookStoreApplicationTestBase
    {
        private readonly IBookAppService _bookAppService;
        private readonly IAuthorAppService _authorAppService;

        public BookAppService_Tests()
        {
            _bookAppService = GetRequiredService<IBookAppService>();
            _authorAppService = GetRequiredService<IAuthorAppService>();
        }

        [Fact]
        public async Task Should_Get_List_Of_Books()
        {
            //Act
            var result = await _bookAppService.GetListAsync(
                new PagedAndSortedResultRequestDto()
            );

            //Assert
            result.TotalCount.ShouldBeGreaterThan(0);
            result.Items.ShouldContain(b => b.Name == "1984" &&
                                       b.AuthorName == "George Orwell");
        }

        [Fact]
        public async Task Should_Create_A_Valid_Book()
        {
            var authors = await _authorAppService.GetListAsync(new GetAuthorListDto());
            var firstAuthor = authors.Items.First();

            //Act
            var result = await _bookAppService.CreateAsync(
                new CreateUpdateBookDto
                {
                    AuthorId = firstAuthor.Id,
                    Name = "New test book 42",
                    Price = 10,
                    PublishDate = System.DateTime.Now,
                    Type = BookType.ScienceFiction
                }
            );

            //Assert
            result.Id.ShouldNotBe(Guid.Empty);
            result.Name.ShouldBe("New test book 42");
        }

        [Fact]
        public async Task Should_Not_Create_A_Book_Without_Name()
        {
            var exception = await Assert.ThrowsAsync<AbpValidationException>(async () =>
            {
                await _bookAppService.CreateAsync(
                    new CreateUpdateBookDto
                    {
                        Name = "",
                        Price = 10,
                        PublishDate = DateTime.Now,
                        Type = BookType.ScienceFiction
                    }
                );
            });

            exception.ValidationErrors
                .ShouldContain(err => err.MemberNames.Any(m => m == "Name"));
        }
    }
}
```
- 改变`Should_Get_List_Of_Books`中断言条件从`b => b.Name == "1984"`为`b => b.Name == "1984" && b.AuthorName == "George Orwell"`以检查是否提供了作者的名字.
- 更改了的法`Should_Create_A_Valid_Book`在创建新`Book`时设置AuthorId.