# 项目中出现 Assert Failure Expression: [Recursive resource lookup bug]
1. 运行时出现如下错误：
    ```
    Assert Failure
    Expression: [Recursive resource lookup bug]
    Description: Infinite recursion during resource lookup within System.Private.CoreLib.  This may be a bug in System.Private.CoreLib, or potentially in certain extensibility points such as assembly resolve events or CultureInfo names.  Resource name: ArgumentNull_Generic
    ```
1. 出现原因

服务器本地文化为非标准文化引起，如：en-US-POSIX

1. 解决方案：将文化设置为标准文化，如: en-US

    - 当出现此类错误时，提供配置文件设置默认文化的方式
        
        在配置文件appsettings.json中增加以下配置：
        ```
        "DefaultCurrentCulture": "en-US"
        ```
1. 在Setup类的构造方法中加入：
    ```
    // 读取默认文化信息，名称为: en-US, zh-CN 等
    var cultureName = configuration.GetValue<string>("DefaultCurrentCulture");
    if (cultureName == null && Thread.CurrentThread.CurrentCulture.Name == "en-US-POSIX")
    {
        cultureName = "en-US";
    }

    if (cultureName != null)
    {
        CultureInfo culture;
        try
        {
            culture = CultureInfo.CreateSpecificCulture(cultureName);
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }
        catch
        {
            Console.WriteLine($"不支持的CultureInfo名称 {cultureName}");
        }
    }
    ```