# 理解Abp框架
## Abp框架
### 架构
1. 模块架构
    ![avatar](../setup-templates/images/layered-project-dependencies-module.png)
    参考: [模板启动模板](../setup-templates/module.md)
1. 应用程序架构
    ![avatar](../setup-templates/images/layered-project-dependencies.png)
    参考: [应用程序启动模板](../setup-templates/application.md)
### 框架基础功能
1. Dependency Injection(依赖注入)
    - 模块化自注册
    - 基架中集成AutoFac
1. Configuration(配置)
    - 同ASP.NET Core的配置
1. Options(选项)
    - Abp模块基类提供快捷配置 `Configure<TOpions>` 扩展方法
    - Abp模块基类提供预配置选项方法 `PreConfigure<MyPreOptions>`
1. Authorization(授权)
    - 添加 `Permission` 做为自动策略来扩充 ASP.NET Core 授权
    - `PermissionDefinitionProvider`
    - 本地化 `Permission` 名称
    - `Permission` 支持多租户
1. Validation(验证)
    - 与 ASP.NET Core 模型验证系统兼容
    - 验证输入DTO
        - 数据注释特性(`[Required]`,  `[StringLength(100)]`,...)
        - 可验证对象(`IValidatableObject`)
    - 验证基础设施
        - `IValidationEnabled` 
        - `[DisableValidation]`
        - `AbpValidationException`
    - 验证器
        - `IObjectValidator`
        - `IMethodInvocationValidator`
1. Localization(本地化)
    - `IStringLocalizer<TResource>`
    - `IHtmlLocalizer<T>`
1. Caching(缓存)
    - `IDistributedCache<TCacheItem>` 
    - `IDistributedCache<TCacheItem, TCacheKey>`
    - `IDistributedCacheSerializer`
1. Exception Handling(异常处理)
    - 自动处理所有异常，并针对 API/AJAX 请求向客户端发送标准格式的错误消息。
    - 自动隐藏内部基础设施错误并返回标准错误消息
    - 提供一种简单且可配置的方法来本地化异常消息
    - 自动将标准异常映射到HTTP 状态代码，并提供一个可配置的选项来映射自定义异常
1. Settings(设置)
    - `SettingDefinition`
    - `SettingDefinitionProvider`
    - `ISettingProvider`
        - `DefaultValueSettingValueProvider`
        - `ConfigurationSettingValueProvider`
        - `GlobalSettingValueProvider`
        - `TenantSettingValueProvider`
        - `UserSettingValueProvider`
    - `ISettingStore`
        - `NullSettingStore` 
    - `ISettingEncryptionService`
1. Connection Strings(连接字符串)
    - `IConnectionStringResolver`
        - `DefaultConnectionStringResolver`
        - `MultiTenantConnectionStringResolver`
    - 允许为每个模块设置单独的连接字符串，因此每个模块都可以拥有自己的物理数据库。模块甚至可能被配置为使用不同的 DBMS。
    - 允许设置单独的连接字符串并为每个租户使用单独的数据库。
    - 允许将模块分组到数据库中。
    - 允许将租户分组到数据库中，就像模块一样。
    - 允许每个模块每个租户单独的数据库
1. Object Extensions(对象扩展)
    - `IHasExtraProperties` 
        - `AggregateRoot` 
        - `ExtensibleEntityDto`
        - `ExtensibleAuditedEntityDto`
        - `ExtensibleObject`
1. Logging(日志)
    - 同ASP.NET Core的日志系统
### 框架基础设施
1. Audit Logging(审计日志)
    - 审计日志对象包含
        - 请求和响应详细信息（如 URL、Http 方法、浏览器信息、HTTP 状态代码...等）。
        - 执行的操作（控制器操作和应用程序服务方法调用及其参数）。
        - Web 请求中发生了实体更改。
        - 异常信息（如果在执行请求时出现错误）。
        - 请求持续时间（用于衡量应用程序的性能）。
    - 可配置启用
    - 可扩展
1. Background Jobs(后台任务)
    - `Hangfire`
    - `RabbitMQ` 
    - `Quartz`
1. BLOB Storing(BLOB存储)
    - 文件系统
    - 数据库
    - `Azure`
    - `Aliyun`
    - `Minio`
    - `Aws`
1. CSRF/XSRF & Anti Forgery(反跨站点请求伪造攻击)
    - 已内置
    - 可定制
1. Current User(当前用户)
    - `ICurrentUser`
1. Data Filtering(数据过滤)
    - `ISoftDelete`
    - `IMultiTenant`
    - 可禁用/启用
1. Data Seeding(初始数据)
    - `IDataSeedContributor`
1. Email Sending(发送电子邮件)
    - `IEmailSender` 
        - `NullEmailSender`
    - `ISmtpEmailSender`
    - `IMailKitSmtpEmailSender`
   
1. SMS Sending(发送短信)
    - `ISmsSender` 
        - `NullSmsSender`
        - `AliyunSmsSender`
1. Event Bus(事件总线)
    - Local Event Bus(本地事件总线)
        - `ILocalEventBus`
    - Distributed Event Bus(分布式事件总线)
        - `LocalDistributedEventBus` 
        - `RabbitMqDistributedEventBus` 
        - `KafkaDistributedEventBus`
        - `RebusDistributedEventBus` 
1. JSON
    - `IJsonSerializer`
1. Features(功能特性)
    - `FeatureDefinitionProvider`
    - `[RequiresFeature]`
    - `IFeatureChecker`
1. Global Features(全局功能特性)
    - 在开发时禁/启用
    - 启用时才创建数据库表
1. GUID Generation(GUID生成器)
    - `IGuidGenerator`
    - 提供生成顺序的GUID, 当做为聚集索引时，能带来显著的插入性能提升
1. Object To Object Mapping(对象自动映射)
    - `IObjectMapper`
1. String Encryption(字符串加密)
    - `IStringEncryptionService`
1. Text Templating(文本模板)
    - `Razor`
    - `Scriban`
1. Timing(时间)
    - `IClock`
    - `ITimezoneProvider` 
1. Virtual File System(虚拟文件系统)
    - 嵌入文件
1. Cancellation Token Provider(取消令牌提供者)
    - `ICancellationTokenProvider`
        - `NullCancellationTokenProvider`
        - `HttpContextCancellationTokenProvider`
1. Simple State Checker(简单的状态检查器)
    - `IHasSimpleStateCheckers`
## 框架启动流程
### Program
- 构建`IHost`并运行
- 构建`IHostBuilder`
    ```
    internal static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(build =>
                {
                    build.AddJsonFile("appsettings.secrets.json", optional: true);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseAutofac() // 使用AutoFac IoC容器
                .UseSerilog(); // 启用Serilog
    ```
### Setup
1. 配置(注册)服务 `ConfigureServices`
    ```
    public void ConfigureServices(IServiceCollection services)
    {
        // 创建应用并注册到容器 (IAbpApplicationWithExternalServiceProvider)
        services.AddApplication<MultiDbHttpApiHostModule>(options =>
        {
            services.ConfigurePluginSource(Path.Combine(services.GetHostingEnvironment().ContentRootPath, $"..{Path.DirectorySeparatorChar}"), options);
        });            
    }
    ```
    - `IAbpApplicationWithExternalServiceProvider` 的实现类(`AbpApplicationWithExternalServiceProvider`)在构造时处理:
        1. 注册启动模块类型为 `InitModule`
        1. 注册`IServiceProvider`的占位: `ObjectAccessor<IServiceProvider>`
            - 实际是通过 IServiceProviderFactory.CreateServiceProvider() 创建替换，**AbpAutofacServiceProviderFactory** 
            - `IServiceProviderFactory`实际注册是在`IHostBuilder.UseAutofac()` 扩展方法中
            - 实际实现类型为 `AutofacServiceProvider`
            - 在应用程序初始化方法`Initialize`中创建并替换 `IServiceProvider`, 通过调用`CreateServiceProvider`
            - 受保护方法`SetServiceProvider`
                ```
                protected virtual void SetServiceProvider(IServiceProvider serviceProvider)
                {
                    ServiceProvider = serviceProvider;
                    ServiceProvider.GetRequiredService<ObjectAccessor<IServiceProvider>>().Value = ServiceProvider;
                }
                ```
        1. 创建 `AbpApplicationCreationOptions` 选项
        1. 如果存在`Action<AbpApplicationCreationOptions>`委托则执行它
        1. 将自身注册为 `IAbpApplication`
        1. 将自身注册为 `IModuleContainer`
        1. 注册核心服务
            - `services.AddOptions();`
            - `services.AddLogging();`
            - `services.AddLocalization();`
        1. 注册Abp核心服务
            - 确保注册配置 `IConfiguration` (单例)
            - 注册 `IModuleLoader` (单例)
            - 注册 `IAssemblyFinder` (单例)
            - 注册 `ITypeFinder` (单例)
            - 注册 `IInitLoggerFactory` (单例)
            - 注册 `Volo.Abp.Core` 程序集中符合注册器约定的类型到容器 （约定有接口, 特性, 名称等)
            - 注册 `ISimpleStateCheckerManager` (瞬态)
            - 注册 `AbpModuleLifecycleOptions` 选项, 管理模块生命周期`Contributor`(在`AbpApplicationWithExternalServiceProvider.Initialize`方法中调用)
                - `options.Contributors.Add<OnPreApplicationInitializationModuleLifecycleContributor>();`
                - `options.Contributors.Add<OnApplicationInitializationModuleLifecycleContributor>();`
                - `options.Contributors.Add<OnPostApplicationInitializationModuleLifecycleContributor>();`
                - `options.Contributors.Add<OnApplicationShutdownModuleLifecycleContributor>();`
        1. 加载模块
            - 调用 `IModuleLoader.LoadModules` 方法(同时会加载插件中模块)
            - 将模块按照依赖关系排序返回 `IAbpModuleDescriptor[]`
        1. 注册模块服务
            1. 注册模块`PreConfigureServices`服务                 
                - 模块类实现了 `IPreConfigureServices` 接口才会执行 
                - 模块类 `PreConfigureServices` 方法中注册的服务   
                - 按模块的依赖关系依次执行(先执行被依赖的模块)               
            1. 注册模块服务                
                - 自动注册服务
                    - 模块`SkipAutoServiceRegistration` 设置为`false`才会自动注册
                    - 将模块类所在程序集中符合注册器约定的类型注册到容器 （约定有接口, 特性, 名称等)
                - 模块类 `ConfigureServices` 方法中注册的服务
                - 按模块的依赖关系依次执行(先执行被依赖的模块)
            1. 注册模块`PostConfigureServices`服务 
                - 按模块的依赖关系依次执行(先执行被依赖的模块)
                - 模块类实现了 `IPostConfigureServices` 接口才会执行 
                - 模块类 `PostConfigureServices` 方法中注册的服务   
        1. 将自已注册为 `IAbpApplicationWithExternalServiceProvider`
1. Abp应用初始化 `Configure`
    ```
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
    {
        app.InitializeApplication();
    }
    ```
    - `IApplicationBuilder.InitializeApplication` 为扩展方法, 此方法处理：
        1. 替换 IApplicationBuilder 占位 `ObjectAccessor<IApplicationBuilder>` 为实际的 app
            `app.ApplicationServices.GetRequiredService<ObjectAccessor<IApplicationBuilder>>().Value = app;`
        1. 注册 `IHostApplicationLifetime` 事件
            ```
            var application = app.ApplicationServices.GetRequiredService<IAbpApplicationWithExternalServiceProvider>();
            var applicationLifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>();

            applicationLifetime.ApplicationStopping.Register(() =>
            {
                application.Shutdown();
            });

            applicationLifetime.ApplicationStopped.Register(() =>
            {
                application.Dispose();
            });
            ```
        1. 应用初始化
            ```
            application.Initialize(app.ApplicationServices);
            ```
            - 设置`IServiceProvider` 占位 `ObjectAccessor<IServiceProvider>`
            - 初始化模块生成周期
                - 为每一个模块生命周期`Contributor`执行初始化模块方法 `Initialize` (同时执行各生命周期初始化方法?)
                    - 按模块的依赖关系依次执行(先执行被依赖的模块)

        ![avatar](images/module-lifetime.png)
                    

## 模块
实现 `IAbpModule` 接口的类就叫做Abp模块
```
public interface IAbpModule
{
    void ConfigureServices(ServiceConfigurationContext context);
}
```
应在 `ConfigureServices` 方法中完成本模块服务的注册, 而不应放在应用中注册

### 模块依赖
- 由于模块中的服务是在本模块中完成注册的, 一个模块依赖另一个模块，如果被赖模块比依赖模块服务后注册, 则有可能造成注入被依赖模块服务时异常情况。因此模块服务的注册顺序应根据依赖关系确定, 被依赖的模块应先注册服务。
- `Abp` 通过在模块类定义 `DependsOnAttribute` 来确定模块的依赖关系, 并根据依赖关系确定模块服务的注册顺序
- Abp 框架模块依赖关系如下图:
    ![avatar](images/module-depend-relation.png)

## 依赖注入
### 注册服务
#### 自动注册(通过 `IConventionalRegistrar`)
1. Abp IConventionalRegistrar 实现类
    - DefaultConventionalRegistrar
        - 定义了 `DependencyAttribute` 特性
        - 实现了 `ITransientDependency` 接口
        - 实现了 `ISingletonDependency` 接口
        - 实现了 `IScopedDependency` 接口
    - AbpWebAssemblyConventionalRegistrar 瞬态
        1. 继承 `ComponentBase`
    - AbpAspNetCoreMvcConventionalRegistrar 瞬态
        1. 继承 `Controller` 或 定义了 `ControllerAttribute` 特性
        1. 继承 `PageModel` 或 定义了 `PageModelAttribute` 特性
        1. 继承 `ViewComponent` 或 定义了 `ViewComponent` 特性
    - AbpSignalRConventionalRegistrar 瞬态
        1. 继承 `Hub`
    - AbpAutoMapperConventionalRegistrar 瞬态
        1. 实现了 `IValueResolver<,,>` 接口
        1. 实现了 `IMemberValueResolver<,,,>` 接口
        1. 实现了 `ITypeConverter<,>` 接口
        1. 实现了 `IValueConverter<,>` 接口
        1. 实现了 `IMappingAction<,>` 接口
    - AbpQuartzConventionalRegistrar
        1. 实现了 `IQuartzBackgroundWorker` 接口
        1. 必须是 `DefaultConventionalRegistrar` 解析的类型
    - AbpRepositoryConventionalRegistrar
        1. 必须实现了 `IRepository` 接口
        1. 默认 **只注册为接口**
        1. 当 `ExposeRepositoryClasses = true` 时 注册接口和类型
    - AbpFluentValidationConventionalRegistrar
        1. 必须实现了 `IValidator<>` 接口, 并且泛型参数类型为服务类类型
    - AbpMongoDbConventionalRegistrar
        1. 必须实现了 `IAbpMongoDbContext` 接口 且不是 `AbpMongoDbContext`
        1. 必须是 `DefaultConventionalRegistrar` 解析的类型
1. 启用自定义 IConventionalRegistrar 实现类
    - 在定义的模块类的 PreConfigureServices 方法中添加 IServiceCollection.AddConventionalRegistrar 扩展方法
        ```
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddConventionalRegistrar(new AbpFluentValidationConventionalRegistrar());
        }
        ```
1. 固有注册类型
    - 模块类注册为单例。
    - MVC 控制器（继承Controller或AbpController）注册为瞬态。
    - MVC 页面模型（继承PageModel或AbpPageModel）注册为瞬态。
    - MVC 视图组件（继承ViewComponent或AbpViewComponent）注册为瞬态。
    - 应用程序服务（继承ApplicationService类或其子类）注册为瞬态。
    - 存储库（实现BasicRepositoryBase类或其子类）注册为瞬态。
    - 领域服务（实现IDomainService接口或继承DomainService类）注册为瞬态。
    ```
    public class BlogPostAppService : ApplicationService
    {
    }
    ```
1. 依赖接口
    - ITransientDependency 注册瞬态生命周期。
    - ISingletonDependency 注册单例生命周期。
    - IScopedDependency 注册范围生命周期。
    ```
    public class TaxCalculator : ITransientDependency
    {
    }
    ```
1. 依赖特性`DependencyAttribute`
    - Lifetime: 注册的生命周期: Singleton,Transient或Scoped.
    - TryRegister: 设置true为仅在之前未注册的情况下注册服务。使用 IServiceCollection 的 TryAdd... 扩展方法。
    - ReplaceServices: 设置true为替换之前已经注册过的服务。使用 IServiceCollection 的 Replace 扩展方法。
    ```
    [Dependency(ServiceLifetime.Transient, ReplaceServices = true)]
    public class TaxCalculator
    {

    }
    ```
1. 暴露服务特性`ExposeServicesAttribute`
    - 通过 Type 控制相关类提供哪些服务
    ```
    [ExposeServices(typeof(ITaxCalculator))]
    public class TaxCalculator: ICalculator, ITaxCalculator, ICanCalculate, ITransientDependency
    {

    }
    ```
    - TaxCalculator类只公开ITaxCalculator接口。这意味着只能注入ITaxCalculator，但不能注入TaxCalculator或ICalculator。
1. 注册服务约定
    - 默认情况下，类本身可以注入。
    - 默认接口默认可以注入。默认接口由命名约定。
        - 默认接口名去掉最前面的I为实现类名的后缀或全部
        - 如: ICalculator和ITaxCalculator是 TaxCalculator 类 的默认接口，但ICanCalculate不是。
1. 以上规则结合
    ```
    [Dependency(ReplaceServices = true)]
    [ExposeServices(typeof(ITaxCalculator))]
    public class TaxCalculator : ITaxCalculator, ITransientDependency
    {

    }
    ```
1. 取消某个类自动注册
    - 为类定义 `DisableConventionalRegistrationAttribute` 特性
#### 手动注册
1. 在模块类的ConfigureServices方法中进行
1. 一般用于自定义工厂方法或特殊单例实例
    ```
    public class BlogModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            //Register an instance as singleton
            context.Services.AddSingleton<TaxCalculator>(new TaxCalculator(taxRatio: 0.18));

            //Register a factory method that resolves from IServiceProvider
            context.Services.AddScoped<ITaxCalculator>(
                sp => sp.GetRequiredService<TaxCalculator>()
            );
        }
    }
    ```
#### 替换服务
- 用于替换现有的服务（由 ABP 框架或其他依赖模块定义）
- 有两种方式
    1. 使用ABP 框架的Dependency特性, 并设置ReplaceServices = true，如上面自动注册中描述。
    1. 使用IServiceCollection.Replace微软依赖注入库的方法
        ```
        public class MyModule : AbpModule
        {
            public override void ConfigureServices(ServiceConfigurationContext context)
            {
                //Replacing the IConnectionStringResolver service
                context.Services.Replace(ServiceDescriptor.Transient<IConnectionStringResolver, MyConnectionStringResolver>());
            }
        }
        ```
### 注入依赖
1. 构造函数注入, 为优先方法
    ```
    public class TaxAppService : ApplicationService
    {
        private readonly ITaxCalculator _taxCalculator;

        public TaxAppService(ITaxCalculator taxCalculator)
        {
            _taxCalculator = taxCalculator;
        }

        public void DoSomething()
        {
            //...use _taxCalculator...
        }
    }
    ```
1. 属性注入
    - Microsoft 依赖注入库不支持属性注入。但是，ABP 可以与第 3 方 DI 提供程序（例如Autofac）集成以使属性注入成为可能
    ```
    public class MyService : ITransientDependency
    {
        public ILogger<MyService> Logger { get; set; }

        public MyService()
        {
            Logger = NullLogger<MyService>.Instance;
        }

        public void DoSomething()
        {
            //...use Logger to write logs...
        }
    }
    ```
    - 属性注入在构造时不能使用
    - 应在构造方法设置后备服务
1. 从 IServiceProvider 解析服务
    - 将 IServiceProvider 通过构造方法注入
    ```
    public class MyService : ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public MyService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void DoSomething()
        {
            var taxCalculator = _serviceProvider.GetService<ITaxCalculator>();
            //...
        }
    }
    ```
    ### 高级功能
    #### IServiceCollection.OnRegistred 事件
    1. 定义
        ```
        public static void OnRegistred(this IServiceCollection services, Action<IOnServiceRegistredContext> registrationAction)
        {
            GetOrCreateRegistrationActionList(services).Add(registrationAction);
        }
        ```
        - 这是一个扩展方法, 当服务注册到容器时会被激发
        - 应在模块类的 PreConfigureServices 方法中调用. (此方法会在所有模块类ConfigureServices方法执行之前执行)
        - 这是扩展Abp框架非常有用的机制(针对某一特定类型服务做特定处理, 如 拦截器)
    1. 一般扩展模式
        1. 定义特定类型确定方式(如实现某个接口, 定义了某个特性等, 接口一般以 `Contributor`, `Definition` 结尾)
        1. 定义一个 特定类型集合类 MyServiceTypeList, 继承自`TypeList<ServiceType>`
        1. 定义一个 特定类型选项类, 其中包含 特定类型集合 属性
        1. 定义一个使用类, 并自动或手动注册到容器, 此类包含 
            - 特定类型选项类 属性. (通过构造方法注入)
            - 运行方法
                - 遍历 特定类型选项类 的 特定类型集合 属性
                - 对 特定类型 进行操作
        1. 在模块类的 `PreConfigureServices`方法 调用 `IServiceCollection.OnRegistred` 判断注册的服务类型是否是特定类型, 如果是，则确保将此类型添加到 特定类型选项 的 特定类型集合 中
    1. 扩展示例(以DataSeeder示例):
        1. 定义特定类型接口 IDataSeedContributor
            ```
            public interface IDataSeedContributor
            {
                Task SeedAsync(DataSeedContext context);
            }
            ```
        1. 定义特定类型集合类 `DataSeedContributorList`
            ```
            public class DataSeedContributorList : TypeList<IDataSeedContributor>
            {

            }
            ```
        1. 定义一个 特定类型选项类 `AbpDataSeedOptions`
            ```
            public class AbpDataSeedOptions
            {
                public DataSeedContributorList Contributors { get; }

                public AbpDataSeedOptions()
                {
                    Contributors = new DataSeedContributorList();
                }
            }
            ```
        1. 定义一个使用类 `DataSeeder`
            ```
            public class DataSeeder : IDataSeeder, ITransientDependency
            {
                protected IServiceScopeFactory ServiceScopeFactory { get; }
                protected AbpDataSeedOptions Options { get; }

                public DataSeeder(
                    IOptions<AbpDataSeedOptions> options,
                    IServiceScopeFactory serviceScopeFactory)
                {
                    ServiceScopeFactory = serviceScopeFactory;
                    Options = options.Value;
                }

                [UnitOfWork]
                public virtual async Task SeedAsync(DataSeedContext context)
                {
                    using (var scope = ServiceScopeFactory.CreateScope())
                    {
                        foreach (var contributorType in Options.Contributors)
                        {
                            var contributor = (IDataSeedContributor) scope
                                .ServiceProvider
                                .GetRequiredService(contributorType);

                            await contributor.SeedAsync(context);
                        }
                    }
                }
            }
            ```
        1. AbpDataModule 模块类注册 `IDataSeedContributor`
            ```
            public override void PreConfigureServices(ServiceConfigurationContext context)
            {
                AutoAddDataSeedContributors(context.Services);
            }

            private static void AutoAddDataSeedContributors(IServiceCollection services)
            {
                var contributors = new List<Type>();

                services.OnRegistred(context =>
                {
                    if (typeof(IDataSeedContributor).IsAssignableFrom(context.ImplementationType))
                    {
                        contributors.Add(context.ImplementationType);
                    }
                });

                services.Configure<AbpDataSeedOptions>(options =>
                {
                    options.Contributors.AddIfNotContains(contributors);
                });
            }
            ```
    1. 使用扩展
        1. 在要使用的服务中通过 构造方法 注入 使用类
        1. 调用使用类的运行方法
        1. 示例:
            ```
            public class MultiDbDbMigrationService : ITransientDependency
            {
                public ILogger<MultiDbDbMigrationService> Logger { get; set; }

                private readonly IDataSeeder _dataSeeder;
                private readonly IEnumerable<IMultiDbDbSchemaMigrator> _dbSchemaMigrators;
                private readonly ITenantRepository _tenantRepository;
                private readonly ICurrentTenant _currentTenant;

                public MultiDbDbMigrationService(
                    IDataSeeder dataSeeder,
                    IEnumerable<IMultiDbDbSchemaMigrator> dbSchemaMigrators,
                    ITenantRepository tenantRepository,
                    ICurrentTenant currentTenant)
                {
                    _dataSeeder = dataSeeder;
                    _dbSchemaMigrators = dbSchemaMigrators;
                    _tenantRepository = tenantRepository;
                    _currentTenant = currentTenant;

                    Logger = NullLogger<MultiDbDbMigrationService>.Instance;
                }

                public async Task MigrateAsync()
                {
                    // ...
                    await SeedDataAsync();
                    // ...

                    
                }                

                private async Task SeedDataAsync(Tenant tenant = null)
                {
                    Logger.LogInformation($"Executing {(tenant == null ? "host" : tenant.Name + " tenant")} database seed...");

                    await _dataSeeder.SeedAsync(new DataSeedContext(tenant?.Id)
                        .WithProperty(IdentityDataSeedContributor.AdminEmailPropertyName, IdentityDataSeedContributor.AdminEmailDefaultValue)
                        .WithProperty(IdentityDataSeedContributor.AdminPasswordPropertyName, IdentityDataSeedContributor.AdminPasswordDefaultValue)
                    );
                }
            }
            ```