# 分布式事件总线
分布式事件总线系统允许发布和订阅跨应用/服务边界传输的事件. 可以使用分布式事件总线在微服务或应用程序之间异步发送和接收消息.
## (生产者)发布事件
分布式事件发布方式有两种
1. 注入 IDistributedEventBus 发布分布式事件
    - 一般在服务中使用
    - 该服务需要通过依赖注入
    - 示例
        ```
        public class MyService : ITransientDependency
        {
            private readonly IDistributedEventBus _distributedEventBus;

            public MyService(IDistributedEventBus distributedEventBus)
            {
                _distributedEventBus = distributedEventBus;
            }
            
            public virtual async Task ChangeStockCountAsync(Guid productId, int newCount)
            {
                await _distributedEventBus.PublishAsync(
                    new StockCountChangedEto
                    {
                        ProductId = productId,
                        NewCount = newCount
                    }
                );
            }
        }
        ```
        - StockCountChangedEto 为事件传输对象, 有以下约定
            1. 以Eto为后缀
            1. 为简单类型
            1. 必须可序列化
            1. 避免循环引用、多态性、私有设置器
            1. 提供默认（空）构造函数(在有非默认构造函数时)
            1. 默认发布的事件名称为 事件传输对象类 的命名
            1. 可以通过在 事件传输对象 定义 EventName 特性来指定事件名称
            - 示例
                ```
                [EventName("MyApp.Product.StockChange")]
                public class StockCountChangedEto
                {
                    public Guid ProductId { get; set; }
                    
                    public int NewCount { get; set; }
                }
                ```
1. 领域对象 发布分布式事件
    1. 聚合根
        - 聚合根默认实现了 IGeneratesDomainEvents 接口, 提供 AddDistributedEvent 方法
        - 示例
            ```
            public class Product : AggregateRoot<Guid>
            {
                public string Name { get; set; }
                
                public int StockCount { get; private set; }

                private Product() { }

                public Product(Guid id, string name)
                    : base(id)
                {
                    Name = name;
                }

                public void ChangeStockCount(int newCount)
                {
                    StockCount = newCount;
                    
                    // 添加准备发布的分布式事件
                    AddDistributedEvent(
                        new StockCountChangedEto
                        {
                            ProductId = Id,
                            NewCount = newCount
                        }
                    );
                }
            }
            ```

    1. 普通实体
        - 需要实现 IGeneratesDomainEvents 接口
        - 不建议为非聚合要实现 IGeneratesDomainEvents
            - 某些数据库驱动不支持(如mongodb)
    1. 事件发布点
        - **不是**在调用 AddDistributedEvent 方法时发布, 而是在聚合根对象保存（创建、更新或删除）到数据库中时发布
        - 对于 EF Core, 在 DbContext.SaveChanges 时发布
        - 对于 MongoDB, 在调用存储库的 InsertAsync、UpdateAsync、DeleteAsync 方法时发布

## (消费者)订阅分布式事件
- 在模块化开发时订阅分布式事件非常简单, 实现 `IDistributedEventHandler<TEvent>` 接口即可
- 示例
    ```
    public class MyHandler
        : IDistributedEventHandler<StockCountChangedEto>,
          ITransientDependency
    {
        public async Task HandleEventAsync(StockCountChangedEto eventData)
        {
            var productId = eventData.ProductId;
        }
    }
    ```
    - MyHandler 为 StockCountChangedEto 分布式事件的事件处理器, 会自动订阅 StockCountChangedEto 并通过 HandleEventAsync 进行事件处理
    - 事件处理器 必须注册到DI中, 实现 ITransientDependency 确保自动注入
    - 如果是分布式消息代理，例如 RabbitMQ, 框架 会自动订阅消息代理上的事件，获取消息，执行处理程序。
    - 如果事件处理程序成功执行（不抛出任何异常），它会向消息代理发送确认(ACK)
    - 可以在事件处理器中注入任何你想使用的服务并执行任意逻辑
    - 单个事件处理器可以订阅多个事件(实现多个接口)
    - 如果在事件处理方法中执行数据库操作并使用仓储, 则需要在工作单元下进行, 有以下方式可以实现工作单元
        1. 将处理方法定义为 virtual 并添加 `[UnitOfWork]` 特性(建议)
        1. 使用 IUnitOfWorkManager 创建一个工作单元范围
    
## 框架预定义的分布式事件
- 框架为实体定义了用于 创建、更新和删除操作的分布式事件, 在配置启用后会自动发布
    1. `EntityCreatedEto<T>` 在创建类型实体T时发布
    1. `EntityUpdatedEto<T>` 在更新类型的实体T时发布
    1. `EntityDeletedEto<T>` 在删除类型实体T时发布
    - T 为事件传输对象
### 预定义分布式事件传输对象
1. 在配置预定义分布式事件时指定, 如果不指定, 框架为使用 默认事件传输对象 EntityEto
    - EntityEto 只包含两个属性
        1. EntityType string 实体类型, 为实体类的全名
        1. KeysAsString string 已更改实体的主键
            - 如果只有一个键，则此属性将是主键值。对于复合键，将包含用逗号分隔的所有键
    - **不建议** 使用 `IDistributedEventHandler<EntityUpdatedEto<EntityEto>>` 来订阅事件
    - 应为每个需要的实体事件定义 Eto
### 配置预定义分布式事件
- 在模块类的 ConfigureServices 方法中配置事件
    - 配置选项 AbpDistributedEntityEventOptions
        - AutoEventSelectors 自动事件选择器用来确定为哪些实体类型启用事件
        - EtoMappings 用于定义实体类型对应的事件传输对象
    - 示例
        ```
        Configure<AbpDistributedEntityEventOptions>(options =>
        {
            //为所有实体启用事件
            options.AutoEventSelectors.AddAll();

            //为指定实体类型启动事件
            options.AutoEventSelectors.Add<IdentityUser>();

            //为指定名称空间(含子名称空间)下的实体启用事件
            options.AutoEventSelectors.AddNamespace("Volo.Abp.Identity");

            //定制类型选择器, 返回true表示该实体类型启用事件
            options.AutoEventSelectors.Add(
                type => type.Namespace.StartsWith("MyProject.")
            );
        });
        ```
        - 自动事件选择器可以有多个, 只要任意一个包含实体, 则为该实体启用事件
    - 指定事件传输对象示例
        ```
        Configure<AbpDistributedEntityEventOptions>(options =>
        {
            options.AutoEventSelectors.Add<Product>();
            options.EtoMappings.Add<Product, ProductEto>();
        });
        ```
        - 实体 映射到 事件传输对象 是通过 对象到对象映射系统 实现
        - 需要配置映射

### 订阅预定义的分布式事件
- 与订阅常规分布式事件相同
- 示例
    ```
    public class MyHandler : 
        IDistributedEventHandler<EntityUpdatedEto<ProductEto>>,
        ITransientDependency
    {
        public async Task HandleEventAsync(EntityUpdatedEto<ProductEto> eventData)
        {
            var productId = eventData.Entity.Id;
            //TODO
        }
    }
    ```
    - MyHandler 实现了 `IDistributedEventHandler<EntityUpdatedEto<ProductEto>>`

### 基础设施层
- 框架提供了几种开箱即用的分布式事件基础设施
    1. LocalDistributedEventBus 本地分布式事件总线
        - 默认
    1. AzureDistributedEventBus 使用Azure Service Bus实现分布式事件总线
    1. RabbitMqDistributedEventBus 使用RabbitMQ实现分布式事件总线
    1. KafkaDistributedEventBus 使用Kafka实现分布式事件总线
    1. RebusDistributedEventBus使用Rebus实现分布式事件总线
- Kafka
    1. 配置
        ```
        "Kafka": {
            "Connections": {
            "Default": {
                "BootstrapServers": "192.168.11.80:9092"
            }
            },
            "EventBus": {
            "GroupId": "DataCategory",
            "TopicName": "DemoTopic"
            }
        }
        ```
        - GroupId 配置应用程序的名称, 每个应用原则上应唯一
        - TopicName 是topic名称, 发布者和订阅者必须相同
- 规范
    1. 不允许在业务中直接依赖 具体基础设施
    1. 在承载模块的启动应用中应配置应用系统使用的基础设置
        - 如果不配置, 默认使用 LocalDistributedEventBus
        - LocalDistributedEventBus 基于内存的本地事务, 不支持跨进程

    

## 规范
1. 模块发布分布式事件时, 应为模块定义一个事件传输对象共享层, 命名为 Domain.Eto, 该层一般由Domain层引用
    - 依赖 Volo.Abp.EventBus
    - 依赖 Volo.Abp.Ddd.Domain, 可选, 在启用预定义分布式事件时建议加上
1. 事件传输对象须以Eto为后缀
1. 模块订阅分布式事件时, 应用在处理类所在层引用事件发布模块的 事件传输对象共享层
