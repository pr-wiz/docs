# 模块/微服务依赖模块/微服务
## 在oidc管理后台配置好以下数据(基础数据预处理, 与模块依赖本身没有关系)
1. 模块Api资源
1. 被依赖模块Api资源
1. 模块服务客户端
    - 允许使用的授权类型:  client_credentials
    - api资源范围: 被依赖模块Api资源
    - 权限: 被依赖模块方法的权限
1. 模块Swagger客户端(后台开发测试验证用)
    - 普通Swagger客户端
    - api资源范围: 添加被依赖模块Api资源
## 在依赖模块和被依赖模块配置
### 统一配置(开发环境规范化, 与模块依赖本身没有关系, 后面会由代码生成工具统一标化)
- appsettings.json
    1. 身份认证: 统一(如开发环境为 http://devaspnetcore01.wiz.top:7990/)
    1. Redis: 统一(如开发环境为 devredis01.wiz.top:9736)
    1. Redis 持久Key: 统一(如开发环境统一为 Portal-Protection-Keys)
- 启动模块类 
    1. 在 app.UseAuthentication(); 后调用 app.UseAbpClaimsMap();
### OIDC配置
1. 配置本模块 Swagger客户端和 Audience(Api资源), 配置正确的OIDC资源

### 引用被依赖模块接口
1. 依赖模块的 Application 层 引用 被依赖模块的 Application.Contracts 层 nuget 包
1. 依赖模块的 Application 层 的模块添加 被依赖模块的 Application.Contracts 层的模块类 依赖
    ```
    [DependsOn(typeof(XxxApplicationContractsModule))]
    ```
1. 在需要调用的被依赖模块服务的 服务类中 注入 需要的接口, 一般位于依赖模块的 Application 层
    ```
    private readonly IDataCategoryAppService _dataCategoryAppService;

    public SampleAppService(IDataCategoryAppService dataCategoryAppService)
    {
        _dataCategoryAppService = dataCategoryAppService;
    }
    ```
1. 使用被依赖模块的服务, 同普通开发一样使用

## 承载依赖模块/微服务的站点配置 一般为 HttpApi.Host 层
### 依赖被依赖模块的动态客户API
- 引用 被依赖模块 HttpApi.Client nuget包
- 模块类添加对被依赖模块 HttpApi.Client 层模块类的依赖
    ```
    [DependsOn(typeof(XxxHttpApiClientModule))]
        ```
### 配置被依赖模块的远程地址
- 在 appsettings.json 中配置
    ```
     "RemoteServices": {
        "Default": {
            "BaseUrl": "http://devaspnetcore02.wiz.top:7900/"
        }
    }
    ```
    - Default 为默认的远程地址
    - 如果模块没有找到远程地址, 会自动使用 default 配置

