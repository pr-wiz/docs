# 模块升级指南
## 约定
1. 版本暂定为0.3.0-Alpha
1. 要升级项目已从 dev 分支创建升级分支: lichunxiao/5.x, 所有的操作都在 升级分支: lichunxiao/5.x 下操作
1. 在外网进行升级
1. VS2022
1. 严格按步骤操作
## 步骤
### 以下操作不用VS打开
1. 创建一新版本的同名模块空源码
  - abp new Wiz.XXXX -t module --no-ui
1. 为空项目添加 Eaf 模块
  - abphelper module add Wiz.Eaf -soalch -v 0.3.0-Alpha
  - abphelper module add Wiz.Eaf -e -v 0.3.0-Alpha
1. 修改 common.props 将VersionPrefix设置为 0.3.0, 将 VersionSuffix 设置为 Alpha， 修改 PackageReference Include="Fody" Version="6.5.3"

1. 以下使用使用脚本命令完成
    1. 删除升级项目的host, 直接复制空项目的host三个项目到升级项目
    1. 删除升级项目的test(除Application.Tests),直接复制空项目的host项目到升级项目
    1. 删除升级项目的xxx.MongoDB,直接复制空项目的xxx.MongoDB到升级项目
    1. 删除升级项目的database目录,直接复制空项目的database到升级项目
    1. 复制空项目的xxx.Installer到升级项目
    1. 将空源码中的每一个项目的如下两个文件复制到要升级的同项目：
        .abppkg.json
        .csproj
    1. 复制空项目的 Application.Contracts 的 XXXRemoteServiceConsts.cs 文件到 要升级的项目
    1. 删除 要升级项目 EntityFrameworkCore 中的 XXXModelBuilderConfigurationOptions 类
    1. 将空项目 Application.Contracts 中的文件 XXXRemoteServiceConsts.cs 复制到 要升级的项目
    1. 复制空项目的以下文件到要升级的项目
        - docker-compose.migrations.yml
        - docker-compose.override.yml
        - docker-compose.yml
        - XXX.abpmdl.json
        - XXX.abpsln.json
        - XXX.sln
    ```
    ;@echo off
    ; 设置部分
    set module_name="Wiz.FileManagement"
    set module_prefix="FileManagement"
    set empty_base="C:\Users\Jack Lee\abp5"
    set target_base="D:\work\git\abp\demo\modules\abp4"


    set replace_dir=host
    del /s /q %target_base%\%replace_dir%
    rd  /s /q %target_base%\%replace_dir%
    xcopy /S /Y %empty_base%\%replace_dir%\ %target_base%\%replace_dir%\ /e
    set replace_dir=database
    del /s /q %target_base%\%replace_dir%
    rd  /s /q %target_base%\%replace_dir%
    xcopy /S /Y %empty_base%\%replace_dir%\ %target_base%\%replace_dir%\ /e

    set replace_dir=%module_name%.MongoDB
    del /s /q %target_base%\src\%replace_dir%
    rd  /s /q %target_base%\src\%replace_dir%
    xcopy  /S /Y %empty_base%\src\%replace_dir%\ %target_base%\src\%replace_dir%\ /e

    xcopy  /S /Y %empty_base%\src\%module_name%.Installer\ %target_base%\src\%module_name%.Installer\ /e

    set replace_dir=%module_name%.Domain.Tests
    del /s /q %target_base%\test\%replace_dir%
    rd  /s /q %target_base%\test\%replace_dir%
    xcopy %empty_base%\test\%replace_dir%\ %target_base%\test\%replace_dir%\ /e

    set replace_dir=%module_name%.EntityFrameworkCore.Tests
    del /s /q %target_base%\test\%replace_dir%
    rd  /s /q %target_base%\test\%replace_dir%
    xcopy  /S /Y  %empty_base%\test\%replace_dir%\ %target_base%\test\%replace_dir%\ /e

    set replace_dir=%module_name%.HttpApi.Client.ConsoleTestApp
    del /s /q %target_base%\test\%replace_dir%
    rd  /s /q %target_base%\test\%replace_dir%
    xcopy %empty_base%\test\%replace_dir%\ %target_base%\test\%replace_dir%\ /e

    set replace_dir=%module_name%.MongoDB.Tests
    del /s /q %target_base%\test\%replace_dir%
    rd  /s /q %target_base%\test\%replace_dir%
    xcopy %empty_base%\test\%replace_dir%\ %target_base%\test\%replace_dir%\ /e

    set replace_dir=%module_name%.TestBase
    del /s /q %target_base%\test\%replace_dir%
    rd  /s /q %target_base%\test\%replace_dir%
    xcopy %empty_base%\test\%replace_dir%\ %target_base%\test\%replace_dir%\ /e

    set project_name=%module_name%.Application
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    set project_name=%module_name%.Application.Contracts
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%module_prefix%RemoteServiceConsts.cs %target_base%\src\%project_name%
    set project_name=%module_name%.Domain
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    set project_name=%module_name%.Domain.Shared
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    set project_name=%module_name%.EntityFrameworkCore
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    del /q %target_base%\src\%project_name%\EntityFrameworkCore\%module_prefix%ModelBuilderConfigurationOptions.cs
    set project_name=%module_name%.HttpApi
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    set project_name=%module_name%.HttpApi.Client
    copy /y %empty_base%\src\%project_name%\%project_name%.abppkg.json %target_base%\src\%project_name%
    copy /y %empty_base%\src\%project_name%\%project_name%.csproj %target_base%\src\%project_name%
    set project_name=%module_name%.Application.Tests
    copy /y %empty_base%\test\%project_name%\%project_name%.abppkg.json %target_base%\test\%project_name%
    copy /y %empty_base%\test\%project_name%\%project_name%.csproj %target_base%\test\%project_name%

    copy /y %empty_base%\docker-compose.migrations.yml %target_base%
    copy /y %empty_base%\docker-compose.override.yml %target_base%
    copy /y %empty_base%\docker-compose.yml %target_base%
    copy /y %empty_base%\%module_name%.abpmdl.json %target_base%
    copy /y %empty_base%\%module_name%.abpsln.json %target_base%
    copy /y %empty_base%\%module_name%.sln %target_base%

    pause
    ```
### 以下操作用VS打开要升级的项目进行
1. 添加替换.csproj文件后丢失的nuget依赖
1. 修改 XXXDbContextModelCreatingExtensions 扩展类, 
    - 移除 ConfigureXXX 方法中 Action<XXXModelBuilderConfigurationOptions> 参数 及参数的使用代码
    - 替换 options. 为 XXXDbProperties.Db
### 编译, 修复错误
1. 移除测试项目中 Sample 的错误
