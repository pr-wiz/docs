# API Host应用程序开发教程 - 第二章: 动态JavaScript代理和本地化

## 动态JavaScript代理
通常在 JavaScript 端通过AJAX调用**HTTP API**端点. 你可以使用 `$.ajax` 或其他工具来调用端点. 但是ABP提供了更好的方法.

ABP**动态**为所有API端点创建 [JavaScript代理](https://docs.abp.io/zh-Hans/abp/latest/UI/AspNetCore/Dynamic-JavaScript-Proxies). 所以你可以像调用**Javascript本地方法**一样使用任何**端点**.

### 在开发者控制台中进行测试
你可以在自己喜欢的浏览器的**开发者控制台**轻松的测试JavaScript代理. 运行应用程序,打开浏览器的**开发者人员工具**(快捷键通常是F12),切换到**控制台**选项卡,输入以下代码然后按回车:
```
acme.bookStore.books.book.getList({}).done(function (result) { console.log(result); });
```
- `acme.bookStore.books` 是 `BookAppService` 的命令空间转换成[小驼峰](https://en.wikipedia.org/wiki/Camel_case)形式.
- `book` 是 `BookAppService` 的约定名称(删除`AppService`后缀并且转换为小驼峰).
- `getList` 是 `CrudAppService` 基类定义的 `GetListAsync` 方法的约定名称(删除`Async`后缀并且转换为小驼峰).
- {} 参数将空对象发送到 `GetListAsync` 方法,该方法通常需要一个类型为 `PagedAndSortedResultRequestDto` 的对象,该对象用于将分页和排序选项发送到服务器(所有属性都是可选的,具有默认值. 因此你可以发送一个空对象).
- `getList` 函数返回一个 `promise`. 你可以传递一个回调到 `then`(或`done`)函数来获取从服务器返回的结果.
运行该代码会产生以下输出:

![avatar](images/bookstore-javascript-proxy-console.png)

你可以看到服务端返回的 **图书列表**. 你也可以在开发者人员工具的 **网络** 选项卡查看客户端到服务端的通信:

![avatar](images/bookstore-getlist-result-network.png)

让我们使用 `create` 函数创建一本书:
```
acme.bookStore.books.book.create({ 
        name: 'Foundation', 
        type: 7, 
        publishDate: '1951-05-24', 
        price: 21.5 
    }).then(function (result) { 
        console.log('successfully created the book with id: ' + result.id); 
    });
```
您应该在控制台中看到类似以下的消息:
```
successfully created the book with id: 439b0ea8-923e-8e1e-5d97-39f2c7ac4246
```
检查数据库中的 `Books` 表你会看到新的一行. 你可以自己尝试使用 `get`, `update` 和 `delete` 函数.

我们将利用这些动态代理功能在接下来的章节来与服务器通信.

### Abp动态脚本
- abp.localization 本地化脚本
    ```
    var l = abp.localization.getResource('BookStore');
    var name = l('Name');
    var locale = abp.localization.currentCulture.name
    ```
    - abp.localization.getResource 获取一个函数,该函数用于使用服务器端定义的相同JSON文件对文本进行本地化. 通过这种方式你可以与客户端共享本地化值.
- abp.libs.datatables datatables脚本
    ```
    var dataTable = $('#BooksTable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            ajax: abp.libs.datatables.createAjax(acme.bookStore.books.book.getList),
            columnDefs: [...]
        }));
    ```
    - abp.libs.datatables.normalizeConfiguration是另一个辅助方法.不是必须的, 但是它通过为缺少的选项提供常规值来简化数据表配置.
    - abp.libs.datatables.createAjax是帮助ABP的动态JavaScript API代理跟Datatable的格式相适应的辅助方法.

## 本地化
开始的UI开发之前,我们首先要准备本地化的文本(这是你通常在开发应用程序时需要做的).

本地化文本位于 `Acme.BookStore.Domain.Shared` 项目的 `Localization/BookStore` 文件夹下:

![avatar](images/bookstore-localization-files-v2.png)

打开 `en.json` (英文翻译)文件并更改内容,如下所示:
```
{
  "Culture": "en",
  "Texts": {
    "Menu:Home": "Home",
    "Welcome": "Welcome",
    "LongWelcomeMessage": "Welcome to the application. This is a startup project based on the ABP framework. For more information, visit abp.io.",
    "Menu:BookStore": "Book Store",
    "Menu:Books": "Books",
    "Actions": "Actions",
    "Close": "Close",
    "Delete": "Delete",
    "Edit": "Edit",
    "PublishDate": "Publish date",
    "NewBook": "New book",
    "Name": "Name",
    "Type": "Type",
    "Price": "Price",
    "CreationTime": "Creation time",
    "AreYouSure": "Are you sure?",
    "AreYouSureToDelete": "Are you sure you want to delete this item?",
    "Enum:BookType:0": "Undefined",
    "Enum:BookType:1": "Adventure",
    "Enum:BookType:2": "Biography",
    "Enum:BookType:3": "Dystopia",
    "Enum:BookType:4": "Fantastic",
    "Enum:BookType:5": "Horror",
    "Enum:BookType:6": "Science",
    "Enum:BookType:7": "Science fiction",
    "Enum:BookType:8": "Poetry"
  }
}
```
- 本地化关键字名称是任意的. 你可以设置任何名称. 对于特定的文本类型,我们更喜欢遵循一些约定:
    - 为按钮项添加 `Menu:` 前缀.
    - 使用 `Enum:<enum-type>:<enum-value>` 命名约定来本地化枚举成员. 当您这样做时ABP可以在某些适当的情况下自动将枚举本地化.

如果未在本地化文件中定义文本,则文本将**回退**到本地化键(作为ASP.NET Core的标准行为).

> ABP本地化系统建立在[ASP.NET Core标准本地化系统](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/localization)之上,并以多种方式进行了扩展. 有关详细信息请参见[本地化文档](https://docs.abp.io/zh-Hans/abp/latest/Localization).

## 下一章
参阅教程的[下一章](no-ui-api-host-part3.md).