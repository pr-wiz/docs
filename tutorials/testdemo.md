# 基于Abp创建无界面模块Demo
## 要求
1. 领域对象 Person
    - 业务属性集
        1. 姓名(不允许为空, 唯一约束 最大长度20个字节)
        1. 性别(枚举类型: Man/Female)
        1. 年龄
    - 为聚合根对象, Id为Guid
    - 实现多租户
    - 具有完整的审计属性
        1. IsDeleted
        1. DeleterId
        1. DeletionTime
        1. LastModificationTime
        1. LastModifierId
        1. CreationTime
        1. CreatorId
1. 为 Person 属性提供 本地化资源(中/英)
1. 为 Person 提供仓储接口
1. 使用SqlServer数据库, 使用默认的连接信息
1. 使用 Eaf 框架
1. 应用服务层
    - 提供根据 姓名 查找 Person 的方法
1. 初始数据
    - 创建一条姓名为本人的 Person 初始数据
1. 单元测试
    - 应该存在一条姓名为本人的 Person 数据
    - 不应该保存 姓名 相同的 Person

## 通过标准
1. 通过单元测试
1. 能够正常运行, 并能通过Swagger进行测试验证
    - 没有登录之前
## 参考
1. [开发环境准备](https://gitee.com/pr-wiz/docs/blob/master/getting-started/dev-env.md)
1. [无界面模块](https://gitee.com/pr-wiz/docs/blob/master/tutorials/no-ui-module.md)
1. [Eaf框架](https://gitee.com/pr-wiz/docs/blob/master/eaf.md)