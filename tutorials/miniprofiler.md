# 在 Aspnetcore API 中使用 MiniProfiler 
## 介绍
- MiniProfiler 是一个用于分析应用程序的库和 UI。
- 查看时间花在何处、运行了哪些查询以及添加的任何其他自定义计时
- MiniProfiler 可帮助调试问题并优化性能。

- MiniProfiler 会公布三个端口:
    1. {RouteBasePath}/results-index
    1. {RouteBasePath}/results-list
    1. {RouteBasePath}/results

## 配置和实现
1. 在HttpApi.Host项目中添加以下包引用
    - Install-Package MiniProfiler.AspNetCore.Mvc
    - Install-Package MiniProfiler.EntityFrameworkCore (如果使用 EntityFrameworkCore)
1. 在 AppSettings.json 配置文件中添加以下配置, 用于控制是否启用 MiniProfiler (可选)
    ```
    "MiniProfiler": {
        "Enabled": true,
        "RouteBasePath": "/profiler"
    }
    ```
    - 是否启用 MiniProfiler, 默认为 false
    - MiniProfiler 的 路由基础路径, 默认为 /profiler
1. 在 `ConfigureServices(IServiceCollection services)` 方法中加入
    ```
    var enableMiniProfiler = Configuration.GetValue<bool>("MiniProfiler:Enabled");
    if (enableMiniProfiler)
    {
        var routeBasePath = Configuration.GetValue<string>("MiniProfiler:RouteBasePath");
        if (string.IsNullOrEmpty(routeBasePath))
        {
            routeBasePath = "/profiler";
        }
        else
        {
            routeBasePath = $"/{routeBasePath.TrimStart('/')}";
        }
        services.AddMiniProfiler(options =>
        {
            options.RouteBasePath = routeBasePath;
            (options.Storage as StackExchange.Profiling.Storage.MemoryCacheStorage).CacheDuration = TimeSpan.FromMinutes(60);

        })
        // 如果使用 EntityFrameworkCore
        // .AddEntityFramework()
        ;
    }
    ```
1. 在 `ConfigureServices(IServiceCollection services)` 方法的`app.UseMvc();`前加入
    ```
    var enableMiniProfiler = Configuration.GetValue<bool>("MiniProfiler:Enabled");
    if (enableMiniProfiler)
    {
        app.UseMiniProfiler();
    }
    app.UseMvc();
    ```
    - 务必在 `app.UseMvc();` 之前

## 使用
1. http://localhost:58117/profiler/results-index
    - 以UI的方法返回最近的分析列表
1. http://localhost:58117/profiler/results-list
    - 以json格式返回最近的分析列表
1. http://localhost:58117/profiler/results
    - 以UI的方式显示最后一次分析的视图
1. http://localhost:58117/profiler/results?id=ae0da733-3762-476a-958d-a23b2668c35e
    - 以UI的方式显示指定分析的视图
