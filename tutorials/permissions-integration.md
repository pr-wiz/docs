# 权限集成
## 集成方式
1. 通过数据库集成
1. 通过远程服务集成
## 通过数据库集成
1. 在承载微服务的站点 依赖 Volo.Abp.PermissionManagement.EntityFrameworkCore
    ```
    [DependsOn(typeof(AbpPermissionManagementEntityFrameworkCoreModule))]
    ```