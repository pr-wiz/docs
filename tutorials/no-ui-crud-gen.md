# 自动生成CRUD代码教程

## 前置条件
1. 安装代码生成工具 **AbpHelper CLI tool**
    创建名为 `Wiz.ModuleDemo` 的新解决方案.
    ```
    dotnet tool install EasyAbp.AbpHelper -g
    ```
1. 已创建模块方案和要生成代码的实体
    参考[NO-UI 模块开发教程](no-ui-module.md)


## 根据实体生成 各层CRUD 实现
- 聚合根, 如: 根据 `Person` 实体生成
    ```
    abphelper gen crud --separate-dto --skip-db-migrations --skip-ui --skip-view-model Person
    ```
- 非聚合根实体(生成独立仓储), 如: 根据 `Person` 实体生成
    ```
    abphelper gen crud --separate-dto --skip-db-migrations --skip-ui --skip-view-model --skip-test --no-overwrite Person
    ```

- 非聚合根实体(不生成独立仓储), 如: 根据 `Person` 实体生成
    ```
    abphelper gen crud --separate-dto --skip-db-migrations --skip-ui --skip-view-model --skip-custom-repository --skip-test --no-overwrite Person
    ```
    - 生成后调整(无独立仓储时需要)        
        1. 调整Dto
            - 输出实体Dto移除聚合根Id属性
            - 输入实体Dto移除聚合根Id属性
            - 输出聚合根Dto添加输出实体Dto列表属性
            - 输入聚合根Dto添加输入实体Dto列表属性并初始化		
        1. 调整实体带参构造函数为 `protected internal `, 防止被外部实例化       
        1. 聚合根查询扩展类 XXXEfCoreQuerableExtensions 的 IncludeDetails 添加包含明细集合
            ```
            return queryable
                    .Include(x => x.XXX);
            ```
        1. 聚合根仓储类添加 `WithDetailsAsync` 方法：
            ```
            public override async Task<IQueryable<XXX>> WithDetailsAsync()
            {
                return (await GetQueryableAsync()).IncludeDetails();
            }
            ```
        
        1. DbContextModelCreatingExtensions 中在对应聚合根下添加关系
            `b.HasMany(x => x.Persons).WithOne().HasForeignKey(x => x.YYYId).IsRequired();`
        1. IXXXDbContext 接口删除 `DbSet<Person> Persons` 属性
        1. XXXDbContext 类删除 `DbSet<Person> Persons` 属性
        1. 删除应用服务接口, 不应提供独立服务
        1. 删除应用服务, 不应提供独立服务

1. 最终的文件夹/文件结构应该如下所示:

![avatar](images/PersonCrud.png)

## 快速安装引用 遵照Abp规范开发的模块
1. 模块名称
    - 模块名称为模块nuget包名称的共用前缀, 如: Wiz.Easc.Domain 包的模块名称为: Wiz.Easc
1. 安装模块选项
    - `-v, --version <version>`                        增定要依赖的模块版本
    - `--module-company-name <module-company-name>`    模块公司名称, **一般不需要设置**
    - `-s, --shared`                                   安装 **{module-name}.Domain.Shared** nuget 包
    - `-o, --domain`                                   安装 **{module-name}.Domain** nuget 包
    - `-e, --entity-framework-core`                    安装 **{module-name}.EntityFrameworkCore** nuget 包
    - `-m, --mongo-db`                                 安装 **{module-name}.MongoDB** nuget 包, **一般不需要设置**
    - `-c, --contracts`                                安装 **{module-name}.Application.Contracts** nuget 包
    - `-a, --application`                              安装 **{module-name}.Application** nuget 包
    - `-h, --http-api`                                 安装 **{module-name}.HttpApi** nuget 包
    - `-l, --client`                                   安装 **{module-name}.HttpApi.Client** nuget 包, **一般不需要设置**
    - `-w, --web`                                      安装 **{module-name}.Web** nuget 包, **一般不需要设置**
    - `--custom <custom>`                              指定哪个模块应增加到应用程序, 如. "`Web:Web,Orders.Web:Web:Orders`" 指的是`Web` 模块添加到`Web`应用程序, `Orders.Web` 模块也添加到`Web`应用程序项目,最后的 `Orders`是子模块名称，用来标识引用的名称空间, **一般不需要设置**
    - `-d, --directory <directory>`                    Abp项目的根路径. 如果没有指定, 则为当前运行目录, **一般不需要设置**
    - `--exclude <exclude>`                            在查找文件时也排除的目录, 可以使用通配符 (`*` 和 `?`). 使用两个*(`**`)表示所有的目录, **一般不需要设置**, 如：`--exclude *Folder1 Folder2/Folder* **/*Folder? **/*Folder*`
1. 示例
    - 添加 Wiz.Easc模块 `Domain.Shared`, `Domain`, `EntityFrameworkCore`, `Application.Contracts`, `Application`, `HttpApi` 包的引用
        ```
        abphelper module add WIZ.Easc -soecah
        ```
## 快速移除对 遵照Abp规范开发的模块 的引用  
1. 模块名称
    - 模块名称为模块nuget包名称的共用前缀, 如: Wiz.Easc.Domain 包的模块名称为: Wiz.Easc
1. 移除模块选项
    - 同安装模块选项
1. 示例
    - 移除 Wiz.Easc模块 `Domain.Shared`, `Domain`, `EntityFrameworkCore`, `Application.Contracts`, `Application`, `HttpApi` 包的引用
        ```
        abphelper module remove WIZ.Easc -soecah
        ```
    