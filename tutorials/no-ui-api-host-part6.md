# API Host应用程序开发教程 - 第六章: 数据库集成
## 介绍
这一章解释了如何为上一章介绍的Author实体配置数据库集成.

## DB Context
在Acme.BookStore.EntityFrameworkCore项目中打开BookStoreDbContext并添加以下DbSet属性：
```
public DbSet<Author> Authors { get; set; }
```
然后定位到同一个项目中`BookStoreDbContext`类中的`OnModelCreating`方法，在方法末尾添加以下几行：
```
builder.Entity<Author>(b =>
{
    b.ToTable(BookStoreConsts.DbTablePrefix + "Authors",
        BookStoreConsts.DbSchema);
    
    b.ConfigureByConvention();
    
    b.Property(x => x.Name)
        .IsRequired()
        .HasMaxLength(AuthorConsts.MaxNameLength);

    b.HasIndex(x => x.Name);
});
```
这就像`Book`之前对实体所做的一样，因此不再次解释.

## 创建新的数据库迁移
模板已配置使用[Entity Framework Core Code First Migrations](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/).由于我们已经更改了数据库映射配置，应该创建一个新的迁移并将更改应用于数据库.

在`Acme.BookStore.EntityFrameworkCore`项目目录中打开命令行终端并键入以下命令：
```
dotnet ef migrations add Added_Authors
```

这将向项目添加一个新的迁移类：

![avatar](images/bookstore-efcore-migration-authors.png)

您可以在同一命令行终端中使用以下命令对数据库应用更改：
```
dotnet ef database update
```
> 如果您使用的是 Visual Studio，则可以在包管理器控制台 (PMC) 中使用`Add-Migration Added_Authors -Context BookStoreMigrationsDbContext`和`Update-Database -Context BookStoreMigrationsDbContext`命令.在这种情况下，请确保是启动项目并且是PMC 中的默认项目.Acme.BookStore.WebAcme.BookStore.EntityFrameworkCore

## 实现 IAuthorRepository
在`Acme.BookStore.EntityFrameworkCore`项目（`Authors`文件夹中）创建一个名为`EfCoreAuthorRepository`的类并粘贴以下代码：
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Acme.BookStore.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Acme.BookStore.Authors
{
    public class EfCoreAuthorRepository
        : EfCoreRepository<BookStoreDbContext, Author, Guid>,
            IAuthorRepository
    {
        public EfCoreAuthorRepository(
            IDbContextProvider<BookStoreDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task<Author> FindByNameAsync(string name)
        {
            var dbSet = await GetDbSetAsync();
            return await dbSet.FirstOrDefaultAsync(author => author.Name == name);
        }

        public async Task<List<Author>> GetListAsync(
            int skipCount,
            int maxResultCount,
            string sorting,
            string filter = null)
        {
            var dbSet = await GetDbSetAsync();
            return await dbSet
                .WhereIf(
                    !filter.IsNullOrWhiteSpace(),
                    author => author.Name.Contains(filter)
                 )
                .OrderBy(sorting)
                .Skip(skipCount)
                .Take(maxResultCount)
                .ToListAsync();
        }
    }
}
```
- 继承自`EfCoreRepository`，因此它继承了标准存储库方法实现.
- `WhereIf`是 ABP 框架的快捷扩展方法. 仅当第一个条件满足时才添加Where条件. 你可以自己做同样的事情，但这些类型的快捷方法让我们的实现更轻松.
- `sorting`可以是像`Name`,`Name ASC`或`Name DESC` 之类的字符串.可以使用[System.Linq.Dynamic.Core](https://www.nuget.org/packages/System.Linq.Dynamic.Core) NuGet 包.
> 有关 EF Core 的存储库的更多信息，请参阅[EF Core 集成文档](https://docs.abp.io/en/abp/latest/Entity-Framework-Core).

## 下一章
查看本教程的[下一章](no-ui-api-host-part7.md).

