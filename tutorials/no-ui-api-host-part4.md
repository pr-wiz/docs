# API Host应用程序开发教程 - 第四章: 授权
## 权限
ABP Framework 提供了一个基于 ASP.NET Core 的[授权基础架构](https://docs.microsoft.com/en-us/aspnet/core/security/authorization/introduction)的[授权系统](https://docs.abp.io/en/abp/latest/Authorization).在标准授权基础架构之上添加的一项主要功能是**权限系统**，它允许定义权限并为每个角色、用户或客户端启用/禁用权限.

### 权限名称
权限必须具有唯一的名称, 权限名称为字符串.最好的方法是将其定义为常量，这样我们就可以重用权限名称.

打开项目`Acme.BookStore.Application.Contracts`里面的类`BookStorePermissions`（在Permissions文件夹中），修改内容如下：
```
namespace Acme.BookStore.Permissions
{
    public static class BookStorePermissions
    {
        public const string GroupName = "BookStore";

        public static class Books
        {
            public const string Default = GroupName + ".Books";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }
    }
}
```
**推荐** 采用分层的方式定义权限名称, 这种方式很有用.例如，“创建书”这个权限的名称定义为`BookStore.Books.Create`.

### 权限定义
您应该在使用它们之前定义权限.

打开工程`Acme.BookStore.Application.Contracts`里面的类`BookStorePermissionDefinitionProvider`（在Permissions文件夹中），修改内容如下：
```
using Acme.BookStore.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Acme.BookStore.Permissions
{
    public class BookStorePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var bookStoreGroup = context.AddGroup(BookStorePermissions.GroupName, L("Permission:BookStore"));

            var booksPermission = bookStoreGroup.AddPermission(BookStorePermissions.Books.Default, L("Permission:Books"));
            booksPermission.AddChild(BookStorePermissions.Books.Create, L("Permission:Books.Create"));
            booksPermission.AddChild(BookStorePermissions.Books.Edit, L("Permission:Books.Edit"));
            booksPermission.AddChild(BookStorePermissions.Books.Delete, L("Permission:Books.Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<BookStoreResource>(name);
        }
    }
}
```
这个类定义了一个**权限组**（在 UI 上对权限进行分组，将在下面看到）和这个组内的**4 个权限**.此外，`Create`、`Edit`和`Delete`是`BookStorePermissions.Books.Default`权限的子级.只有选择了父权限，才能选择子权限.

最后，编辑本地化文件`en.json`（在项目Acme.BookStore.Domain.Shared的Localization/BookStore文件夹下）定义上面使用的本地化键：
```
"Permission:BookStore": "Book Store",
"Permission:Books": "Book Management",
"Permission:Books.Create": "Creating new books",
"Permission:Books.Edit": "Editing the books",
"Permission:Books.Delete": "Deleting the books"
```
> 本地化键名是任意的，没有强制规则.但我们更喜欢上面使用的约定.


### 权限管理界面
定义权限后，您可以在**权限管理模块**中看到它们.

进入*Administration* -> *Identity* -> *Roles*页面，选择`admin` 角色的`Permissions`操作，打开权限管理模块：

![avatar](images/bookstore-permissions-ui.png)

授予您想要的权限并保存.

> 提示：如果您运行`Acme.BookStore.DbMigrator`应用程序，新的权限会自动授予管理员角色.

### 授权
现在，您可以使用权限来授权图书管理.

#### 应用层 & HTTP API
打开`BookAppService`类并将**策略名称**添加为上面定义的**权限名称**：
```
using System;
using Acme.BookStore.Permissions;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Acme.BookStore.Books
{
    public class BookAppService :
        CrudAppService<
            Book, //The Book entity
            BookDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateBookDto>, //Used to create/update a book
        IBookAppService //implement the IBookAppService
    {
        public BookAppService(IRepository<Book, Guid> repository)
            : base(repository)
        {
            GetPolicyName = BookStorePermissions.Books.Default;
            GetListPolicyName = BookStorePermissions.Books.Default;
            CreatePolicyName = BookStorePermissions.Books.Create;
            UpdatePolicyName = BookStorePermissions.Books.Edit;
            DeletePolicyName = BookStorePermissions.Books.Delete;
        }
    }
}
```
向构造函数添加了代码.基类`CrudAppService`**自动**对 **CRUD** 操作使用这些权限.这使应用程序服务安全，也使HTTP API安全，因为此服务自动用作 HTTP API，请参阅[自动 API 控制器](https://docs.abp.io/en/abp/latest/API/Auto-API-Controllers).

> 稍后在开发作者管理功能时，您将看到使用`[Authorize(...)]`属性的声明性授权.


#### JavaScript 端

- abp.auth.isGranted(...) 用于检查之前定义的权限.
    ```
    var bookDelete = abp.auth.isGranted('BookStore.Books.Delete');
    ```
## 下一章
查看本教程的[下一章](no-ui-api-host-part5.md).

