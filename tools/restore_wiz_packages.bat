@echo off
; rem remove cache

dotnet nuget locals http-cache -c
dotnet nuget locals plugins-cache -c
dotnet nuget locals temp -c

for /d %%d in ("%USERPROFILE%\.nuget\packages\wiz.*") do rd /s /q "%%d"

; rem restore package
for  %%f in (*.sln) do (
    ; rem 跳过nuget-package.sln
    if /i not "%%f" =="nuget-package.sln"  dotnet restore %%f
)

pause