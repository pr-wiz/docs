# 接口设计说明
## 普通实体(非聚合根实体)
- 原则上不为非聚合根实体创建端点

## 聚合根(实体)
### 为每一个聚合根创建端点, Abp框架提供标准CRUD方法(具体实体相关参数格式参考swagger)
假设端点为: http://localhost:62112/api/app/user
1. 创建
    - Url: http://localhost:62112/api/app/user
    - Method: POST
    - Args: 聚合根 Dto Json
    - 正常返回: 已创建的聚合根 Json
1. 获取单个
    - Url: http://localhost:62112/api/app/user/{id}
    - Method: GET
    - Args: 无
    - 正常返回: 找到的聚合根 Dto Json
        
1. 获取列表
    - Url: http://localhost:62112/api/app/user
    - Method: GET
    - Args: 直接跟在url后,可选, 如: ?Sorting=name&SkipCount=0&MaxResultCount=10'
        - Sorting: 排序字段
        - SkipCount: 跳过前多少条
        - MaxResultCount: 最多获取多少条
    - 正常返回: 符合要求的聚合根列表 Json
        ```
        {
            "items": [
                {
                    // 聚合根 Dto
                }
            ],
            "totalCount": 2 // 符合条件的记录总数(非当页数据)
        }
        ```
1. 更新单个
    - Url: http://localhost:62112/api/app/user/{id}
    - Method: PUT
    - Args: 聚合根 Json
    - 正常返回: 已更新的聚合根 Dto Json
1. 删除单个
    - Url: http://localhost:62112/api/app/user/{id}
    - Method: DELETE
    - Args: 聚合根 Json
    - 正常返回: 已删除的聚合根 Dto Json

### 两个聚合根之间的关联
- 以 User 和 Role 举例
- 假设端点为: http://localhost:62112/api/app/user
1. 获取某个用户的角色
    - Url: http://localhost:62112/api/app/user/{id}/roles
    - Method: GET
    - Args: 无
    - 正常返回: 符合条件的 Role Dto Json
        ```
        {
            "items": [
                {
                    //Role Dto
                }
            ]
        }
        ```
### 查找
- 以 User 为例
- 所有查找方法都返回列表
- 假设端点为: http://localhost:62112/api/app/user
1. 根据单个属性查找(以Name举例), 返回例
    - Url: http://localhost:62112/api/app/user/by-name/{name}
    - Method: GET
    - Args: 无
    - 正常返回: 符合条件的聚合根 Dto Json
        ```
        {
            "items": [
                {
                    //User Dto
                }
            ],
            "totalCount": 2 // 符合条件的记录总数(非当页数据)            
        }
        ```
1. 动态查找
    - 不建议
    - 根据业务需要对应创建命名业务方法(参数可为空)

## 返回
### 正常返回
#### 单个对象 Dto json
- 以下方法返回单个对象
    - 创建
    - 更新单个
    - 获取单个
- 默认包含所有子实体 Dto

##### 实体 Dto 基础属性
- 示例
    ```
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "creationTime": "2021-09-01T07:51:42.050Z",
      "creatorId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "lastModificationTime": "2021-09-01T07:51:42.050Z",
      "lastModifierId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
      "isDeleted": false,
      "tenantId": null
    }
    ```
- 说明
    - `id` 实体主键, GUID
    - `creationTime` 创建时间, DateTime
    - `creatorId` 创建者Id, GUID
    - `lastModificationTime` 最后修改时间, DateTime
    - `lastModifierId` 最后修改者Id, GUID
    - `isDeleted` 是否已删除, Boolean (仅实现了逻辑删除的实体有此属性)
    - `deletionTime` 删除时间, DateTime
    - `deleterId` 删除者Id, GUID
    - `tenantId` 租户Id, GUID? (仅实现了多租户的实体有此属性)
#### 返回列表
    1. 分页返回
        ```
        {
            "items": [
                {
                    //User Dto
                }
            ],
            "totalCount": 2 // 符合条件的记录总数(非当页数据)            
        }
        ```
        - items 中只有该页的记录
        - totalCount 为总记录数

        一般适用于, 应根据业务数据量来定:
        - 动态查询
        - 返回所有记录

    
    1. 非分页返回
        ```
        {
            "items": [
                {
                    //User Dto
                }
            ]         
        }
        ```
        - items 中为符合条件的所有记录
        
        一般适用于, 应根据业务数据量来定:
        - 聚合根中包含另一个聚合根集合
### 异常返回
- 不允许直接返回原始错误代码
- 异常返回一般只包含一个 `error` 节
- 一般应封闭为业务异常
    ```
    {
        "error": {
            "code": "Wiz.EmergencyDrillSystem:010001",
            "message": "This is an test error",
            "details": null,
            "data": {},
            "validationErrors": null
        }
    }
    ```
    - code 错误代码
    - message 提示消息
    - detials 详细信息
    - data 错误数据, json格式, 如果无则为 {}
    - validationErrors 校验错误信息, json格式, 只有校验错误时才会有, 否则为null

#### 错误代码
1. 错误代码由 **名称空间** 和代码组成
1. 名称空间分为公共和业务模块两类, 最好同Abp后台命名保持一致, 如: `Wiz.EmergencyDrillSystem`
1. 代码由6位数字标识, 可以具有前置0
1. 异常代码应做为字典维护

#### 提示消息
- 提示消息 和 错误代码关联
- 提示消息应实现本地化

#### 详细信息
- 一般不需要, 调试时可能需要

#### 错误数据
- 一般不需要
- 可扩展记录和日志关联uuid

#### validationErrors 
- 参数校验出现异常时会自动包含
- 一般由Abp框架自动实现

### 异常对应 http status
- 对于 `AbpAuthorizationException`:
  - 用户没有登录,返回 `401` (未认证).
  - 用户已登录,但是当前访问未授权,返回 `403` (未授权).
- 对于 `AbpValidationException` 返回 `400` (错误的请求) .
- 对于 `EntityNotFoundException` 返回 `404` (未找到).
- 对于 `AbpDbConcurrencyException` 返回 `409` (并发冲突).
- 对于 `IBusinessException` 和 `IUserFriendlyException` (它是`IBusinessException`的扩展) 返回`403` (未授权) .
- 对于 `NotImplementedException` 返回 `501` (未实现) .
- 对于其他异常 (基础架构中未定义的) 返回 `500` (服务器内部错误) .

### 常见异常
- AbpAuthorizationException
    ```
    {
        "error": {
            "code": "Volo.Authorization:010002",
            "message": "Authorization failed! Given policy has not granted: EmergencyDrillSystem.DrillPlan.Create",
            "details": null,
            "data": {
                "PolicyName": "EmergencyDrillSystem.DrillPlan.Create"
            },
            "validationErrors": null
        }
    }
    ```
- EntityNotFoundException
    ```
    {
        "error": {
            "code": null,
            "message": "There is no entity DrillPlan with id = 39feb283-55bb-c04d-eec5-47885f73fa63!",
            "details": null,
            "data": null,
            "validationErrors": null
        }
    }
    ```
- AbpValidationException
    ```
    {
        "error": {
            "code": null,
            "message": "你的请求无效!",
            "details": "验证时发现以下错误.\r\n - name不能为空\r\n",
            "data": {},
            "validationErrors": [
                {
                    "message": "name不能为空",
                    "members": [
                    "name"
                    ]
                }
            ]
        }
    }
    ```
- IBusinessException
    ```
    {
        "error": {
            "code": "Eaf:000001",
            "message": "类型 Wiz.MultiDb.Samples.Sample 没有实现/继承 Volo.Abp.MultiTenancy.IMultiTenant",
            "details": null,
            "data": {
            "type": "Wiz.MultiDb.Samples.Sample",
            "baseType": "Volo.Abp.MultiTenancy.IMultiTenant"
            },
            "validationErrors": null
        }
    }
    ```