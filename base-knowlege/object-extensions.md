# 对象扩展
ABP 框架提供了一个**对象扩展系统**，允许您在**不修改**相关类的情况下向现有对象**添加额外的属性**。这允许扩展由依赖的应用程序模块实现的功能，特别是当您想要**扩展模块定义的实体和DTO **时。

> 您自己的对象通常不需要对象扩展系统，因为您可以轻松地将常规属性添加到您自己的类中。

## IHasExtraProperties 接口
这是一个使类可扩展的接口. 它定义了 Dictionary 属性:
```
Dictionary<string, object> ExtraProperties { get; }
```

然后您可以使用此字典添加或获取额外的属性。

### 基类
默认以下基类实现了 `IHasExtraProperties` 接口:

- 由 `AggregateRoot` 类实现 (参阅 entities).
- 由 `ExtensibleEntityDto`, `ExtensibleAuditedEntityDto`... DTO基类实现.
- 由 `ExtensibleObject` 实现, 它是一个简单的基类,任何类型的对象都可以继承.

因此，如果您从这些类继承，您的类也将是可扩展的。如果没有，您始终可以手动实现它。

### 基本扩展方法

虽然可以直接使用类的 `ExtraProperties` 属性,但建议使用以下扩展方法使用额外属性.

**SetProperty**

用于设置额外属性的值：
```
user.SetProperty("Title", "My Title");
user.SetProperty("IsSuperUser", true);
```
`SetProperty` 返回相同的对象, 你可以使用链式编程:
```
user.SetProperty("Title", "My Title")
    .SetProperty("IsSuperUser", true);
```

**GetProperty**

用于读取额外属性的值：
```
var title = user.GetProperty<string>("Title");

if (user.GetProperty<bool>("IsSuperUser"))
{
    //...
}
```
- GetProperty 是一个泛型方法,对象类型做为泛型参数.
- 如果未设置给定的属性,则返回默认值 (int 的默认值为 0 , bool 的默认值是 false ... 等).

**非原始属性类型**
如果你的属性类型不是原始类型(int,bool,枚举,字符串等),你需要使用 GetProperty 的非泛型版本,它会返回 object.

**HasProperty**
用于检查对象之前是否设置了属性.

**RemoveProperty**
用于从对象中删除属性. 使用此方法代替为属性设置 null 值.

### 一些最佳实践
为属性名称使用魔术字符串很危险,因为你很容易输入错误的属性名称-这并不安全;

- 在 Domain.Shared 项目中定义一个静态类, 用于为额外属性名称定义常量
- 此静态类的名称空间同要扩展的类的名称空间
- 在 Domain 项目中定义一个扩展方法类, 使用扩展方法轻松设置你的属性
- 扩展类的名称空间同要扩展的类的名称空间
    ```
    public static class OrganizationUnitExtensions
    {
        public static void SetTitle(this OrganizationUnit ou, string title)
        {
            ou.SetProperty(OrganizationUnitExtraPropertyNames.Title, title);
        }

        public static string GetTitle(this OrganizationUnit ou)
        {
            return ou.GetProperty<string>(OrganizationUnitExtraPropertyNames.Title);
        }
    }
    ```
然后您可以轻松设置或获取Title属性：
```
ou.SetTitle("My Title");
var title = ou.GetTitle();
```

## Object Extension Manager
你可以为可扩展对象(实现 IHasExtraProperties接口)设置任意属性, ObjectExtensionManager 用于显式定义可扩展类的其他属性.

显式定义一个额外的属性有一些用例：

- 允许控制如何在对象到对象的映射上处理额外的属性 (参阅下面的部分).
- 允许定义属性的元数据. 例如你可以在使用[EF Core](https://docs.abp.io/zh-Hans/abp/latest/Entity-Framework-Core)时将额外的属性映射到数据库中的表字段.

> ObjectExtensionManager 实现单例模式 (ObjectExtensionManager.Instance) ,你应该在应用程序启动之前定义对象扩展. [应用程序启动模板](../setup-templates/readme.md) 有一些预定义的静态类,可以安全在内部定义对象扩展.

### AddOrUpdate
`AddOrUpdate` 是为对象定义额外属性或更新额外属性的主要方法。

示例：在 Domain 项目为 `OrganizationUnit` 实体定义额外的属性：
```
ObjectExtensionManager.Instance
    .AddOrUpdate<OrganizationUnit>(options =>
        {
            options.AddOrUpdateProperty<string>(OrganizationUnitExtraPropertyNames.Title);
        }
    );
```

### AddOrUpdateProperty
虽然可以如上所示使用 AddOrUpdateProperty, 但如果要定义单个额外的属性,也可以使用快捷的扩展方法:
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnit, string>(OrganizationUnitExtraPropertyNames.Title);
```

有时为多个类型定义一个额外的属性是可行的(应在类型定义的项目中, 此处为Application.Contracts)。不用一一定义，可以使用如下代码：
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<string>(
        new[]
        {
            typeof(OrganizationUnitDto),
            typeof(OrganizationUnitCreateDto),
            typeof(OrganizationUnitUpdateDto)
        },
        OrganizationUnitExtraPropertyNames.Title
    );
```
#### 属性配置
`AddOrUpdateProperty` 还可以为属性定义执行其他配置的操作.
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnit, string>(
        OrganizationUnitExtraPropertyNames.Title,
        options =>
        {
            //Configure options...
        });
```
> `options` 有一个字典, `namedConfiguration` 使得对象扩展定义甚至可以扩展。EF Core 使用它来将额外的属性映射到数据库中的表字段。请参阅扩展实体文档。

以下部分解释了基本的属性配置选项。

1. 默认值
为新属性自动设置默认值，这是属性类型的自然默认值，如 `string` 为  `null` 、`bool` 为 `false`、`int` 为 0.

有两种方法可以覆盖默认值：

    - DefaultValue Option
        DefaultValue 选项可以设置为任何值：
        ```
        ObjectExtensionManager.Instance
            .AddOrUpdateProperty<OrganizationUnit, int>(
                "MyIntProperty",
                options =>
                {
                    options.DefaultValue = 42;
                });
        ```
    - DefaultValueFactory Options
        `DefaultValueFactory` 可以设置为返回默认值的函数：
        ```
        ObjectExtensionManager.Instance
            .AddOrUpdateProperty<OrganizationUnit, DateTime>(
                "MyDateTimeProperty",
                options =>
                {
                    options.DefaultValueFactory = () => DateTime.Now;
                });
        ```

    `options.DefaultValueFactory` 具有比 `options.DefaultValue` 更高的优先级。

    > 提示：仅当默认值可能随时间发生变化时才使用`DefaultValueFactory`选项（如本例中`DateTime.Now`）。如果它是一个常量值，则使用`DefaultValue`选项。

1. CheckPairDefinitionOnMapping
    控制在映射两个可扩展对象时如何检查属性定义。请参阅“对象到对象映射”部分以更好地了解`CheckPairDefinitionOnMapping`选项。

## Validation
你可能要为你定义的额外属性添加一些 验证规则. AddOrUpdateProperty 方法选项允许进行验证的方法有两种:

- 你可以为属性添加** 数据注解 attributes**.
- 你可以给定一个action(代码块)执行 **自定义验证**.

当你在**自动验证**的方法(例如:控制器操作,页面处理程序方法,应用程序服务方法...)中使用对象时,验证会工作. 因此,每当扩展对象被验证时,所有额外的属性都会被验证.

### 数据注解 Attributes
所有标准数据注释属性都对额外属性有效。例：
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnitCreateDto, string>(
        OrganizationUnitExtraPropertyNames.Title,
        options =>
        {
            options.Attributes.Add(new RequiredAttribute());
            options.Attributes.Add(
                new StringLengthAttribute(32) {
                    MinimumLength = 6 
                }
            );
        });
```

使用此配置, `OrganizationUnitCreateDto` 如果没有提供有效值，对象将无效 Title。

**默认验证属性**
当您创建某些类型的属性时，会自动添加一些属性；
- 非可空原语的属性类型（例如int，bool，DateTime...）和enum类型 加入 `RequiredAttribute`。
- 为枚举类型添加 `EnumDataTypeAttribute`，以防止设置无效的枚举值。
- 如果你不希望这些属性, 使用`options.Attributes.Clear();`清除

### 自定义验证
如果需要，您可以添加一个自定义操作来执行以验证额外的属性。例：
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnitCreateDto, string>(
        OrganizationUnitExtraPropertyNames.Title,
        options =>
        {
            options.Validators.Add(context =>
            {
                var title = context.Value as string;

                if (title == null ||
                    title.StartsWith("X"))
                {
                    context.ValidationErrors.Add(
                        new ValidationResult(
                            "Invalid title: " + title,
                            new[] { OrganizationUnitExtraPropertyNames.Title }
                        )
                    );
                }
            });
        });
```
`context.ServiceProvider` 可用于解析高级场景的服务依赖性。

除了为单个属性添加自定义验证逻辑之外，您还可以添加在对象级别执行的自定义验证逻辑。例：
```
ObjectExtensionManager.Instance
.AddOrUpdate<OrganizationUnitCreateDto>(objConfig =>
{
    //Define two properties with their own validation rules
    
    objConfig.AddOrUpdateProperty<string>("Password", propertyConfig =>
    {
        propertyConfig.Attributes.Add(new RequiredAttribute());
    });

    objConfig.AddOrUpdateProperty<string>("PasswordRepeat", propertyConfig =>
    {
        propertyConfig.Attributes.Add(new RequiredAttribute());
    });

    //Write a common validation logic works on multiple properties
    
    objConfig.Validators.Add(context =>
    {
        if (context.ValidatingObject.GetProperty<string>("Password") !=
            context.ValidatingObject.GetProperty<string>("PasswordRepeat"))
        {
            context.ValidationErrors.Add(
                new ValidationResult(
                    "Please repeat the same password!",
                    new[] { "Password", "PasswordRepeat" }
                )
            );
        }
    });
});
```

## 对象到对象映射

假设你已向可扩展的实体对象添加了额外的属性并使用了自动[对象到对象的映射](https://docs.abp.io/zh-Hans/abp/latest/Object-To-Object-Mapping) 将该实体映射到可扩展的DTO类. 在这种情况下你需要格外小心,因为额外属性可能包含**敏感数据**,这些数据对于客户端不可用.

本节提供了一些**好的做法**来控制对象映射上的额外属性。

### MapExtraPropertiesTo
`MapExtraPropertiesTo` 是 ABP 框架提供的一种扩展方法，用于以受控方式将额外的属性从一个对象复制到另一个对象。用法示例：
```
ou.MapExtraPropertiesTo(ouDto);
```

*** 注意 ***
`MapExtraPropertiesTo` 需要在**两侧**(本例中是 `OrganizationUnit` 和 `OrganizationUnitDto`)定义属性. 以将值复制到目标对象. 否则即使源对象(在此示例中为 identityUser )中确实存在该值,它也不会复制. 有一些重载此限制的方法.

- MappingPropertyDefinitionChecks
    `MapExtraPropertiesTo` 获取一个附加参数来控制单个映射操作的定义检查：
    ```
    identityUser.MapExtraPropertiesTo(
        identityUserDto,
        MappingPropertyDefinitionChecks.None
    );
    ```
    > 要小心,因为 `MappingPropertyDefinitionChecks.None` 会复制所有的额外属性而不进行任何检查. `MappingPropertyDefinitionChecks` 枚举还有其他成员.

- 如果要完全禁用属性的定义检查,可以在定义额外的属性(或更新现有定义)时设置`options.CheckPairDefinitionOnMapping = false;`,如下所示:
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnit, string>(
        OrganizationUnitExtraPropertyNames.Title,
        options =>
        {
            options.CheckPairDefinitionOnMapping = false;
        });
```

- 忽略的属性
    您可能希望忽略特定映射操作的某些属性：
    ```
    ou.MapExtraPropertiesTo(
        ouDto,
        ignoredProperties: new[] {"MySensitiveProp"}
    );
    ```
    忽略的属性不会复制到目标对象。

### AutoMapper 集成
如果你使用的是AutoMapper库,ABP框架还提供了一种扩展方法来利用上面定义的 `MapExtraPropertiesTo` 方法.

你可以在映射配置文件中使用 `MapExtraProperties()` 方法.
```
public class MyProfile : Profile
{
    public MyProfile()
    {
        CreateMap<OrganizationUnit, OrganizationUnitDto>()
            .MapExtraProperties();
    }
}
```
它具有与`MapExtraPropertiesTo()`方法相同的参数。

## Entity Framework Core 数据库映射
如果你使用的是EF Core,可以将额外的属性映射到数据库中的表字段. 例:
```
ObjectExtensionManager.Instance
    .AddOrUpdateProperty<OrganizationUnit, string>(
        OrganizationUnitExtraPropertyNames.Title,
        options =>
        {
            options.MapEfCore(b => b.HasMaxLength(32));
        }
    );
```
有关更多信息，请参阅[Entity Framework Core 集成文档](https://docs.abp.io/zh-Hans/abp/latest/Entity-Framework-Core)。

## 问题
1. 为何扩展属性不能保存和读取？
    - 这是Abp框架为了安全保护而做了匹配规则验证
    - 匹配规则说明
        1. 扩展属性是否在过滤属性列表中, 如果是, 则匹配失败
        1. 是否存在 MappingPropertyDefinitionChecks
            - 不存在, 继续后面的规则
            - MappingPropertyDefinitionChecks.None 匹配成功
            - MappingPropertyDefinitionChecks.Source 在源中没有定义扩展属性就 匹配失败
            - MappingPropertyDefinitionChecks.Destination 在目标中没有定义扩展属性就 匹配失败
            - MappingPropertyDefinitionChecks.Both 在源和目标中都定义了扩展属性才 匹配成功
        1. 扩展属性定义检查
            - 源和目标都定义了扩展属性 匹配成功
            - 源定义了扩展属性且其 CheckPairDefinitionOnMapping = false 匹配成功
            - 目标定义了扩展属性且其 CheckPairDefinitionOnMapping = false 匹配成功
            - 其他失败
    - 有两种方式:
        1. 跳过规则验证(简单, **不推荐**)
            - 在保存前以下面的方式匹配实体:
                ```
                input.MapExtraPropertiesTo(ou, MappingPropertyDefinitionChecks.None);
                ```
            - 对象映射配置文件 由实体映射到Dto调用下面的方法
                ```
                CreateMap<OrganizationUnit, OrganizationUnitDto>()
                    .MapExtraProperties(MappingPropertyDefinitionChecks.None);
                ```
        1. 显式定义扩展属性
            - 定义实体扩展属性(Domain.Share)
                
                - 定义静态方法: `OrganizationUnitManagementModuleExtensionConfigurator.Configure()`
                    ```
                    ObjectExtensionManager.Instance.Modules()
                    .ConfigureIdentity(identity =>
                    {
                        identity.ConfigureOrganizationUnit(ou =>
                        {                        
                            ou.AddOrUpdateProperty<string>( //property type: string
                                OrganizationUnitExtraPropertyNames.Title
                                    );
                        });
                    });
                    ```
                - 在 DomainSharedModule 类中添加
                    ```
                    public override void PreConfigureServices(ServiceConfigurationContext context)
                    {
                        OrganizationUnitManagementModuleExtensionConfigurator.Configure();
                    }
                    ```
            - 定义输入或输出Dto属性(Application.Contracts)
                - 定义静态方法 `OrganizationUnitManagementDtoExtensions.Configure()`
                    ```
                    ObjectExtensionManager.Instance
                        .AddOrUpdateProperty<string>(
                            new[]
                            {    
                                typeof(OrganizationUnitDto),                        
                                typeof(OrganizationUnitCreateDto),
                                typeof(OrganizationUnitUpdateDto)
                            },
                            OrganizationUnitExtraPropertyNames.Title
                        );
                    ```
                - 在 ApplicationContractsModule 类中添加
                    ```
                    public override void PreConfigureServices(ServiceConfigurationContext context)
                    {
                        OrganizationUnitManagementDtoExtensions.Configure();
                    }
                     ```
1. 扩展属性如何实现自动校验？
    - 在定义输入参数Dto扩展属性时添加校验特性(Application.Contracts), 如:
        ```
        ObjectExtensionManager.Instance
                    .AddOrUpdateProperty<string>(
                        new[]{typeof(OrganizationUnitCreateDto), typeof(OrganizationUnitUpdateDto)},
                        OrganizationUnitExtraPropertyNames.Title,
                        property =>
                        {
                            property.Attributes.Add(new RequiredAttribute());
                            property.Attributes.Add(new StringLengthAttribute(64){ MinimumLength = 4 });
                        }
                    );
        ```

1. 如何将扩展属性新建对应到表的字段?
    - 定义映射配置(EnityFrameworkCore)

        - 定义静态方法 OrganizationUnitManagementEfCoreEntityExtensionMappings.Configure() 中实现:
            ```
            // 先执行实体扩展属性定义
            OrganizationUnitManagementModuleExtensionConfigurator.Configure();

            ObjectExtensionManager.Instance
                .MapEfCoreProperty<OrganizationUnit, string>(
                    OrganizationUnitExtraPropertyNames.Title,
                    (entityBuilder, propertyBuilder) =>
                    {
                        propertyBuilder.HasMaxLength(64);
                    }
                );
            ```
        - 在 DbContextFactory 的 CreateDbContext 方法中最开始 调用 定义映射配置(用于生成迁移架构)
            `OrganizationUnitManagementEfCoreEntityExtensionMappings.Configure();`
        - 在 EntityFrameworkCoreModule 类中增加
            ```
            public override void PreConfigureServices(ServiceConfigurationContext context)
            {
                OrganizationUnitManagementEfCoreEntityExtensionMappings.Configure();
            }
            ```
    - 生成一个新的迁移
    - 更新迁移
