# 常见问题
## 在执行数据库迁移`Add-Migration xxx -Context xxx`时出现错误: `A relational store has been configured without specifying either the DbConnection or connection string to use.`
- 一般在MySql迁移时出现
- 一般在appSettings.json中存在多个数据库连接时出现
- 执行UseMySql时只传递了一个参数 `UseMySql(ServerVersion.AutoDetect(connectionString));`
- 处理方法: 执行UseMySql时传递两个参数
	`UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));`

## Abphelper普通实体(非聚合根, 无独立仓储)生成
- 执行代码
	`abphelper gen crud --skip-db-migrations --skip-ui --skip-view-model --skip-custom-repository --skip-test --no-overwrite Entity`
- 调整Dto
	- 输出实体Dto移除聚合根Id属性
	- 输入实体Dto移除聚合根Id属性
	- 输出聚合根Dto添加输出实体Dto列表属性
	- 输入聚合根Dto添加输入实体Dto列表属性并初始化		
- 调整实体带参构造函数为 `protected internal `, 防止被外部实例化
- 删除仓储接口, 由聚合根仓储负责
- 删除仓储实现, 由聚合根仓储负责
- EntityFrameworkCoreModule 中删除 仓储服务 注册
	`options.AddRepository<xxxx, xxxxRepository>();`
- 聚合根查询扩展类 XXXEfCoreQuerableExtensions 的 IncludeDetails 添加包含明细集合
	```
	return queryable
			.Include(x => x.XXX);
	```
- 聚合根仓储类添加 `WithDetailsAsync` 方法：
	```
	public override async Task<IQueryable<XXX>> WithDetailsAsync()
	{
		return (await GetQueryableAsync()).IncludeDetails();
	}
	```
- 删除应用服务接口, 不应提供独立服务
- 删除应用服务, 不应提供独立服务
- DbContextModelCreatingExtensions 中在对应聚合根下添加关系
	`b.HasMany(x => x.XXXs).WithOne().HasForeignKey(x => x.YYYId).IsRequired();`

## 如何禁用授权检查
- 使用 IServiceCollection.AddAlwaysAllowAuthorization() 扩展方法将 AlwaysAllowAuthorizationService 注册到 依赖注入 系统中:
	```
	public override void ConfigureServices(ServiceConfigurationContext context)
	{
		context.Services.AddAlwaysAllowAuthorization();
	}
	```
- 启动模板的集成测试已经禁用了授权服务

## 如何升级现有项目的Abp框架版本
- 使用Abp CLI 工具
	```
	abp update --nuget -v 4.4.2
	```
- --nuget 只升级nuget包, 默认升级nuget和npm包
- -v 4.4.2 表示升级到 4.4.2版本, 如果不提供-v选项, 默认升级到最新版本

## 如何快添加/速移除 遵照Abp规范开发的**模块** 的引用  
1. 模块名称
	- 模块名称为模块nuget包名称的共用前缀, 如: Wiz.Easc.Domain 包的模块名称为: Wiz.Easc
1. 模块选项
	- `-v, --version <version>`                        增定要依赖的模块版本
	- `--module-company-name <module-company-name>`    模块公司名称, **一般不需要设置**
	- `-s, --shared`                                   安装 **{module-name}.Domain.Shared** nuget 包
	- `-o, --domain`                                   安装 **{module-name}.Domain** nuget 包
	- `-e, --entity-framework-core`                    安装 **{module-name}.EntityFrameworkCore** nuget 包
	- `-m, --mongo-db`                                 安装 **{module-name}.MongoDB** nuget 包, **一般不需要设置**
	- `-c, --contracts`                                安装 **{module-name}.Application.Contracts** nuget 包
	- `-a, --application`                              安装 **{module-name}.Application** nuget 包
	- `-h, --http-api`                                 安装 **{module-name}.HttpApi** nuget 包
	- `-l, --client`                                   安装 **{module-name}.HttpApi.Client** nuget 包, **一般不需要设置**
	- `-w, --web`                                      安装 **{module-name}.Web** nuget 包, **一般不需要设置**
	- `--custom <custom>`                              指定哪个模块应增加到应用程序, 如. "`Web:Web,Orders.Web:Web:Orders`" 指的是`Web` 模块添加到`Web`应用程序, `Orders.Web` 模块也添加到`Web`应用程序项目,最后的 `Orders`是子模块名称，用来标识引用的名称空间, **一般不需要设置**
	- `-d, --directory <directory>`                    Abp项目的根路径. 如果没有指定, 则为当前运行目录, **一般不需要设置**
	- `--exclude <exclude>`                            在查找文件时也排除的目录, 可以使用通配符 (`*` 和 `?`). 使用两个*(`**`)表示所有的目录, **一般不需要设置**, 如：`--exclude *Folder1 Folder2/Folder* **/*Folder? **/*Folder*`
1. 示例
	- 添加 Wiz.Easc模块 `Domain.Shared`, `Domain`, `EntityFrameworkCore`, `Application.Contracts`, `Application`, `HttpApi` 包的引用
		```
		abphelper module add WIZ.Easc -soecah
	- 移除 Wiz.Easc模块 `Domain.Shared`, `Domain`, `EntityFrameworkCore`, `Application.Contracts`, `Application`, `HttpApi` 包的引用
		```
		abphelper module remove WIZ.Easc -soecah

## 如何删除windows中的nuget公司nuget包缓存?
- 公司的nuget包名统一以wiz.开头
- windows nuget 缓存默认位置为: `%USERPROFILE%\.nuget\packages`
- 清理nuget缓存方法
	在CMD中执行:
	```
	for /d %x in ("%USERPROFILE%\.nuget\packages\wiz.*") do rd /s /q "%x"
	```

## 如何使用日志?
1. 启动时配置日志
	`.UseSerilog();`
1. 使用时注入日志
	- 构造方法注入
		```
		protected MyClass(ILogger<MyClass> logger)
		{
			_logger = logger;
		}
		``` 
	- 公共属性注入
		```
		public ILogger<MyClass> Logger { get; set; }
		protected MyClass()
		{				
			Logger = NullLogger<PortalDbMigrationService>.Instance;
		}
		```
		 
## CRUD有子集时的保存问题
### 问题
1. 新增/修改时如果新增加了多个子集, 会提示主键冲突
	- Id 都为 Guid.Empty 引起
1. 更新的逻辑不符合实际要求
	- 删除原有的子集再保存传入的子集
	- 如果原有子集数量非常大，传入子集的数据也会非常大
	- 创建和修改日期变乱
### 解决方法
1. CRUD服务从 `EafCrudAppService` 基类继承
1. CRUD服务的 TCreateInput 和 TUpdateInput 必须不同
1. AutoMapper 配置聚合根的 `UpdateDto` => Entity 不自动 Map 子集
	```
	CreateMap<UpdateXXXDto, XXX>(MemberList.Source)
		.ForMember(dest => dest.YYYs, opt => opt.Ignore());
	```
1. 子实体的 `UpdateDto` 实现 `IEntityDto<Guid>`
1. CRUD服务 重写方法 `MergeDetails(TUpdateInput updateInput, TEntity entity)`, 实现合并details到entity中
	- 示例
		```
		protected override void MergeDetails(UpdatXXXDto updateInput, XXX entity)
        {
            YYY tempChildEntity = null;
            foreach (var item in updateInput.YYYs)
            {
                bool isNew = false;
                if (item.Id == Guid.Empty)
                {
                    isNew = true;
                    item.Id = GuidGenerator.Create();
                }
                else
                {
                    tempChildEntity = entity.YYYs.FirstOrDefault(s => s.Id == item.Id);
                    if (tempChildEntity == null)
                    {
                        isNew = true;
                    }
                }
                if (isNew)
                {
                    entity.YYYs.Add(ObjectMapper.Map<UpdateYYYDto, YYY>(item));
                }
                else
                {
                    ObjectMapper.Map<UpdateYYYDto, YYY>(item, tempChildEntity);
                }
            }
        }
		```

## 如何提供HardDelete应用服务方法？
- 当实体实现了ISoftDelete接口后, 默认的删除方法只会做逻辑删除, 即将实体的 IsDeleted 属性置为true
- 如无业务需要, 应禁止实现物理删除应用服务方法
- 要实现物理删除应用服务方法
	1. 定义应用服务方法: `DeleteHardAsync(Guid id)`
		```
		public virtual async Task<DrillPlanWithDetailsDto> DeleteHardAsync(Guid id)
		{
			DrillPlan drillPlan = await _repository.GetAsync(id);            
			await _repository.HardDeleteAsync(drillPlan);
			return ObjectMapper.Map<DrillPlan,DrillPlanWithDetailsDto>(drillPlan);
		}
		```
	1. 定义应用服务方法: `DeleteManyHardAsync(IEnumerable<Guid> ids)`
		```
		public virtual async Task DeleteManyHardAsync(IEnumerable<Guid> ids)
		{
			await _repository.HardDeleteAsync(p => ids.Contains(p.Id));
		}
		```
## 启动应用时出现以下异常:`There is no tenant with the tenant id or name: 39feb327-88d8-6139-41d2-3cd7ac051136`
- 这一般是浏览器cookie引起, 清除浏览器cookie再运行即可解决

## 使用Swagger时没法切换用户, 怎么处理？
- 找到 `Login` 端点
- 执行 ​`/api​/account​/logout` 方法

## 访问HttpApi.Host api时, 当Token失效时, 会跳转到 /Error 页面并出现 500 错误
- Abp 启动模板非开发时启用了 app.UseErrorPage(); 导致发生错误时转向了 默认的 /Error 页面
- HttpApi.Host 项目应禁用 app.UseErrorPage();



## 在未登录时会自动跳到登录界面, 如何避免并直接抛出友好异常消息
- 方法1
	- 以下为临时处理方法
	- Controller 不加 `[Authorize]` 特性
	- 对应ApplicationService 加 `[Authorize]` 特性
- 方法2
	- 在注册验证服务时 启用 `Bearer` 认证架构
		```
		context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearer(options =>
		```
		- 此时如果没有登录, 会出现401错误, 如果已登录但没授权，会出现403错误
	- 原理：进行授权时的Challenge(挑战)机制
		1. 当未经身份验证的用户请求要求身份验证的终结点时，授权会发起身份验证挑战。(例如，当匿名用户请求受限资源或单击登录链接时，会引发身份验证挑战。 ) 
		1. 授权会使用指定的身份验证方案发起挑战；如果未指定任何方案，则使用默认方案:
			- **cookie 身份验证方案** 将用户重定向到登录页面
			- **JWT 持有者方案** 返回具有 www-authenticate: bearer 标头的 401 结果
		1. 挑战操作应告知用户要使用哪种身份验证机制来访问所请求的资源。

## 在执行Ef时出现以下错误:
```
ArgumentException: Format of the initialization string does not conform to specification starting at index 82.
```
- 一般是由于连接字符串格式问题，请确认字符串格式
	```
	Server=localhost;Port=3306;Database=MultiDb_t1;Uid=admin;Pwd=P@ssw0rd;,
	```

## 如何注册默认仓储?
- 如果没有为实体实现自定义仓储且没有注册默认仓储, 在注入仓储的时候会抛出异常:
	```
	Autofac.Core.DependencyResolutionException: An exception was thrown while activating Wiz.MultiDb.Samples.SampleController -> Wiz.MultiDb.Samples.SampleAppService.
	---> Autofac.Core.DependencyResolutionException: None of the constructors found with 'Volo.Abp.Autofac.AbpAutofacConstructorFinder' on type 'Wiz.MultiDb.Samples.SampleAppService' can be invoked with the available services and parameters:
	Cannot resolve parameter 'Volo.Abp.Domain.Repositories.IRepository`2[Wiz.MultiDb.Samples.Sample,System.Guid] sampleRepository' of constructor 'Void .ctor(Volo.Abp.TenantManagement.ITenantRepository, Volo.Abp.Domain.Repositories.IRepository`2[Wiz.MultiDb.Samples.Sample,System.Guid], Volo.Abp.Domain.Repositories.IRepository`2[Wiz.MultiDb.Samples.TenantSample,System.Guid])'.
	```
- 在 `EntityFrameworkCore` 的模块类中为每个DbContext调用选项配置`options.AddDefaultRepositories()`:
	```
	context.Services.AddAbpDbContext<BusinessDbContext>(options => options.AddDefaultRepositories(includeAllEntities: true));
	```
## 自动Api 相关问题
1. 如何配置自动生成 **API Controller**
	- 在应用的Host项目的的模块类(**Module)的ConfigureServices方法中加上
		```
		Configure<AbpAspNetCoreMvcOptions>(options =>
		{
			options.ConventionalControllers.Create(typeof(EascApplicationModule).Assembly);
		});
		```
		- 为每个需要添加的应用服务模块创建
		- 如果要在 Swagger 中显示, 应在配置 Swagger 代码前进行
		- 框架为应用服务模块中的每一个应用服务自动生成 **API Controller**

1. 哪些类会自动生成 API Controller?
	- 实现了 IRemoteService 的类, 由于所有的应用服务都实现了 IApplicationService 接口, 因此所有的服务类都会自动生成

1. 某个类符合条件, 但是定义了 `Dependency` 和 `ExposeServices` 如下特性, 结果会怎么样？
	```
	[Dependency(ReplaceServices = true)]
    [ExposeServices(typeof(DemoAppService))]
	```
	- `DemoAppService` 必须实现了 `IRemoteService` 接口
	- 当前类 直接或间接继承了 `DemoAppService` 类
	- 当前类可以不实现 `IRemoteService` 接口
	- 此机制可以用来在一些情况下替换自动API的实现

1. 自动生成的Api路由规则是什么?
	- 默认路由生成规则如下(按顺序组装, 参考 `ConventionalRouteBuilder`):
		1. 以/api开头
		1. 路由根路径为 /app, 可以通过配置选项的 `RootPath` 变更
		1. 控制器路径, 为应用服务类名的规范化
			- 删除 '`AppService`'、'`ApplicationService`' 和 '`Service`' 后缀
			- 转换为 `kebab-case` 格式
			- 如服务类名称是 `ReadingBookAppService`, 规范化后会变成`/reading-book`
			- 可以通过配置选项的 `UrlControllerNameNormalizer` 的委托自定义规范化后的命名
		1. 如果有主键参数且命名为 id, 则添加 /{id}
		1. Action路径, 为应用服务公共方法名称的规范化
			- 删除 `Async` 后缀
			- 删除 HttpMethod 前缀
				- GET '`GetList`', '`GetAll`', '`Get`'
				- PUT  '`Put`', '`Update`'
				- DELETE '`Delete`', '`Remove`'
				- POST '`Create`', '`Add`', '`Insert`', '`Post`'
				- PATCH '`Patch`'
			- 转换为 `kebab-case` 格式
			- 如果生成的Action路径为空，则不会将其添加到路由中
				- `GetAllAsync` 生成路径
				- `GetPhonesAsync` **phones**
			- 可以通过配置选项的 `UrlActionNameNormalizer` 的委托自定义规范化后的命名
		1. 参数路径
			- 如果参数中有且只有另一个以Id(不含参数 id)为后缀的参数, 则会将它做为路由的最后一部分加入
				- 如有一个参数名为 phoneId, 则会添加 /phoneId 到路由的最后部分
	- 示例：
		- `SampleAppService.CreateSampleAsync(CreateOrUpdateSampleDto input)` **POST** **/api/app/sample/sample**	
		- `SampleAppService.UpdateConnectionStringAsync(Guid id, string name, string connectionString)` **PUT** **​/api​/app​/sample​/{id}​/connection-string**
		- `SampleAppService.UpdateConnectionStringAsync(Guid id, string name, string connectionString, string testId)` **PUT** **​/api​/app​/sample​/{id}​/connection-string​/{testId}**
			- 如下设置 `RootPath`, `UrlControllerNameNormalizer`, `UrlActionNameNormalizer` **PUT** **​/api​/Wiz​/sample​/demo​/{id}​/update-connection-string​/{testId}**
				```
				options.ConventionalControllers.Create(
				typeof(MultiDbApplicationModule).Assembly,
				opts =>
				{
					opts.RootPath = "Wiz/sample";
					opts.UrlControllerNameNormalizer = urlControllerNameNormalizerContext =>
						urlControllerNameNormalizerContext.ControllerName.Equals("Sample")
							? "Demo"
							: urlControllerNameNormalizerContext.ControllerName;
					opts.UrlActionNameNormalizer = urlActionNameNormalizerContext =>
						urlActionNameNormalizerContext.HttpMethod == "PUT" ? $"Update{urlActionNameNormalizerContext.ActionNameInUrl}" : urlActionNameNormalizerContext.ActionNameInUrl;
				});
				```
				- RootPath 区分大小写, 可以多层级, 以/隔开
				- urlControllerNameNormalizerContext.ControllerName 为已规范化但还未`kebab-case`格式化的值
					- SampleAppService => Sample
					- urlActionNameNormalizerContext.ActionNameInUrl 为已规范化但还未`kebab-case`格式化的值
						- UpdateConnectionStringAsync => ConnectionString

1. 4.0 路由样式为 `kebab-case` 格式, 可以通过配置选项设置为 `camel` 格式
	- 设置单个应用模块, 只对单个应用模块的服务有效
		```
		options.ConventionalControllers.Create(
				typeof(MultiDbApplicationModule).Assembly,
				opts =>
				{
					opts.UseV3UrlStyle = true; 
				});
		```
		- `SampleAppService.UpdateConnectionStringAsync(Guid id, string name, string connectionString, string testId)` **PUT** **​/api​/app​/sample​/{id}​/connectionString​/{testId}**
	- 全局设置, 对应用中的所有模块有效
		```
		Configure<AbpConventionalControllerOptions>(options =>
		{
			options.UseV3UrlStyle = true;
		});
		```
	
1. 如何禁止某个服务类自动生成 **API Controller**
	- 为要禁止的服务类添加特征 `[RemoteService(IsEnabled = false)]` 或 `[RemoteService(false)]`
		```
		[RemoteService(IsEnabled = false)] 
		public class MyAppService : ApplicationService
		{

		}
		```
		- 可以用于方法
	- 通过配置选项 TypePredicate 委托
		```
		options.ConventionalControllers
			.Create(typeof(BookStoreApplicationModule).Assembly, opts =>
				{
					opts.TypePredicate = type => { return type != typeof(MyAppService); };
				});
		```
		- 返回 false 表示禁止生成此服务类

1. 如何禁止元数据查看
	- 默认自动生成Api允许查看元数据, Swagger 可以使用元数据生成api文档, 可以通过定义 `[RemoteService(IsMetadataEnabled = false)]` 特性禁止 元数据查看
		```
		[RemoteService(IsMetadataEnabled = false)]
		public class MyAppService : ApplicationService
		{

		}
		```
		- 禁止后 Swagger 不能查看该服务
		- 如果知道路由信息客户端可以调用api
		- 可以用于方法

## 跳转登录页面出现如下错误:
```
NullReferenceException: Object reference not set to an instance of an object.
IdentityServer4.Extensions.StringExtensions.AddQueryString(string url, string query)
```
- 这一般是由于验证方案配置错误, IdentityServer4只支持Cookie方案？ 如果配置成bearer方案，会报此中错误, 如：
	```
	context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
	```
	
	修改为

	```
	context.Services.AddAuthentication()
	```

## Options相关
1. 选项的值是什么时候确定的？
	- 通过 `Configure<TOpions>` 配置的选项, 是在选项注入时确定的
	- 实际上 `Configure<TOpions>` 注册了一个委托(Action), 在取值时如果存在对应选项的委托, 则执行委托。
1. 在不同模块调用 `Configure<TOpions>` 设置同一选项结果是什么？
	- 会根据模块的依赖关系，**依赖模块的设置将覆盖被依赖模块的设置**
	- 只会覆盖选项中被依赖模块设置的属性, 依赖模块没有设置则仍采用被依赖模块设置的属性值
	- 可以通过这种机制修改选项的值为应用需要的值
1. 选项值是从配置文件中读取, 配置文件变更后, 选项值怎么没有变更？
	- 通过 `IOptions<TOptions>` 注入, 不会读取应用程序启动后配置的变更信息
	- 可以通过 `IOptionsSnapshot<TOptions>` 注入, 这种方式会读取应用程序启动后配置的变更信息

## 使用nginx反向代理后, 程序需要做什么配置？
- 参考Issue: https://github.com/dotnet/AspNetCore.Docs/issues/2384
- 在使用其他中间件之前使用 `UseForwardedHeaders`
	```
	app.UseForwardedHeaders(new ForwardedHeadersOptions
		{
			ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
		});
	```
- 添加选项配置(根据实际情况确定是否需要)
	```
	Configure<ForwardedHeadersOptions>(options =>
		{
			options.KnownProxies.Clear();
			options.KnownNetworks.Clear();
		});
	```

## 如何配置swagger xml注释?
1. 确保以下三个项目生成xml文档
	- *.Application.Contracts
	- *.HttpApi
	- *.Application
1. 在启动模块(一般为 Web 或 HttpApi.Host) 模块类的 ConfigureServices 方法中添加
	```
	// 包含XmlComments文件
	var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
	foreach (var xmlCommentFile in xmlCommentFiles)
	{
		options.IncludeXmlComments(xmlCommentFile, true);
	}
	```

	修改位置参考如下:
	```
	context.Services.AddAbpSwaggerGenWithOAuth(
	configuration["AuthServer:Authority"],
	new Dictionary<string, string>
	{
		{"FileManagement", "FileManagement API"}
	},
	options =>
	{
		options.SwaggerDoc("v1", new OpenApiInfo { Title = "FileManagement API", Version = "v1" });
		options.DocInclusionPredicate((docName, description) => true);
		options.CustomSchemaIds(type => type.FullName);

		// 包含XmlComments文件
		var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
		foreach (var xmlCommentFile in xmlCommentFiles)
		{
			options.IncludeXmlComments(xmlCommentFile, true);
		}		
	});
	```
1. 如何将nuget包中的xml输出到输出目录?
	- 在Swagger Api文档中，有一些api是由nuget包提供的，如果没有nuget包相关的xml, 在Api文档中没有对应api的注释说明,可以参考下面的步骤将nuget包中的xml文件输出到输出目录:
		1. 在启动模块项目(Web或HttpApi.Host)中引用相应的nuget包
		1. 打开启动模块项目文件(xml方式)
			1.  为每一个相应的nuget包添加 CopyToOutputDirectory 节点, 如框架中基类部分:
				```
				<PackageReference Include="Wiz.Eaf.Application" Version="0.3.0">
					<CopyToOutputDirectory>lib\netstandard2.0\*.xml</CopyToOutputDirectory>
				</PackageReference>
				<PackageReference Include="Wiz.Eaf.Application.Contracts" Version="0.3.0">
					<CopyToOutputDirectory>lib\netstandard2.0\*.xml</CopyToOutputDirectory>
				</PackageReference>
				<PackageReference Include="Wiz.Eaf.HttpApi" Version="0.3.0">
					<CopyToOutputDirectory>lib\net6.0\*.xml</CopyToOutputDirectory>
				</PackageReference>
				```
				- `netstandard2.0` 和 `net6.0` 按nuget包实际的TargetFramework确定
			1. 添加Target AfterTargetsBuild
				```
				<Target Name="AfterTargetsBuild" AfterTargets="Build">		
					<ItemGroup>
						<PackageReferenceFiles Condition="%(PackageReference.CopyToOutputDirectory) != ''" Include="$(NugetPackageRoot)\$([MSBuild]::Escape('%(PackageReference.Identity)').ToLower())\%(PackageReference.Version)\%(PackageReference.CopyToOutputDirectory)" />
					</ItemGroup>
					<Copy SourceFiles="@(PackageReferenceFiles)" DestinationFolder="$(OutDir)" />
				</Target>
				```
			1. 添加Target AfterTargetsPublish
				```
				<Target Name="AfterTargetsPublish" AfterTargets="Publish">
					<ItemGroup>
						<PackageReferenceFiles Condition="%(PackageReference.CopyToOutputDirectory) != ''" Include="$(NugetPackageRoot)\$([MSBuild]::Escape('%(PackageReference.Identity)').ToLower())\%(PackageReference.Version)\%(PackageReference.CopyToOutputDirectory)" />
					</ItemGroup>
					<Copy SourceFiles="@(PackageReferenceFiles)" DestinationFolder="$(PublishDir)" />
				</Target>
				```
		1. 重新编译运行

## EfCore HasDefaultValue(true) 没有得到预期的值
- 当 EfCore 设置 对象bool值属性的默认值为true, 在插入时不管值为何值, 始终插入true.
	```
	b.Property(dc => dc.IsActive)
		.HasDefaultValue(true)
		.HasColumnName(nameof(MenuGroup.IsActive));
	```
- 可能原因: EfCore对bool值判断是否传值 为 false, 当值为 false 时, 认为没有传值, 插入为默认值 true
- 解决方法:
	1. 不设置默认值
		```
		b.Property(dc => dc.IsActive)
			.HasColumnName(nameof(MenuGroup.IsActive));
		```
	1. 对象类型更改为 bool?
	1. (不建议) 增加 .ValueGeneratedNever() 
		```
		b.Property(dc => dc.IsActive)
			.HasDefaultValue(true)
			.ValueGeneratedNever() 
			.HasColumnName(nameof(MenuGroup.IsActive));
		```

## 已经抛出BusinessException异常, 并且也做了本地化，但是客户端接收到的还是内部错误
- 做以下排查:
	1. 检查是否正确映射了代码名称空间. (一般在Damain.Share模块类中)
		```
		Configure<AbpExceptionLocalizationOptions>(options =>
		{
			options.MapCodeNamespace("DataCategoryManagement", typeof(DataCategoryManagementResource));
		});
		```
		- `DataCategoryManagement` 代码名称空间, 资源文件错误代码的名称空间必须与此完全一致
		- 代码名称空间一般同模块名
	1. 检查是否正确定义了资源文件中的 key
		- key 必须是 名称空间:Code的形式
	1. 检查本地化内容中的占位符是否提供一致

## SameSite 问题
1. 现象1
	- 在使用 **http** 进行oidc身份认证时, 有时浏览器会对 跨站点请求伪造(CSRF) 提供保护的一种标准, 导致认证失效(登录成功后还是处于未登录状态，一直停在登录页面, 不提示用户名密码错误)
	- 解决方法[0.3.0]:	
		1. 在身份认证站点 IdentityServer 项目引用项目` Wiz.Eaf.HttpApi-0.3.0`
		1. 在身份认证站点 IdentityServer 项目的模块类的 `ConfigureServices` 方法中配置
			```
			context.Services.AddSameSiteCookiePolicy();
			```
		1. 在身份认证站 IdentityServer 项目的模块类的 `OnApplicationInitialization` 方法中 `app.UseAuthentication();` 之前配置
			```
			app.UseCookiePolicy();
			app.UseAuthentication();
			```
	- 解决方法[0.2.0]:
		1. 在身份认证站点 IdentityServer 项目的后台配置文件 appsettings.json 中添加 "App:SameSiteLaxMode": true
			```
			"App": {
				"SameSiteLaxMode": true
			}
			```
		1. 在身份认证站点 IdentityServer 项目的模块类的 `ConfigureServices` 方法中配置
			```
			// 是否启用SameSitMode 为 Lax
            bool sameSiteLaxMode = false;
            _ = bool.TryParse(configuration["App:SameSiteLaxMode"], out sameSiteLaxMode);
            if (sameSiteLaxMode)
            {
                // 配置Cookie SameSite策略为Lax
                Configure<CookiePolicyOptions>(options =>
                {
                    options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Lax;
                });
            }
			```
		1. 在身份认证站 IdentityServer 项目的模块类的 `OnApplicationInitialization` 方法中 `app.UseAuthentication();` 之前配置
			```
			// 是否启用SameSitMode 为 Lax
            bool sameSiteLaxMode = false;
            _ = bool.TryParse(context.GetConfiguration()["App:SameSiteLaxMode"], out sameSiteLaxMode);
            if (sameSiteLaxMode)
            {
                {
                	app.UseCookiePolicy();
				}
            }
			app.UseAuthentication();
			```
		
1. 现象2
	- 报400错误，后台日志提示 `[WRN] Failed to determine the https port for redirect.`
	- 解决方法[0.3.0]:	
		1. 在启动项目的模块类的 `ConfigureServices` 方法中配置
			```
			context.Services.AddSameSiteCookiePolicy();
			```
		1. 在启动项目的模块类的 `OnApplicationInitialization` 方法中 `app.UseAuthentication();` 之前配置
			```
			app.UseCookiePolicy();
			app.UseAuthentication();
			```
	- 解决方法[0.3.0]：
		1. 在相应启动项目的后台配置文件 appsettings.json 中添加 "App:SameSiteLaxMode": true
			```
			"App": {
				"SameSiteLaxMode": true
			}
			```
		1. 在相应启动项目模块的模块类的 `ConfigureServices` 方法中配置
			```
			// 是否启用SameSitMode 为 Lax
            bool sameSiteLaxMode = false;
            _ = bool.TryParse(configuration["App:SameSiteLaxMode"], out sameSiteLaxMode);
            if (sameSiteLaxMode)
            {
                // 配置Cookie SameSite策略为Lax
                Configure<CookiePolicyOptions>(options =>
                {
                    options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Lax;
                });
            }
			```
		1. 在相应启动项目模块的模块类的 `OnApplicationInitialization` 方法中 `app.UseAuthentication();` 之前配置
			```
			// 是否启用SameSitMode 为 Lax
            bool sameSiteLaxMode = false;
            _ = bool.TryParse(context.GetConfiguration()["App:SameSiteLaxMode"], out sameSiteLaxMode);
            if (sameSiteLaxMode)
            {
                {
                	app.UseCookiePolicy();
				}
            }
			app.UseAuthentication();
			```
1. 参考: https://docs.microsoft.com/zh-cn/aspnet/core/security/samesite?view=aspnetcore-3.1&viewFallbackFrom=aspnetcore-3

## vue登出时不跳转到应用界面的问题
- vue 使用 oidc 库登出出，成功时不会自动跳转到应用登出地址
- 原因：可能是oidc协议的规定引起, 后台没有接收到 PostLogoutRedirectUri 参数, 不知道要跳转的地址
	```
	@if (Model.PostLogoutRedirectUri != null)
	{
		<a abp-button="Primary" id="redirectButton" href="@Model.PostLogoutRedirectUri" cname="@Model.ClientName">@L["ReturnToText"]</a>
	}
	@if (Model.SignOutIframeUrl != null)
	{
		<iframe class="signout logoutiframe" src="@Model.SignOutIframeUrl"></iframe>
	}
	```
- 解决方法: 登出时传入参数 id_token_hint 参数
	```
	logout(){
		this.userManager.signoutRedirect({ 'id_token_hint': this.user.id_token });
	}
	```
	- user 为登录的用户信息
- 参考：https://stackoverflow.com/questions/44684664/identityserver4-postlogoutredirecturi-null
- oidc摘要:
	- 客户端使用注销后重定向 URI 以受控方式注销，通常重定向到为用户提供再次登录链接的应用程序页面
	- 客户端可能有多个 post_logout_redirect_uri 并根据运行时条件决定使用哪个
	- 发送的 post_logout_redirect_uri 仅在伴随 id_token_hint 并且与 OAuth 客户端的配置值匹配时才会被接受。
		- oidc 客户端应该自动发送 id_token_hint, 如果没有, 则会出现 post_logout_redirect_uri 不被接受的情况
	- 如果 post_logout_redirect_uri 没有被发送，那么授权服务器可以使用默认的配置

## API方法只有单个元数据类型参数问题
1. 当API中参数为单个元数据类型的参数时且不为Post时(不含URLPATH参数), 
	- 在传参数时必须以QueryString传递
	- 如果以json体传递会接收不到参数
1. 元数据类型
	- 语言中基础的类型, 如: string, int, bool, ...
1. 示例
	- 更新租户的默认数据库
		- 正确的调用
			```
			PUT /api/multi-tenancy/tenants/{id}/default-connection-string?defaultConnectionString=xxxx
			```
		- 错误的调用
			```
			PUT /api/multi-tenancy/tenants/{id}/default-connection-string
			{
				"defaultConnectionString","xxxx"
			}
			```
## 如何在Host和Tenant数据库分开时数据表分开
1. 仅Host使用的表只在Host数据库中创建
1. 仅Tenant使用的表只在Tenant数据库中创建
1. 其他表在Tenant数据库中都创建
1. 实现方式：
	- 在模块的 ConfigureXXX 方法中进行构建约束
		1. 全部都是Host表, 在最开始添加:
			```
			if (builder.IsTenantOnlyDatabase())
			{
				return;
			}
			```
		1. 全部都是Tenant表, 在最开始添加:
			```
			if (builder.IsHostOnlyDatabase())
			{
				return;
			}
			```
		1. 对于Host表:
			```
			if (builder.IsHostDatabase())
			{
				builder.Entity<Xxx>(b =>
				{
					b.ToTable(ModuleDbProperties.DbTablePrefix + "Xxxs", ModuleDbProperties.DbSchema);

					b.ConfigureByConvention();
					// ...
				});
			}
			```
		1. 对于Tenant表:
			```
			if (builder.IsTenantDatabase())
			{
				builder.Entity<Xxx>(b =>
				{
					b.ToTable(ModuleDbProperties.DbTablePrefix + "Xxxs", ModuleDbProperties.DbSchema);

					b.ConfigureByConvention();
					// ...
				});
			}
			```
	- 在迁移的DbContext中指定数据库Side, 默认为 MultiTenancySides.Both 
		1. 在 OnModelCreating 方法的最前面指定
			```
			modelBuilder.SetMultiTenancySide(MultiTenancySides.Tenant);
			```
		1. 数据库Side
			- MultiTenancySides.Both Host和租户都使用的数据库
			- MultiTenancySides.Host 仅Host使用的数据库
			- MultiTenancySides.Tenant 仅Tenant使用的数据库
		
	
## 访问Abp Web应用时提示 500 错误
1. 后台提示 Could not find the bundle file
	- Web 应用需要手动安装 客户端包, 在web项目目录下执行命令:
		```
		abp install-libs
		```

## 转到登录时出现错误
1. unauthorized_client
	```
	Unknown client or client not enabled
	```
	1. 由于没有设置客户端的 CorsOrigin 引起
		- 处理方法, 维护客户端的 CorsOrigin
	1. 由于认证服务器的CORS设置没有包含客户端的Origin

## 在内部环境我想允许无限制的跨域访问怎么处理
- 在 `Wiz.Eaf.HttpApi-0.3.0` 中提供一个扩展方法, 用于快捷设置
	```
	/// <summary>
	/// 允许任意 Origin, Header, Mehthod
	/// </summary>
	/// <param name="services">服务容器</param>
	/// <returns>服务容器</returns>
	public static IServiceCollection AddAllowAnyCors(this IServiceCollection services)
	```
- 确保在 Host项目中直接或间接引用了 `Wiz.Eaf.HttpApi-0.3.0` 包
- 在 Host项目模块类的 ConfigureServices 方法中
	1. 注释或删除以下代码
		```
		context.Services.AddCors(options =>
		{
			
			options.AddDefaultPolicy( builder =>
			{

				var corsOrigins = configuration["App:CorsOrigins"];
				if (corsOrigins == "*")
				{
					builder
					.AllowAnyOrigin()
					.WithAbpExposedHeaders()
					.SetIsOriginAllowedToAllowWildcardSubdomains()
					.AllowAnyHeader()
					.AllowAnyMethod();
				}
				else
				{
					builder
				.WithOrigins(
					corsOrigins
						.Split(",", StringSplitOptions.RemoveEmptyEntries)
						.Select(o => o.RemovePostFix("/"))
						.ToArray()
				)
				.WithAbpExposedHeaders()
				.SetIsOriginAllowedToAllowWildcardSubdomains()
				.AllowAnyHeader()
				.AllowAnyMethod()
				.AllowCredentials();
				}
			});
		});
		```
	1. 添加以下代码
		```
		context.Services.AddAllowAnyCors();
		```

## 出现 `[ERR] The antiforgery token could not be decrypted.`
1. 这是由于分布式部署时数据保护密钥不一致造成的
1. 解决方法: 
	- 开发环境，所有部署都在同一台windows开发机器上, 由于 setProfileEnvironment 设置为 false 造成
		1. 打开  %windir%/system32/inetsrv/config 文件夹
		1. 打开 applicationHost.config 文件
		1. 查找 <system.applicationHost><applicationPools><applicationPoolDefaults><processModel> 元素
		1. 删除 setProfileEnvironment 属性将属性的值显式设置为 true。
			```
			<applicationPools>				
				<applicationPoolDefaults>
					<processModel identityType="ApplicationPoolIdentity" loadUserProfile="true" setProfileEnvironment="true" />
				</applicationPoolDefaults>
			</applicationPools>
			```
		1. 参考:[ASP.NET Core 中的数据保护密钥管理和生存期信息](https://docs.microsoft.com/zh-cn/aspnet/core/security/data-protection/configuration/default-settings?view=aspnetcore-5.0)
	- 生产环境, 所有部署可能不在同一台服务器上
		1. 将密钥保存到Redis中，每个应用的Host项目的模块类的 `ConfigureServices` 方法中添加
			```
			if (!hostingEnvironment.IsDevelopment())
            {
                var redis = ConnectionMultiplexer.Connect(configuration["Redis:Configuration"]);
                context.Services
                    .AddDataProtection()
                    .PersistKeysToStackExchangeRedis(redis, "XXX-Protection-Keys");
            }	
			```

## 创建数据架构迁移时为同一字段生成了2个两外键
- 原因
	- 在主表中创建关联引起
- 解决方法
	- 在明细表中创建关联引起

## 在应用中如何为不同的模块使用不同的数据库
- 每个模块都有一个自定义的数据库连接字符串名称, 假设
	- A模块 ADbConnection
	- B模块 BDbConnection
- 为 B模块设置独立的数据库, 只需要在appsettings.json配置文件中配置一个名称为 BDbConnection 的数据库连接即可
	```
	"ConnectionStrings": {
		"Default": "server=devmysql01.wiz.top;database=wiz_authserver;uid=aaa;pwd=bbb;port=6033;",
		"BDbConnection": "server=devmysql01.wiz.top;database=wiz_b;uid=aaa;pwd=bbb;port=6033;"
	}
	```
- 为 A B模块设置不同类型的数据库
	- 示例
		- A SqlServer
		- B MySQL
	- 实现方法
		- 在appsettings.json配置文件中配置一个名称为 BDbConnection 
			```
			"ConnectionStrings": {
				"Default": "Server=(LocalDb)\\MSSQLLocalDB;Database=OptimizationManagement_Main;Trusted_Connection=True",
				"BDbConnection": "server=devmysql01.wiz.top;database=wiz_b;uid=aaa;pwd=bbb;port=6033;"
			}
			```
		- 在 EntityFrameworkCore 项目中引用 
			- Volo.Abp.EntityFrameworkCore.SqlServer
			- Volo.Abp.EntityFrameworkCore.MySQL
		- 在 EntityFrameworkCore 模块类 ConfigureServices 方法中配置
			```
			Configure<AbpDbContextOptions>(options =>
			{
				// 默认数据库
				options.Configure(opts =>
				{
					opts.UseSqlServer();
				});

				// B模块的DbContext
				options.Configure<BDbContext>(opts =>
				{
					opts.UseMySQL();
				});
			});
			```
## nginx反向代理需要注意事项有哪些?
1. 身份服务器应用(运行IdentityServer4的应用)
	- nginx.conf中确保存在:
		```
		ignore_invalid_headers on;
		proxy_set_header   Host             $host:$server_port; 
		proxy_buffer_size 128k;
		proxy_buffers 4 256k;
		proxy_busy_buffers_size 256k;
		large_client_header_buffers 4 16k;
		```
		1. ignore_invalid_headers on;
			- 使 nginx 不忽略Header `__Tenant` 
			- 多租户生效
		1. proxy_set_header   Host             $host:$server_port; 
			- 让 IdentityServer4 的 issuer 会带上端口
			- 否则会出现跳转到登录界面时不带端口的错误
		1. 防止登录成功后出现 502 错误
			```
			proxy_buffer_size 128k;
			proxy_buffers 4 256k;
			proxy_busy_buffers_size 256k;
			large_client_header_buffers 4 16k;
			```
			- 由于成功后跳转的url长度超过nginx限制造成
1. API资源应用
	- nginx.conf中确保存在:
		```
		ignore_invalid_headers on;
		```
		1. ignore_invalid_headers on;
			- 使 nginx 不忽略Header `__Tenant` 
			- 多租户生效


	
