# 模块信息清单
## 模块信息规范
### 命名规范
1. 基础名称空间
    - Wiz.Xxx 
    - Wiz.Xxx.Yyy
    - 最多支持3级
    - 来源: 创建时已确定
1. 包名前缀: 
    - Wiz.Xxx 
    - Wiz.Xxx.Yyy
    - 最多支持3级
    - 来源: 创建时已确定
1. 模块名(模块类前缀)
    - Xxx (Wiz.Xxx)
    - Yyy (Wiz.Xxx.Yyy)
    - 来源: 模块类名 移除层名后缀(MmmModule)
1. 表名前缀
    - 统一为四位前缀
    - 统一编制
    - 全大写(如有需要可包含数字，必须以字符开头)
    - 来源: 在Domain层的 `XxxDbProperties.DbTablePrefix` 中指定
1. 模块错误代码名称空间
    - Xxx (Wiz.Xxx)
    - Yyy (Wiz.Xxx.Yyy)
    - 一般同模块名(注意, 没有Wiz.前缀)
    - 来源: Domain.Share 模块类 的 ConfigureServices 方法中配置
        ```
        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("Eaf", typeof(EafResource));
        });
        ```
1. 模块远程服务名
    - Xxx (Wiz.Xxx)
    - Yyy (Wiz.Xxx.Yyy)
    - 来源： Application.Contracts 的常量类 XxxRemoteServiceConsts.RemoteServiceName 中定义
        ```
        public class XxxRemoteServiceConsts
        {
            /// <summary>
            /// 远程服务名称
            /// </summary>
            public const string RemoteServiceName = "Xxx";
            /// <summary>
            /// 模块名称
            /// </summary>
            public const string ModuleName = "Xxx";
        }
        ```
1. 模块控制器区域名
    - Xxx (Wiz.Xxx)
    - Yyy (Wiz.Xxx.Yyy)
    - 来源： Application.Contracts 的常量类 XxxRemoteServiceConsts.ModuleName 中定义
        ```
        public class XxxRemoteServiceConsts
        {
            /// <summary>
            /// 远程服务名称
            /// </summary>
            public const string RemoteServiceName = "Xxx";
            /// <summary>
            /// 模块名称
            /// </summary>
            public const string ModuleName = "Xxx";
        }
        ```
1. 模块数据库连接名
    - Xxx (Wiz.Xxx)
    - Yyy (Wiz.Xxx.Yyy)
    - 来源: 在 Domain 层的 `XxxDbProperties.DbTablePrefix` 中指定
1. 依赖的模块
    - Wiz.Xxx (全名, 依赖接口)
    - 来源: Application层
1. 依赖的抽象组件
    - 组件全名
    - 来源: Application层,Domain层,EntityFrameworkCore层
1. 配置说明
    - 选项配置说明
    - 其它配置说明

## Eaf框架
1. 模块包名: Wiz.Eaf
1. 模块名: Eaf
1. 表名前缀: Eaf
1. 模块错误代码名称空间: Eaf
1. 模块远程服务名: 无
1. 模块数据库连接名: 无
## SiteManagement
1. 模块包名: Wiz.SiteManagement
1. 模块名: SiteManagement
1. 表名前缀: Eaf
1. 模块错误代码名称空间: SiteManagement
1. 模块远程服务名: SiteManagement
1. 模块数据库连接名: SiteManagement

## 数据字典管理
1. 模块包名: Wiz.DataCategoryManagement
1. 模块名: DataCategoryManagement
1. 表名前缀: DCTM
1. 模块错误代码名称空间: DataCategoryManagement
1. 模块远程服务名: DataCategoryManagement
1. 模块数据库连接名: DataCategoryManagement
1. 依赖模块
1. 依赖抽象组件
    - Volo.Abp.EventBus
    - Wiz.Eaf.ExcelIO

## 文件管理
1. 模块包名: Wiz.FileManagement
1. 模块名: FileManagement
1. 表名前缀: FILM
1. 模块错误代码名称空间: FileManagement
1. 模块远程服务名: FileManagement
1. 模块数据库连接名: FileManagement

## 寻优管理模块
1. 基础名称空间: Wiz.OptimizationManagement.
1. 模块包名前缀: Wiz.OptimizationManagement
1. 模块名: OptimizationManagement
1. 表名前缀: OPTM
1. 模块错误代码名称空间: OptimizationManagement
1. 模块远程服务名: OptimizationManagement
1. 模块数据库连接名: OptimizationManagement
1. 依赖模块
    - Wiz.MeasuringPointManagement    
1. 依赖抽象组件
    - Volo.Abp.Caching
    - Wiz.Eaf.IoT
    - Wiz.Eaf.ExcelIO
1. 配置说明(暂时忽略)
