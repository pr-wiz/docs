# 命名规范
## 命名概述 
名称应该说明“什么”而不是“如何”. 通过避免使用公开基础实现（它们会发生改变）的名称, 可以保留简化复杂性的抽象层. 例如, 可以使用 `GetNextStudent()`, 而不是 `GetNextArrayElement()`.   

命名原则是：选择正确名称时的困难可能表明需要进一步分析或定义项的目的. 使名称足够长以便有一定的意义, 并且足够短以避免冗长. 唯一名称在编程上仅用于将各项区分开. 提供人们可以理解的名称是有意义的. 确保选择的名称符合适用语言的规则和标准.  

以下几点是**推荐**的命名方法.  
1. **✗** 避免容易被主观解释的难懂的名称, 如方法名 `AnalyzeThis()`, 或者属性名 `xxK8`. 这样的名称会导致多义性.  
1. **✗** 在类属性的名称中包含类名是多余的, 如 `Book.BookTitle`. 而是应该使用 `Book.Title`.  
1. 只要合适, 在变量名的末尾或开头加计算限定符（`Avg`、`Sum`、`Min`、`Max`、`Index`）.  
1. 在变量名中使用互补对, 如 `min/max`、`begin/end` 和 `open/close`.   
1. 布尔变量名应该包含 `Is`, 这意味着 `Yes/No` 或 `True/False` 值, 如 `fileIsFound`.  
1. 在命名状态变量时, 避免使用诸如 `Flag` 的术语. 状态变量不同于布尔变量的地方是它可以具有两个以上的可能值 .  **✗** 不是使用 `documentFlag` ,  而是使用更具描述性的名称 ,  如 `documentFormatType`.  （此项只供参考） 
1. 即使对于可能仅出现在几个代码行中的生存期很短的变量, 仍然使用有意义的名称. 仅对于短循环索引使用单字母变量名, 如 `i` 或 `j`.  可能的情况下, 尽量不要使用原义数字或原义字符串, 如 
`for (int i = 1; i < 7; i++)`. 而是使用命名常数, 如 `for (int i = 1; i < NUM_DAYS_IN_WEEK; i++)` 以便于维护和理解.  
1. 用于事件处理的委托添加“`EventHandler`”后缀 
1. 用于事件处理之外的那些委托添加“`Callback`”后缀 
1. **✗** 不要给委托添加“`Delegate`”后缀 
1. 用名词或名词词组来给类型命名, 在少数情况下也可以用形容词词组来给类型命名 
1. 用名词、名词词组或形容词来命名属性
1. 用动词或动词词组来命名方法
1. 要用动词或动词短语来命名事件, 总是给事件名称使用发生前, 发生后的概念, **✗** 不要使用`Before`或`After`前缀来表示事件发生前后. 

    例如, Form关闭前引发的事件应该被命名`Closing`. Form关闭后提出的事件应该被命名`Closed`.  
1. 要用名词或名词短语来命名字段 
1. 使用以下前缀：
    - 接口 `I` 
    - 泛型类型参数(单字母参数除外) `T` 
1. 使用以下后缀：
    - 继承`System.Exception`的类型 `Exception`
    - 实现`IEnumerable`的类型 `Collection`
    - 实现`IDictionary`或`IDictionary<K,V>`的类型 `Dictionary`
    - 继承`System.EventArgs`的类型 `EventArgs`
    - 继承`System.Delegate`的类型 `EventHandler`
    - 继承`System.Attribute`的类型 `Attribute`
1. **✗** 不要使用`Flags`或`Enum`来作为类型名称后缀
1. 使用复数名词短语命名`Flags`枚举（枚举支持按位运算值）, 使用单数名词短语命名`non-Flags`枚举. 
 
## 大小写规则 
大写
对于由两个或者更少字母组成的标识符, 标识符中的所有字母都大写, 例如： 
```
System.IO 
System.Web.UI 
```
 
下表汇总了大小写规则, 并提供了不同类型的标识符的示例.  
 
| 标识符 | 	大小写 | 	样例 | 
| ---- | ---- | ---- |
| 名字空间 | 	`Pascal` | 	namespace `System.Security` { … }  |
| 类型 | 	`Pascal` | 	public class `StreamReader` { … }  |
| 接口 | 	`Pascal` | 	public interface `IEnumerable` { … } | 
| 方法 | 	`Pascal` | 	public class Object { public virtual string `ToString()`; 
} | 
| 属性 | 	`Pascal` | 	public class String { public int `Length` { get; } 
} | 
| 事件 | 	`Pascal` | 	public class Process{ public event EventHandler `Exited`; 
} | 
| 字段(私有实例) | 	`Camel` | 	private string `userName`； | 
| 字段(公共静态) | 	`Pascal` | 	public static readonly string `UserId`; | 
| 枚举 | 	`Pascal` | 	enum `FileMode` { `Append`, … } | 
| 参数 | 	`Camel` | 	public class Convert { public static int ToInt32(string `userId`); 
} | 
 
 
## 缩写 
      
为了避免混淆和保证跨语言交互操作, 请遵循有关区缩写的使用的下列规则：        
1. **✗** 不要将缩写或缩略形式用作标识符名称的组成部分. 例如, 使用 `GetWindow`, 而不要使用 
`GetWin`.   
1. **✗** 不要使用计算机领域中未被普遍接受的缩写.   
1. 在适当的时候, 使用众所周知的缩写替换冗长的词组名称. 例如, 用 `UI` 作为 `User Interface` 缩写, 用 `OLAP` 作为 `On-line Analytical Processing` 的缩写.   
1. 在使用缩写时, 对于超过两个字符长度的缩写请使用 `Pascal` 大小写或 `Camel` 大小写. 例如, 使用 `HtmlButton` 或 `HTMLButton`. 但是, 应当大写仅有两个字符的缩写, 如, `System.IO`, 而不是 `System.Io`.  
1. **✗** 不要在标识符或参数名称中使用缩写. 如果必须使用缩写, 对于由多于两个字符所组成的缩写请使用 `Camel` 大小写, 虽然这和单词的标准缩写相冲突.  
 
## 命名空间 
1. 给命名空间命名时的一般性规则是使用公司名称, 后跟技术名称和可选的功能与设计, 如下所示.  
    ```
    Company.Technology[.Feature][.Design]
    ```       
    例如：          
    - namespace TopOne.Student             //学员系统
    - namespace TopOne.Student.Register    //学员注册模块
1. 命名空间使用 `Pascal` 大小写, 用点号分隔.  
1. `Technology` 指的是该项目的英文缩写, 或软件名.  
1. **✗** 命名空间和类不能使用同样的名字. 
    例如, 有一个类被命名为 `Debug` 后, 就不要再使用 `Debug` 作为一个名称空间名称.  
1. 公司名称空间命名(讨论):
    - Company(公司)
        - Wiz
    - Technology(产品?)
        - Core (框架, 其Feature不同与业务)
        - Plant
        - Data
        - ...
    - Feature(技术层？)
        - Domain.Shared
        - Domain
        - Application.Contracts
        - Application
        - EntityFrameworkCore
        - EntityFrameworkCore.<DB>
        - MongoDB
        - HttpApi
        - HttpApi.Host
        - IdentityServer
        - HttpApi.Client
    - Design(产品模块及其他?)
        - Equipments(设备)
    
    - 示例:
        - Wiz.Core
        - Wiz.Core.Data
        - Wiz.Core.Domain.Shared
        - wiz.Plant.Domain.Equipments (or wiz.Plant.Equipments.Domain)

## 解决方案命名
    - 解决方案命名同命名空间 `Company.Technology`
    - 解决方案中项目命名同命名空间 `Company.Technology.Feature`
    - 目录命名
        - 解决方案目录下包含三个子目录
            - src: 存放基础项目源码
            - test: 存在测试项目源码
            - host: 存放host源码(托管应用)
        - 项目目录命名同项目名称

## 文件命名 
1. 文件名遵从 `Pascal` 命名法, 无特殊情况, 扩展名小写. 
    - 如: 应命名为 `MyClass`.`cs`, 而不要命名为 `myClass`.cs, MyClass.`CS`, MyClass.`Cs`
1. `partial`类文件定义二级扩展名称, 采用`camel`命名法, 用来表示部分类的分类, 可以按功能范围或产生方式命名
    - 如: `generate`, `static`, `convert`. 
1. 使用统一而又通用的文件扩展名, 如下表： 
    | 文件类型 | 		扩展名 | 
    1 ---- | ---- |
    | C#类 | 	`.cs` | 	
    | Asp.net 页面 | 	`.aspx` | 	
    | Js文件 |	`.js`	
    | Json配置文件 |	`.json` |	
    | Xml配置文件 |	`.config` |	
    | …  |	使用默认扩展名 | 	
 
## 类 
1. 使用 `Pascal` 大小写.  
1. 用名词或名词短语命名类.  
1. **使用全称**避免缩写, 除非缩写已是一种`公认的约定`, 如 `URL`、`HTML`     
1. **✗** 不要使用类型前缀, 如在类名称上对类使用 `C` 前缀. 例如, 使用类名称 `FileStream`, 而**不是**  
`CFileStream`.   
1. **✗** **不要使用下划线字符** (_).   
1. 其他
    - 有时候需要提供以字母 `I` 开始的类名称, 虽然该类不是接口. 只要 `I` 是作为类名称组成部分的整个单词的第一个字母, 这便是适当的. 例如, 类名称 `IdentityStore` 是适当的. 
    - **在适当的地方**, **使用复合单词命名派生的类**. 派生类名称的**第二个部分应当是基类的名称**. 
        - 例如, `ApplicationException` 对于从名为 `Exception` 的类派生的类是适当的名称, 原因 `ApplicationException` 是一种 `Exception`. 
        - 在应用该规则时进行合理的判断. 例如, `Button` 对于从 `Control` 派生的类是适当的名称. 尽管按钮是一种控件, 但是将 Control 作为类名称的一部分将使名称**不必要地加长**.  
        ```
        public class FileStream             
        public class Button 
        public class String  
        ```
     
## 接口 
以下规则概述接口的命名指南：  
1. 用名词或名词短语, 或者描述行为的形容词命名接口. 
    - 例如, 接口名称 `IComponent` 使用描述性名词. 
    - 接口名称 `ICustomAttributeProvider` 使用名词短语. 
    - 名称 `IPersistable` 使用形容词.   
1. 使用 `Pascal` 大小写.   
1. **✗** 少用缩写.   
1. 给接口名称加上字母 `I` 前缀, 以指示该类型为接口. 
 
1. **✗** 不要使用下划线字符 (_).   
1. 当类是接口的标准执行时, 定义这一对类/接口组合就要使用相似的名称. 两个名称的不同之处只是接口名前有一个 `I` 前缀.  

1. 示例. 
    - 正确命名示例
        ```
        public interface IServiceProvider            
        public interface IFormatable 
        ```
    - 阐释如何定义 IComponent 接口及其标准实现 Component 类.  
        ```
        public interface IComponent  
        { 
            // Implementation code goes here. 
        }                                
        public class Component: IComponent  
        { 
            // Implementation code goes here. 
        } 
        ```
 
## 枚举 (Enum) 

枚举 (Enum) 值类型从 Enum 类继承. 以下规则概述枚举的命名指南：   

1. 对于 Enum 类型和值名称使用 `Pascal` 大小写.   
1. **✗** 少用缩写.   
1. **✗** 不要在 `Enum` 类型名称上使用 `Enum` 后缀.   
1. 对大多数 `Enum` 类型使用**单数**名称, 但是对作为**位域**的 `Enum` 类型使用**复数**名称.   
1. 总是将 `FlagsAttribute` 添加到**位域** Enum 类型.   
1. **✗** 不要命名 `Reserved` 枚举值.  
 
## 参数 
       
以下规则概述参数的命名指南：  
1. 使用描述性参数名称. 参数名称应当具有足够的描述性, 以便参数的名称及其类型可用于在大多数情况下确定它的含义.   
1. 对参数名称使用 `Camel` 大小写.   
1. 使用描述参数的**含义**的名称, 而**不要使用描述参数的类型**的名称.    
1. **✗** 不要使用保留的参数.    
1. **✗** 不要给参数名称加匈牙利语类型表示法的前缀.   
1. 示例:
```
Type GetType(string typeName) 
string Format(string format, params object[] args) 
```

## 方法 
       
以下规则概述方法的命名指南：             
1. 使用动词或动词短语命名方法.   
1. 使用 `Pascal` 大小写.   
1. 以下是正确命名的方法的实例.  
    ``` 
    RemoveAll() 
    GetCharArray() 
    Invoke() 
    ```
 
 
## 属性 (property) 
        
以下规则概述属性的命名指南：              
1. 使用名词或名词短语命名属性.   
1. 使用 `Pascal` 大小写.   
1. 不要使用匈牙利语表示法.   
1. 考虑用与属性的基础类型相同的名称创建属性. 
    - 例如, 如果声明名为 `Color` 的属性, 则属性的类型同样应该是 `Color`. 请参阅本主题中后面的示例.   
           
1. 示例:

    - 正确示例
        ```  
        
        public class SampleClass 
        {                       
            public Color BackColor                        
            { 
                // Code for Get and Set accessors goes here. 
            } 
        } 
        ```
    - 以下代码示例阐释提供其名称与类型相同的属性.  
        ``` 
        public enum Color  
        { 
            // Insert code for Enum here. 
        } 


        public class Control 
        { 
            public Color Color                         
            {  
                get 
                { 
                    // Insert code here. 
                }  
                set 
                { 
                    // Insert code here. 
                }  
            } 
        }
        ```
    - **✗** 以下代码示例不正确, 原因是 `Color` 属性是 `Int` 类型的.  
        ```
        public enum Color 
        { 
            // Insert code for Enum here.  
        } 

        public class Control  
        {                     
            public int Color  
            {  
                // Insert code here  
            } 
        } 
        ``` 
        在不正确的示例中, 不可能引用 `Color` 枚举的成员. `Color.Xxx` 将被解释为访问一个成员, 该成员首先获取 `Color` **属性**(为 int 类型)的值, 然后再访问该值的某个成员（该成员必须是 System.Int32 的实例成员）.  
 
 
## 事件 
        
以下规则概述事件的命名指南：  
1. 对事件处理程序名称使用 `EventHandler` 后缀.   
1. 指定两个名为 `sender` 和 `e` 的参数. 
    - `sender` 参数表示引发事件的对象. `sender` 参始终是 `object` 类型的, 即使在可以使用更为特定的类型时也如此. 
    - 与事件相关联的状态封装在名为 `e` 的事件类的实例中. `e` 参数类型使用适当而特定的事件类.   
1. 用 `EventArgs` 后缀命名事件参数类.   
1. 考虑用**动词**命名事件.   
1. 使用**动名词**（动词的“ing”形式）创建表示**事件前**的概念的事件名称, 用**过去式**表示**事件后**. 
    - 例如, 可以取消的 `Close` 事件应当具有 `Closing` 事件和 `Closed` 事件. 
    - 不要使用 `BeforeXxx`/`AfterXxx` 命名模式.   
1. **✗** **不要**在类型的事件声明上使用**前缀或者后缀**. 例如, 使用 `Close`, 而不要使用 `OnClose`.   
1. 通常情况下, 对于**可以在派生类中重写的事件**, 应在类型上提供一个**受保护的方法**（称为  **OnXxx**）. 此方法只应具有事件参数 `e`, 因为发送方总是类型的实例.  
1. 示例： 
    - 以下示例阐释具有适当名称和参数的事件处理程序
        ```
        public delegate void MouseEventHandler(object sender, MouseEventArgs e); 
        ```
    - 以下示例阐释正确命名的事件参数类. 
        ```  
        public class MouseEventArgs : EventArgs                  
        {                    
            int x;                
            int y; 

            public MouseEventArgs(int x, int y)                    
            { 
                this.x = x; 
                this.y = y;  
            }  
            public int X 
            { 
                get                       
                {                           
                    return x; 
                } 
            }  

            public int Y 
            {                  
                get                        
                {                            
                    return y; 
                } 
            }  
        } 
        ```
 
## 常量 (const) 

以下规则概述常量的命名指南：          
1. 所有单词大写, 多个单词之间用 "_" 隔开. 
    - 如: `public const string PAGE_TITLE = "Welcome"; `
            
 
## 字段 

以下规则概述字段的命名指南：  
1. `private`、`protected` 使用 `Camel` 大小写.  
1. `public` 使用 `Pascal` 大小写.  
1. 不要使用缩写. 
    - 仅在一般都能理解时使用缩写. 
1. **✗** 字段名称不要使用全大写字母. 
1. **✗** 不要对字段名使用匈牙利语表示法. 名称应描述字段含义而非字段类型.   
1. **✗** 不要对字段名或静态字段名使用前缀. 
    - 不要使用前缀来区分静态和非静态字段. 例如, 应用 `g_` 或 `s_` 前缀是不正确的.   
1. 对预定义对象实例使用`public static readonly`字段. 如果存在对象的预定义实例, 则将它们声明为对象本身的公共静态只读字段. 使用 `Pascal` 大小写, 原因是字段是公共的. 
1. 示例：
    - 下面是正确命名的字段的示例.  
        ```
        class SampleClass  
        {                     
            string url;             
            string destinationUrl; 
        }  
        ```
    - 下面的代码示例阐释公共静态只读字段的正确使用.   
        ```
        public struct Color 
        {                       
            public static readonly Color Red = new Color(0x0000FF); 
            
            public Color(int rgb) 
            { 
                // Insert code here.
            } 
            public Color(byte r, byte g, byte b) 
            { 
                // Insert code here. 
            } 

            public byte RedValue  
            {                             
                get                            
                {                                 
                    return Color;                            
                } 
            } 
        } 
        ```
## 静态字段 

以下规则概述静态字段的命名指南：  
1. 使用名词、名词短语或者名词的缩写命名静态字段.   
1. 使用 `Pascal` 大小写.   
1. **✗** 不要对静态字段名称使用匈牙利语表示法前缀.   
1. **建议** 尽可能使用静态属性而不是公共静态字段.   
 
## 集合 
集合是一组组合在一起的类似的类型化对象, 如哈希表、查询、堆栈、字典和列表, 
**建议** 使用复数.  
 
## 标识符 
**✗** 避免使用与常用的 .NET 框架命名空间重复的类名称. 
 
- **不要**将以下任何名称用作类名称： 
    - `System`、`Collections`、`Forms` 或 `UI`. 有关 .NET 框架命名空间的列表, 请参阅类库. 
- 避免使用和以下关键字冲突的标识符:
    ```
    AddHandler AddressOf 	Alias 	And 	Ansi 
    As 	Assembly 	Auto 	Base 	Boolean 
    ByRef 	Byte 	ByVal 	Call 	Case 
    Catch 	CBool 	CByte 	Cchar 	CDate 
    CDec 	CDbl 	Char 	Cint 	Class 
    CLng 	CObj 	Const 	Cshort 	CSng 
    CStr 	CType 	Date 	Decimal 	Declare 
    Default 	Delegate 	Dim 	Do 	Double 
    Each 	Else 	ElseIf 	End 	Enum 
    Erase 	Error 	Event 	Exit 	ExternalSource 
    False 	Finalize 	Finally  	Float 	For 
    Friend 	Function 	Get 	GetType 	Goto 
    Handles 	If 	Implements 	Imports 	In 
    Inherits 	Integer 	Interface 	Is 	Let 
    Lib 	Like 	Long 	Loop 	Me 
    Mod 	Module 	MustInherit 	MustOverride MyBase 
    MyClass 	Namespace 	New 	Next 	Not 
    Nothing 	NotInheritable NotOverridable 	Object 	On 
    Option 	Optional 	Or 	Overloads 	Overridable 
    Overrides 	ParamArray 	Preserve 	Private 	Property 
    Protected 	Public 	RaiseEvent 	ReadOnly 	ReDim 
    Region 	REM 	RemoveHandler 	Resume 	Return 
    Select 	Set 	Shadows 	Shared 	Short 
    Single 	Static 	Step 	Stop 	String 
    Structure 	Sub 	SyncLock 	Then 	Throw 
    To 	True 	Try 	TypeOf 	Unicode 
    Until 	volatile 	When 	While 	With 
    WithEvents WriteOnly 	Xor 	Eval 	extends 
    instanceof package 	var 	    	    
    ``` 

## Abp命名规范
### 通用
1. Create/Insert 对应中文为 新增
1. Update 对应中文为 编辑
1. Delete 对应中文为 删除
1. Get 对应中文为 获取
1. Find/Search 对应中文为 查找
1. Download  对应中文为 下载
1. Upload  对应中文为 上传
1. Export  对应中文为 导出
1. Import  对应中文为 导入

1. 异步方法中的 async 标识 放在最后
    - 虚异步方法
        - virtual 放在前面，async 放在后面, 如:
            `public virtual async Task DeleteAsync(Guid Id)`
    - 重载异步方法
        - virtual 放在前面，async 放在后面, 如:
            `public override async Task DeleteAsync(Guid Id)`

#### xml注释
1. 应为所有的public,internal,protected的类型和成员提供xml注释
1. 对于逻辑比较复杂的成员, 可以添加 remarks 节点来详细说明
1. (可选) 提供使用示例 examples 节点
1. (可选) 提供可能出现的异常 exception 节点
1. ? 属性
    - 可读写属性: 设置或获取 xxxx
    - 只写属性: 设置 xxxx
    - 只读属性: 读取 xxxx
1. 统一基础名词
    1. 新增 对应 Create/Insert
    1. 修改/编辑? 对应 Update
    1. 删除 对应 Delete
    1. 获取 对应 Get
    1. 获取列表 对应 Get...List
    1. 查询 对应 Find
    1. 查询列表 对应 FindList
    1. 添加 对应 Add
    1. 移除 对应 Remove
1. 防止歧义
    1. 新增信息输入Dto => 新增信息的输入Dto / 新增 信息输入Dto

### 领域层
#### 常量
1. 常量在 Domain.Share 项目中定义
1. 常量类应定义为静态类型 static, 并以 Consts结尾
1. 默认应定义以下常量(没有使用时可省略),假设模块名为 Wiz.FileManagement
    - FileManagementConsts 模块全局用常量
    - FileManagementErrorCodes 模块错误代码常量
    - EntityConsts 为每一个实体类型相关的常量, 如 FileInfoConsts
        - 一般为实体的校验属性常量, 如 MaxNameLength 表示 名称 最大长度
#### 实体类
1. 为每个聚合根建立一个目录, 目录名称为实体名称的复数形式
1. 实体名称同类命名规范
1. 实体具有名为`Id`的唯一属性
1. 根据实现Abp的接口, 实体可具有以下属性:
    - `IHasCreationTime` 定义了以下属性:
        - `CreationTime`
    - `IMayHaveCreator` 定义了以下属性:
        - `CreatorId`
    - `ICreationAuditedObject` 继承 `IHasCreationTime` 和 `IMayHaveCreator`, 所以它定义了以下属性:
        - `CreationTime`
        - `CreatorId`
    - `IHasModificationTime` 定义了以下属性:
        - `LastModificationTime`
    - `IModificationAuditedObject` 扩展 `IHasModificationTime` 并添加了 `LastModifierId` 属性. 所以它定义了以下属性:
        - `LastModificationTime`
        - `LastModifierId`
    - `IAuditedObject` 扩展 `ICreationAuditedObject` 和 `IModificationAuditedObject`, 所以它定义了以下属性:
        - `CreationTime`
        - `CreatorId`
        - `LastModificationTime`
        - `LastModifierId`
    - `ISoftDelete` 定义了以下属性:
        - `IsDeleted`
    - `IHasDeletionTime` 扩展 `ISoftDelete` 并添加了 `DeletionTime` 属性. 所以它定义了以下属性:
        - `IsDeleted`
        - `DeletionTime`
    - `IDeletionAuditedObject` 扩展 `IHasDeletionTime` 并添加了 `DeleterId` 属性. 所以它定义了以下属性:
        - `IsDeleted`
        - `DeletionTime`
        - `DeleterId`
    - `IFullAuditedObject` 继承 `IAuditedObject` 和 `IDeletionAuditedObject`, 所以它定义了以下属性:
        - `CreationTime`
        - `CreatorId`
        - `LastModificationTime`
        - `LastModifierId`
        - `IsDeleted`
        - `DeletionTime`
        - `DeleterId`
    - `IHasConcurrencyStamp` 定义了以下属性:
        - `ConcurrencyStamp` Abp实现了乐观并发
    - `IHasExtraProperties` 定义了以下属性:
        - `ExtraProperties` 类型为 `Dictionary<string, object>`, 对于实现`IHasExtraProperties`接口的实体, 一般应为实体定义扩展方式 `GetProperty` `SetProperty` 方法
    - `IMultiTenant` 定义了以下属性:
        - `TenantId` 
#### 值对象
1. 没有 `Id` 属性

#### 领域服务类
1. 类名应以 `Manager` 或 `Service` 为后缀
1. 领域方法应定义为异步方法, 方法名 以 `Async` 为后缀
1. 领域方法一般被 应用层服务调用
1. 如无必要一般不定义领域服务类
1. 领域服务类和应用服务类的区别:
    - 应用服务实现应用的**用例**（典型 Web 应用程序中的用户交互），而域服务实现**核心的**、**用例独立的域逻辑**
    - 应用服务获取/返回**数据传输对象**，域服务方法通常获取和返回域对象（**实体**、**值对象**）
    - 领域服务通常由应用程序服务或其他领域服务使用，而应用程序服务由表示层或客户端应用程序使用
1. **ErrorCode**名称空间应和模块名称保持一致，具体规则如下：
    - 移除第1节, 保留剩余的部分
    - 如: 模块 `Wiz.FileManagement` 的 **ErrorCode**名称空间为 `FileManagement`

#### 存储库接口
1. 创建 Repositories 目录, 所有的仓储的接口都放在此目录下
1. 接口名应为: I + EntityName + Repository
    - 如: `IPersonRepository`
    - 存储库方法应定义为异步, 并以 Async 为后缀
    - 存储库所有的方法应添加一个可选的 `CancellationToken  cancellationToken` 参数
    - 存储库每一个返回单个实体的方法应添加一个可选的 `bool includeDetails = true` 参数
    - 存储库每一个返回实体列表的方法应添加一个可选的 `bool includeDetails = false` 参数
    - CRUD方法
        - InsertAsync
        - GetAsync
        - GetListAsync
        - GetPagedListAsync
        - GetCountAsync
        - FindAsync
        - UpdateAsync
        - DeleteAsync
    - 批量方法
        - InsertManyAsync
        - UpdateManyAsync
        - DeleteManyAsync
    - 如果实体实现了 ISoftDelete, 则应提供物理删除方法
        - HardDeleteAsync

#### 规范类
1. 类名应以 `Specification` 为后缀
#### 选项类
1. 类名应以 `Options` 为后缀
1. 类应该 定义一个常量属性 SectionPosition, 用来定义选项对应配置节的名称
    ```
    /// <summary>
    /// 配置节点名称
    /// </summary>
    public const string SectionPosition = "DefaultIoT";
    ```
1. 如果模块中只有一个选项类, 命名为
    - XxxOptions
        - Xxx 为模块名称
### 应用层
#### 数据传输对象(DTO)
1. 数据传输对象名以 Dto 为后缀
1. 应为实体创建一个匹配的DTO
1. 应定义一个与实体属性完整匹配的输出DTO
    - 敏感属性除外, 如: 密码
1. 属性应为可读写的 **public getter and setter**
1. 为输入DTO定义 **data annotations** 以实现自动校验
1. 所有的DTO定义为 [Serializable] 
1. 如对应实体有关联实体, 应为实体定义两个输出DTO
    - 基础Dto, 命名为 EntityName + Dto, 如 PersonDto
    - 详细Dto, 命名为 EntityName + WithDetailsDto, 如 PersonWithDetailsDto
1. 所有的输入 Dto 都以 InputDto 结尾
    - 即可作输入也可以作为输出的Dto 以 Dto 结尾

#### 应用服务接口
1. 接口名以 `AppService` 为后缀
1. **不要**使用 实体 做为输入输出参数, 应使用DTO
1. 方法应定义为 异步, 以 `Async` 为后缀
1. 方法名称中不应包含实体名
    - 如: 定义为 `GetAsync` 而**不要**定义为 `GetPersonAsync`
1. 取数据方法前缀命名
    - 如果以 Id 查询实体或实体相关的实体时, 以 Get 开头
    - 以其他参数查询时, 以 Find 开头
1. 方法名应尽量能表达业务含义
    - 获取用户的角色列表
        ```
        Task<List<RoleDto>> User.GetRolesAsync(Guid id)
        ```
    - 查找具有某个角色的用户列表
        ```
        Task<List<UserDto>> User.FindListByRoleNameAsync(string roleName)
        ```
1. 参数封装原则
    1. 参数为 单个Id 的不参与封装(Id, ParentId, RoleId)
    1. 除 Id 外, 只有一个参数的不封装
    1. 除 Id 外, 全为 bool 类型的参数不封装
    1. 其他, 除 Id 外, 所有其他参数都封装为 xxxxInputDto, 参数名叫 input
        - xxxx 为移除 Async 后的方法名, 标准Crud的参数Dto除外
        - 方法名如有同名后面加序号, 从2开始, 第一个不加序号(设计时尽量避免同名情况)
            ```
            GetUsersInputDto
            GetUsers2InputDto
            ```
    1. 示列
        - GetAsync(Guid id)
        - GetRolesByRoleNameAsync(Guid id, string roleName)
        - GetChildrenAsync(Guid id, bool recursive = false, bool includeDetails = true)
        - UpdateAsync(Guid id, UpdateUserInputDto input)
1. CRUD 应用服务方法:
    - CreateAsync 
    - GetAsync 
    - GetListAsync 
    - UpdateAsync
    - DeleteAsync

#### 应用服务类
1. 类名以 `AppService` 为后缀
1. **不要**使用 实体 做为输入输出参数, 应使用DTO
1. 方法应定义为 异步, 以 `Async` 为后缀
1. 方法名称中不应包含实体名
    - 如: 定义为 `GetAsync` 而**不要**定义为 `GetPersonAsync`
1. CRUD 应用服务方法:
    - CreateAsync 
    - GetAsync 
    - GetListAsync 
    - UpdateAsync
    - DeleteAsync

### 数据访问层
#### DbContext接口
1. 接口名以 DbContext 为后缀
1. 接口添加 `[ConnectionStringName]` 特性
1. DbSet 属性不要定义 setter

#### DbContext类
1. 类名以 DbContext 为后缀, 类名应与接口名保持一致
1. 类添加 `[ConnectionStringName]` 特性
1. 添加以下静态属性
    - TablePrefix 
    - Schema 
1. 定义 ModelBuilder 的扩展方法 ConfigModuleName, 用来配置DbContext 类的数据库映射

#### 存储库类
1. 注入 DbContext接口 而非 DbContext类
1. 使用帮助方法 GetCancellationToken 来传递 cancellationToken 
    - 如果调用的代码没有提供 cancellationToken token, 则GetCancellationToken方法会 ICancellationTokenProvider.Token 获取
1. 为每一个包含子集的聚合根实体的 IQueryable<TEntity> 定义一个扩展方法 IncludeDetails, 如:
    ```
    public static IQueryable<IdentityUser> IncludeDetails(
        this IQueryable<IdentityUser> queryable,
        bool include = true)
    {
        if (!include)
        {
            return queryable;
        }

        return queryable
            .Include(x => x.Roles)
            .Include(x => x.Logins)
            .Include(x => x.Claims)
            .Include(x => x.Tokens);
    }
    ```
1. 为每一个包含子集的聚合根实体的 IQueryable<TEntity> 定义一个扩展方法 WithDetails, 如:
    ```
    public override async Task<IQueryable<IdentityUser>> WithDetailsAsync()
    {
        // Uses the extension method defined above
        return (await GetQueryableAsync()).IncludeDetails();
    }
    ```

### HttpApi层
#### 路由命名规范
- 默认路由生成规则如下:
    1. 以`/api`开头
    1. 路由根路径为 `/模块名节`, 采用 `kebab-case` 格式化
        - `Wiz.FileManagement` => `file-management`
    1. 控制器路径, 为控制器类名的规范化(复数形式)
        - 删除 '`Controller`'、'`EafController`' 后缀
        - 转换为 `kebab-case` 格式
        - 如服务类名称是 `ReadingBookController`, 规范化后会变成`/reading-books`
    1. 如果有主键参数且命名为 id, 则添加 `/{id}`
    1. `Action` 路径, 为方法名称的规范化
        - 删除 `Async` 后缀
        - 删除 `HttpMethod` 前缀
            - GET '`GetList`', '`GetAll`', '`Get`', '`Find`', '`FindList`'
            - PUT  '`Put`', '`Update`'
            - DELETE '`Delete`', '`Remove`'
            - POST '`Create`', '`Add`', '`Insert`', '`Post`'
            - PATCH '`Patch`'
        - 删除 控制器本身实体对应实体类名(原则上在方法中不应存在)
            - BookController.FindBooksByNameAsync => books/by-name
        - 如果操作针对明细项, 则应在路径中保留明细项实体的复数形式
            - OrderController.UpdateItemsAsync(Guid id, ...) => orders/{id}/items
        - 转换为 `kebab-case` 格式
        - 如果生成的`Action`路径为空，则不会将其添加到路由中
            - `GetAllAsync` 生成路径为空
            - `GetPhonesAsync` 生成路径为 **phones**
    1. Excel导入导出 `Action`
        - byte[] ExportToExcelAsync => to-excel-content
        - byte[] ImportFromExcelAsync => from-excel-content
        - FileResult ExportToExcelFileAsync => to-excel-file
        - FileResult ImportFromExcelFileAsync => from-excel-file
        - byte[] DownloadExcelTemplateAsync => excel-template-content
        - FileResult DownloadExcelTemplateFileAsync => excel-template-file
        - 此外, 符合上面的规范
    1. 参数路径
        - 如果参数中有且只有另一个以Id(不含参数 id)为后缀的参数, 则会将它做为路由的最后一部分加入
            - 如有一个参数名为 `phoneId`, 则会添加 `/{phoneId}` 到路由的最后部分