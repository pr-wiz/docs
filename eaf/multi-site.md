# MultiSite 隔离
- Site 在此处业务意义为 项目
- 多项目用于创建支持项目隔离数据的应用，在业务只展示选中项目(当前项目)的数据
- 按项目隔离数据有两种实现方式
    1. 手动隔离
        - 在所有对应的业务中都接收 SiteId 参数, 在查询的逻辑代码中添加 SiteId 的查询过滤条件
        - 这种方式会增加业务逻辑的代码复杂度, 易出现忘记, 检查问题麻烦
    1. 自动隔离
        - 通过某种机制自动实现当前Site的接收/存储/查看
        - 通过某种机制实现所有相关业务的查询自动加上 SiteId 的查询过滤条件
        - 这种方式在实现业务逻辑的时不用关心接收 SiteId 参数和在查询中添加 SiteId 的查询过滤条件
- Eaf 实现项目自动隔离机制, 在开发模块业务时无需要显示指定Site参数过滤

## 业务模块应用 MultiSite
1. Domain模块依赖 Wiz.Eaf.MultiSite 
    - 模块类 添加 `[DependsOn(typeof(EafMultiSiteModule))]`
    - 相关实体实现 ISiteData 接口
1. EntityFrameworkCore 层
    - 所有的 DbContext类 应从 EafDbContext 基类 继承
1. Application 层
    - 应用服务继承基类Eaf框架CRUD服务
    - Create
        1. 有两种方式
            1. **建议** CreateDto 不包含 SiteId
                - 框架会自动将当前选择Site的Id赋给新增对象
            1. **不建议** CreateDto包含SiteId
                - 不提供SiteId值, 自动将当前选择Site的Id赋给新增对象
                - 提供SiteId值, 以提供的SiteId赋给新增对象
    - Update
        1. **禁止** 在UpdateDto提供SiteId属性
        1. 即不允许修改业务数据的Site
1. HttpApi.Host 层
    - 承载Site业务服务站点启动模块类的 OnApplicationInitialization 方法 添加 `app.UseMultiSite();` 
        ```
        app.UseMultiSite();
        app.UseAuthorization();
        ```
    - `app.UseMultiSite();` 必须添加在  `app.UseAuthorization();` 之前

1. 调用方(前端、其他应用)
    - 通过 设置 Http请求的 Header来提供当前Site
    - Header的Key为 `EafSite`
        ```
        Header["EafSite"] = "fd865ef8-d1aa-11ec-873c-9befdf4e37ce";
        ```
## Site 数据存储
### 框架内置Site存储
1. **不推荐**, 用于模块开发时, 简化开发过程
1. Site数据存储在配置文件中, 无法通过界面管理Site
1. 使用方法
    - 在启动项目中设置(HttpApi.Host)
        1. 引用 Wiz.Eaf.HttpApi.MultiSite 包
        1. 启动项目模块类 添加 依赖特性
            ```
            [DependsOn(typeof(EafHttpApiMultiSiteModule))]
            ```
        1. 在 appsettings.json 中添加：
            ```
            "MultiSite":{                
                "Sites": [
                    {
                        "Id": "ae4915dc-7ab7-11ec-96e6-e304010eb872",
                        "Name": "Site1",
                        "TenantId": "c3d8faea-7cc2-11ec-913b-378ad5c8768a",
                    },
                    {
                        "Id": "a566dd6e-7ab7-11ec-a4a4-3b84d4eafb73",
                        "Name": "site2"
                    }
                ]
            }
            ```
            - 根据实际情况维护Site数据
                - Id SiteId
                - Name Site名称
                - TenantId 所属租户Id, 如果不设置, 默认为null(表示非租户数据)
### SiteManagement 模块
1. **禁止** 在模块中依赖 SiteManagement 模块
1. 只在应用中依赖 SiteManagement 模块
1. 依赖方式同普通模块
1. 项目数据存储在关系型数据库中             
### 实现自定义存储
1. 实现 ISiteStore 接口并注入
## 配置Site相关选项
### 默认配置选项
1. 设置当前SiteId
    - 在 appsettings.json 文件中添加
        ```
        "MultiSite":{ 
            "CurrentSiteId": "ae4915dc-7ab7-11ec-96e6-e304010eb872"
        }
        ```
    - 一般只用在非Web应用中
1. **不推荐** 禁用 Site 存在和有效性验证
    - 在 appsettings.json 文件中添加
        ```
        "MultiSite":{ 
            "EnableSiteCheck": false
        }
        ```
    - 只在模块开发阶段使用
1. **不推荐** 修改 SiteKey
    - 在 appsettings.json 文件中添加
        ```
        "MultiSite":{ 
            "SiteKey": "EafSite"
        }
        ```
        - **不推荐**
        - 修改后所有微服务和客户端都需要调整一致
        - 修改后需要通知相关应用修改
            1. 前端
            1. 第三方客户端
            
        
## 代码使用当前 Site 信息
### ICurrentSite
1. ICurrentSite 服务提供获取和设置当前 Site 的方法
    1. 在需要的业务服务注入 `ICurrentSite`
        ```
        protected ICurrentSite CurrentSite => LazyServiceProvider.LazyGetRequiredService<ICurrentSite>();
        ```
    1. 获取当前 Site 信息
        ```
        Guid? siteId = CurrentSite.Id;
        ```
        - siteId 如果为null 表示 没有设置当前Site
    1. 设置或变更当前Site
        - 使用 `CurrentSite.Change` 方法
            ```
            CurrentSite.Change(siteId)
            ```
            或
            ```
            using (CurrentSite.Change(siteId))
            {
                return await _tagRepository.GetCountAsync();
            }
            ```
            - 使用 using 表示设置的Site只在当前using块中有效


## 禁用 MultiSite
### 禁用查询数据自动当前Site过滤
- 全局禁用
    - 启动模块类 OnApplicationInitialization 方法 添加
        ```
        Configure<AbpDataFilterOptions>(options =>
        {
            options.DefaultStates[typeof(ISiteData)] = new DataFilterState(isEnabled: false);
        });
        ```
        - 适用于实体需要SiteId, 但所有查询都不需要以 SiteId 过滤的场景
- 局部禁用
    - 在某个具体业务服务方法中禁用 SiteId 过滤
        - 在服务中注入 `IDataFilter` 
            ```
            private readonly IDataFilter _dataFilter;
            ```
        - 将要禁用过滤的查询代码包含在`using`块中
            ```
            using (_dataFilter.Disable<ISiteData>())
            {
                return await _bookRepository.GetListAsync();
            }
            ```
            - 只有`using`块之内的代码会禁用自动Site过滤  
### 禁止在创建时自动给实体赋当前Site的值
1. ***不建议***  ***不建议*** ***不建议***
1. 方法
    - 对应实体 `Create` 方法的 `CreateDto` 添加 `SiteId` 属性且添加`[Required]`特性
    - 手动为 `CreateDto.SiteId` 赋值
    - 创建时为以手动赋值保存
    
