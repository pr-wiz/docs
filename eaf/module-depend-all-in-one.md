# 模块化开发阶段通过单体部署方式承载模块和被依赖模块的方法
## 模块开发原则
1. **重要** 模块(解决方案中src中的层) 不允许依赖被依赖模块的实现, 只允许依赖抽象接口
    - 在 `Application` 层引用被依赖模块的 `Application.Contracts`
    - 确有需要依赖具体实现时先沟通确认
1. **重要** 模块发布的 `nuget` 包要能够实现微服务部署和单体部署
## 单体部署模式
1. **推荐** 在开发模块时, 如果依赖其他模块, 采用单体部署模式, 避免开发时远程调用，降低开发复杂度
1. 模块开发时单体部署主机为 `HttpApi.Host` 层
    - 模块和被依赖模块都部署在 `HttpApi.Host`
### 部署被依赖模块和组件方法
1. 被依赖的模块以 `nuget` 包的方式集成到 `HttpApi.Host`
1. 在 `HttpApi.Host` 中集成 被依赖模块的以下包
    - `xxx.EntityFrameworkCore` 仓储基础设施实现
    - `xxx.Application` 业务服务实现
    - `xxx.HttpApi` Web Api 用于提供 Api, 根据需要确定, 用于承载被依赖模块的 `Api`
    - **注意** 在模块类通过 `DependsOn` 特性添加被依赖模块类的依赖
1. 如被依赖模块 依赖别的模块, 在 `HttpApi.Host` 中集成 被依赖模块所依赖的模块(递归)
    - 在被依赖模块的以下层中查找被依赖包
        - `Application`
        - `Domain`
        - `EntityFrameworkCore`
1. 如被依赖模块(或被依赖模块所依赖的模块) 依赖某个组件抽象, 在` HttpApi.Host` 中集成 依赖的抽象组件实现, 如以下抽象：
    - `Wiz.Eaf.IoT`
    - `Wiz.Eaf.ExcelIO`
    - `Volo.Abp.BlobStoring`
    - `Volo.Abp.BackgroundJobs.Abstractions`
    - `Volo.Abp.BackgroundWorkers`
    - `Volo.Abp.Caching`
    - `Volo.Abp.EventBus`
1. 在 `HttpApi.Host` 中`EntityFrameworkCore`目录下的 XxxMigrationsDbContext 类中添加 被依赖模块的数据模型配置, 如
    ```
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        // 被依赖模块的数据模型配置
        modelBuilder.ConfigureDataCategoryManagement();
        modelBuilder.ConfigureMeasuringPointManagement();
        modelBuilder.ConfigureTagManagement();
        modelBuilder.ConfigureFileManagement();

        // 当前模块的数据模型配置
        modelBuilder.ConfigureThresholdAlarmManagement();
    }
    ```
### 添加被依赖模块和组件需要的配置
    - 查看模块说明
    