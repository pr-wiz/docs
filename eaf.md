# 企业应用框架
- 0.2.0
    - 基于 Abp 4.4.4
    - 增加 Wiz.Eaf.MiniProfiler
- 0.1.0
    - 基于Abp 4.4.2


## 模块
### 使用方法
- 通过**AbpHelper CLI**工具快捷引用
    ```
    abphelper module add Wiz.Eaf -soalch
    abphelper module add Wiz.Eaf -e
    ```
    - `Wiz.Eaf` 为框架模块名称
    - 可以通过 -v 指定框架版本, 如：**-v 0.1.0** 指定引用框架版本为`0.1.0`, 可省略, 默认为引用最新的版本
    - **-socahle** 模块选项, 参考**AbpHelper CLI**工具使用方法
    - 注意: **模块名区分大小写**, 如: `wiz.eaf` 和 `Wiz.Eaf` 不同

### ~~为每种数据库创建一个EF项目(仅针对插件式加载模块且要实现数据迁移需要) 如: *.EntityFrameworkCore.MySQL8~~ 
1. 在此项目中生成数据库架构
1. 在此项目中添加一个 `DbSchemaContributor` 类, 继承 `EafDbSchemaContributor` 类, 实现迁移模块本身的数据架构
    ```
    [Dependency(ReplaceServices = true)]
    public class EascMySQL8DbSchemaContributor 
        : EafDbSchemaContributor<EascMySQL8MigrationsDbContext>, ITransientDependency
    {
        public EascMySQL8DbSchemaContributor(EascMySQL8MigrationsDbContext dbContext, ICurrentTenant currentTenant) :base(dbContext, currentTenant)
        {
            
        }        
    }
    ```

### 统一编码风格
- 复制 [.editorconfig](tools/.editorconfig) 到与解决方案文件相同的目录下

### 修改项目公共属性文件 `common.props` 
1. 修改 **PropertyGroup** 如下:
    ```
    <PropertyGroup>
        <LangVersion>latest</LangVersion>    
        <VersionPrefix>0.1.0</VersionPrefix>
        <VersionSuffix></VersionSuffix>
        <NoWarn>$(NoWarn);CS1591</NoWarn>
        <AbpProjectType>module</AbpProjectType>
        <Company>深圳鹏锐信息技术股份有限公司</Company>
        <Product>Wiz.Eaf</Product>
        <Authors>PR</Authors>
        <GenerateDocumentationFile>true</GenerateDocumentationFile>
    </PropertyGroup>
    ```
    - `VersionPrefix` 版本主要部分
    - `VersionSuffix` 版本后缀, 如 `alpha`, `beta`, `beta2` 等
    - `AbpProjectType` Abp项目类别, 可选值为 `app`, `module`
    - `Company` 公司名称
    - `Product` 产品名称
    - `Authors` 作者
    - `GenerateDocumentationFile` 生成文档文件
    - 针对abp vnext 4.4.4 框架的版本开发版本命名为 0.2.x

### 安装工具 
- 复制 [restore_wiz_packages.bat](tools/restore_wiz_packages.bat) 到与解决方案文件相同的目录下

### 添加打包方案 nuget-package.sln
1. 建立一解决方案, 名称为 nuget-package.sln (用于生成模块nuget包)
1. 此方案只包含 src 目录下的项目

### 领域层
- 树形实体应实现 `IPathTree` 接口
- 树形实体的仓储接口应继承 `IPathTreeRepository` 接口
- 具有叶子节点属性的(树形)实体就实现 `IHasLeaf` 接口
- 具有是否启用属性的实体应实现 `IIsActive` 接口
- 具有 `SiteId` 属性且需要按SiteId过滤的实体应实现 `ISiteData` 接口
    
### 应用层服务
#### CRUD服务
- 服务继承自框架基类
    1. `*.Application.Contracts` 中定义的服务接口继承 `IEafCrudAppService` 接口
    1. `*.Application` 中定义的服务继承 `EafCrudAppService`类
    1. 树形实体 
        - `*.Application.Contracts` 中定义的服务接口继承 `IEafPathTreeCrudAppService` 接口
        - `*.Application` 中定义的服务继承 `EafPathTreeCrudAppService`类
    1. 具有叶子属性的树形实体 
        - `*.Application.Contracts` 中定义的服务接口继承 `IEafHasLeafPathTreeCrudAppService` 接口
        - `*.Application` 中定义的服务继承 `EafHasLeafPathTreeCrudAppService`类
#### AutoMapper 配置
1. 需要配置所有做自动转换的类型
    - 默认是以属性名称是否相同来确定是否匹配
    - InputDto => Entity 
        1. 使用 MemberList.Source
            - InputDto 中的所有属性都需要能映射到 Entity对应的属性
        1. 如果 InputDto 中的 PropertyA 属性在 Entity 中没有匹配, 有两种处理方式
            - 不校验
                ```
                CreateMap<InputDto, Entity>(MemberList.Source)
                    .ForSourceMember(src => src.PropertyA, opt => opt.DoNotValidate());
                ```
            - 匹配 Entity 中的另一属性 PropertyB
                ```
                CreateMap<InputDto, Entity>(MemberList.Source)
                    .ForMember(dest => dest.PropertyB, opt => opt.MapFrom(src => src.PropertyA));
                ```
    - Entity => OutputDto
        1. 使用 MemberList.Destination
            - Entity 中的所有属性都需要能映射到 OutputDto 对应的属性
        1. 如果 Entity 中的 PropertyA 属性在 OutputDto 中没有匹配, 有两种处理方式
            - 不校验
                ```
                CreateMap<Entity, OutputDto>(MemberList.Destination)
                    .ForSourceMember(src => src.PropertyA, opt => opt.DoNotValidate());
                ```
            - 匹配 OutputDto 中的另一属性 PropertyB
                ```
                CreateMap<Entity, OutputDto>(MemberList.Destination)
                    .ForMember(dest => dest.PropertyB, opt => opt.MapFrom(src => src.PropertyA));
                ```
    - 忽略匹配
        1. 如果要忽略 dest 中 PropertyA 的匹配(即映射后 dest 中的 PropertyA 保持原样)
            ```
            CreateMap<src, dest>(MemberList.XXX)
                .ForMember(dest => dest.PropertyA, opt => opt.Ignore());
            ```
        1. 一般用于有明细项的聚合根 InputDto 到 聚合根实体的映射，用于忽略明细项列表属性
            - 如果不忽略 明细项列表 属性, 则会出现以下效果：先删除所有明细项，再重新添加 InputDto 中的明细项
            - 忽略后, 在 UpdateAsync 时需要对明细进行判断处理: 已有为更新, 不存在为新增
            - 暂时有个缺陷，不能删除明细项，需要单独处理
    - 不校验 (尽量避免, 以防止漏掉要映射的属性而不给出错误提示)
        1. 使用 MemberList.None
        1. 只映射 src 和 dest 中相同名称的属性， dest 中属性在 src 中不存在时映射后保持原样
1. 关于聚合根明细项列表属性
    1. **重要**明细项列表属性必须有 Set, 一般采用 Protected Set, 否则在实体映射新增时不会映射!
        

### EntityFrameworkCore层
- DbContext 应继承 `EafDbContext` 基类
- 仓储类继承框架基类
    1. 树形实体仓储继承 `PathTreeRepository`类

### HttpApi层
- Controller
    1. 应定义为 `[RemoteService]`
    1. 应按照规范定义 `[Route]`
        - api/模块节/controller节
            1. 模块节: 模块名称的 `kebab-case` 格式化, 如 `Wiz.FileManagement` => `file-management`
            1. controller节: controller类名称的 `kebab-case` 格式化, 如 `FileController` => `file`
        - 如 `Wiz.FileManagement` 模块的 `FileController`, 其**route**应设置为 `[Route("api/file-management/file")]`
- Action
    1. 应按照规范定义 `[Route]`

### HttpApi.Host层
1. 模块类 ConfigureServices 方法中配置
    1. 添加自动Api
        ```
        Configure<AbpAspNetCoreMvcOptions>(options =>
        {
            context.Services.ConfigAutoApi(applicationAssemblies =>
            {
                applicationAssemblies.ForEach(assembly =>
                {
                    if (assembly.FullName.ToLower().StartsWith("wiz"))
                    {
                        options.ConventionalControllers.Create(assembly, opts =>
                        {
                            bool useV3UrlStyle = opts.UseV3UrlStyle == true;

                            var moduleName = assembly.FullName.Split('.')[1];

                            opts.RootPath = useV3UrlStyle ? moduleName.ToCamelCase() : moduleName.ToKebabCase();
                        });
                    }
                });
            }, onlyPluginModule: false);
        });
        ```
    1. 添加Swagger文档,增加以下内容
        ```
        // 包含XmlComments文件
        var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
        foreach (var xmlCommentFile in xmlCommentFiles)
        {
            options.IncludeXmlComments(xmlCommentFile, true);
        }
        ```

        修改相关内容如下:
        ```
        context.Services.AddAbpSwaggerGenWithOAuth(
        configuration["AuthServer:Authority"],
        new Dictionary<string, string>
        {
            {"FileManagement", "FileManagement API"}
        },
        options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo { Title = "FileManagement API", Version = "v1" });
            options.DocInclusionPredicate((docName, description) => true);
            // 包含XmlComments文件
            var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
            foreach (var xmlCommentFile in xmlCommentFiles)
            {
                options.IncludeXmlComments(xmlCommentFile, true);
            }
            options.CustomSchemaIds(type => type.FullName);
        });
        ```

1. 模块类 OnApplicationInitialization 方法中配置
    1. 应用 SiteId 过滤(可选，根据实际情况), 添加以下代码
        ```
        app.UseMultiSite();
        ```
        - 在 `app.UseAuthorization();` 之前调用
## **应用程序**
### 使用方法
- 通过**AbpHelper CLI**工具快捷引用
    ```
    abphelper module add Wiz.Eaf -soalch
    abphelper module add Wiz.Eaf -e
    ```
    - `Wiz.Eaf` 为框架模块名称（引用其他模块也使用此方法)
    - 可以通过 -v 指定框架版本, 如：**-v 0.1.0** 指定引用框架版本为`0.1.0`, 可省略, 默认为引用最新的版本
    - **-socahle** 模块选项, 参考**AbpHelper CLI**工具使用方法
    - 注意: **模块名区分大小写**, 如: `wiz.eaf` 和 `Wiz.Eaf` 不同
### Domain
1. 创建 `DbMigrationService` 服务类(仅针对插件式加载模块且要实现数据迁移需要), 继承 `EafDbMigrationService`, 用于实现EF迁移
    ```
    public class DbMigrationService : EafDbMigrationService<DbMigrationService>
    {
        public DbMigrationService(IDataSeeder dataSeeder, IEafDbSchemaMigrator dbSchemaMigrator, ICurrentTenant currentTenant) : base(dataSeeder, dbSchemaMigrator, currentTenant)
        {
        }

        public override Task MigrateAsync(bool addInitialMigrationIfNotExist = false)
        {
            // ...
        }
    }
    ```
### EntityFrameworkcore
1. 在 `DbContext` 类的 `OnModelCreating` 方法中添加依赖模块配置
    ```
    builder.ConfigureXXX();
    ```
1. 在此项目中生成数据库架构
    - 设置 DbMigrator 为启动项目
    - 设置 DbMigrator 项目中数据库连接
    - 在PMC中 EntityFrameworkCore 项目执行
        ```
        Add-Migration Intial
        ``` 
1. ~~创建 `DbSchemaContributor` 类, 继承 `EafDbSchemaContributor` 类, 用来迁移公共和应用本身的数据架构(仅针对插件式加载模块且要实现数据迁移需要)~~
    

## ~~DbMigrator (仅针对插件式加载模块且要实现数据迁移需要)~~
1. 引用 `Wiz.Eaf.Core`
1. 在启动类的`StartAsync`方法中添加 `options.Services.ConfigurePluginSource(System.IO.Directory.GetCurrentDirectory(), options);`
1. 在模块类中增加 `context.Services.AddTransient<IEafDbSchemaMigrator, EafDbSchemaMigrator>();`

### HttpApi.Host / Web  中加载 **模块插件**
#### 代码
1. 在 Startup 类 ConfigureServices 方法中增加配置插件方法：
    ```
    services.AddApplication<MultiDbHttpApiHostModule>(options =>
            {
                services.ConfigurePluginSource(Path.Combine(services.GetHostingEnvironment().ContentRootPath, $"..{Path.DirectorySeparatorChar}"), options);
            });
    ```
1. 模块类 ConfigureServices 方法中配置
    1. 添加自动Api
        ```
        Configure<AbpAspNetCoreMvcOptions>(options =>
        {
            context.Services.ConfigAutoApi(applicationAssemblies =>
            {
                applicationAssemblies.ForEach(assembly =>
                {
                    if (assembly.FullName.ToLower().StartsWith("wiz"))
                    {
                        options.ConventionalControllers.Create(assembly, opts =>
                        {
                            bool useV3UrlStyle = opts.UseV3UrlStyle == true;

                            var moduleName = assembly.FullName.Split('.')[1];

                            opts.RootPath = useV3UrlStyle ? moduleName.ToCamelCase() : moduleName.ToKebabCase();
                        });
                    }
                });
            }, onlyPluginModule: false);
        });
        ```
    1. 添加Swagger文档,增加以下内容
        ```
        // 包含XmlComments文件
        var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
        foreach (var xmlCommentFile in xmlCommentFiles)
        {
            options.IncludeXmlComments(xmlCommentFile, true);
        }
        ```

        修改相关内容如下:
        ```
        context.Services.AddAbpSwaggerGenWithOAuth(
        configuration["AuthServer:Authority"],
        new Dictionary<string, string>
        {
            {"FileManagement", "FileManagement API"}
        },
        options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo { Title = "FileManagement API", Version = "v1" });
            options.DocInclusionPredicate((docName, description) => true);
            // 包含XmlComments文件
            var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
            foreach (var xmlCommentFile in xmlCommentFiles)
            {
                options.IncludeXmlComments(xmlCommentFile, true);
            }
            options.CustomSchemaIds(type => type.FullName);
        });
        ```
    1. 注册验证服务时 启用 `Bearer` 认证架构 (仅无UI且没有集成IdentityServer4时可选)
        ```
        context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
        ```
        - 效果
            - 当没有登录的用户访问时：
                1. 如果配置为`JwtBearerDefaults.AuthenticationScheme`, 抛出401错误，不会转到登录界面
                1. 否则，不抛出401错误，转到登录界面
        **注意** 集成IdentityServer4时不要传入JwtBearerDefaults.AuthenticationScheme参数, 否则会报错：
            ```
            System.NullReferenceException: Object reference not set to an instance of an object. 
            at IdentityServer4.Extensions.StringExtensions.AddQueryString(String url, String query) 
            ```

1. 模块类 OnApplicationInitialization 方法中配置
    1. 应用 SiteId 过滤(可选，根据实际情况), 添加以下代码
        ```
        app.UseMultiSite();
        ```
        - 在 `app.UseAuthorization();` 之前调用
        ```
#### 配置
1. 在`appsettings.json`中增加以下配置节`Plugins`:
    ```
    "Plugins": {
        // 启用数据库迁移
        "EnableDbMigration": "true",
        // 隐藏模块菜单
        "HiddenMenu": "false",
        // 插件位置
        "Paths": [
        {
            "Folder": "D:\\work\\git\\abp\\demo\\modules\\easc\\src\\WIZ.Easc.EntityFrameworkCore.MySQL8\\bin\\Debug\\net5.0",
            "IncludeSub": "false"
        }
        ]
    }
    ```
    - `EnableDbMigration` 是否启用数据库迁移
    - `HiddenMenu` 是否隐藏模块菜单
    - `Paths` 插件集
        - `Folder` 插件目录, 绝对路径, 如要使用相对路径请加前置 `.` `..`, `./` 表示当前程序目录
            目录分隔符请按实际的操作系统配置, **Windows** 为 `\`, **Linux** 为 `/`
        - `IncludeSub` 是否包含子目录, 默认为 `false`

1
## 工具
### 重新安装Wiz相关Nuget包脚本
- 在开发时有用, 能够在**同版本**的包更新后删除本地缓存从nuget服务器重新下载安装
- 不会升级包的版本
- 只会重新安装公司内部以wiz.开头的包
- 工具安装方式:
    1. 将 [restore_wiz_packages.bat](tools/restore_wiz_packages.bat) 复制到与解决方案文件相同的目录下
    1. 使用方法: 
        - 在vs中清理解决方案
        - 双击 `restore_wiz_packages.bat`
        - 完成后 在vs中生成解决方案(生成 不是 重新生成)
        - 验证是否更新成功（通过检查 更新后 新增或删除的项)    
        - 如果不成功(重试后不行再用下面的方法)
            - 在解决方案先移除要更新的nuget包
            - 双击 `restore_wiz_packages.bat`
            - 在解决方案添加移除的nuget包

## 其他规范
1. 所有源码文件以UTF-8编码
1. 所有模块项目的根目录对应的名称空间为 模块顶级名称空间, 如果有子名称空间, 应建立对应层级子目录
    - 如: `Wiz.Easc.Domain` 项目根目录对应名称空间为 `Wiz.Easc`
    - 如在 `Wiz.Easc.Domain` 项目中有名称空间 `Wiz.Easc.Resources`, 则应在根目录中创建子目录 `Resources`, 并将此名称空间下的文件放在此目录下
    - **模块类**的名称空间 **必须**是 模块**顶级名称空间**
    - 如果有名称空间不符合 `Wiz.Easc` 开头
        - 只允许在**扩展类/接口方法**中使用
        - 与扩展类/接口的名称空间保持一致
        - 应按照扩展类的名称空间层次在根目录中创建子目录层次
            - 如扩展 `IServiceCollection` 接口方法
                ```
                namespace Microsoft.Extensions.DependencyInjection
                {
                    /// <summary>
                    /// 插件扩展方法
                    /// </summary>
                    public static class ServiceCollectionPluginExtensions
                    {
                        /// <summary>
                        /// 配置插件源
                        /// </summary>
                        /// <param name="services">IServiceCollection</param>
                        /// <param name="basePath">应用程序根目录</param>
                        /// <param name="options">Abp应用程序创建选项</param>
                        /// <returns></returns>
                        public static IServiceCollection ConfigurePluginSource(
                            this IServiceCollection services,
                            string basePath,
                            AbpApplicationCreationOptions options)            
                        {
                ```
                - 在根目录下创建以下目录层次结构
                    ```
                    + Microsoft
                      + Extensions
                        + DependencyInjection
                          - ServiceCollectionPluginExtensions.cs
                    ```

1. 所有继承自 `DomainService` 和 `ApplicationService` 已注入日志, 不要自行定义
    - 日志属性名称为 `Logger`

1. 应为所有的业务异常定义异常编码和本地化消息
    - 对于本地化消息中有参数的应定义一个自定义业务异常类

1. 应用服务接口输入输出参数中不能有实体类型(以`Dto`代替)
    - 对于输入`Dto`, 应根据实际情况添加验证特性 `data annotations` (如: `Required`)

1. Dto
    - dto应尽量从基类中继承，如:
        - `EntityDto<TKey>`
        - `CreationAuditedEntityDto<TKey>`
        - `AuditedEntityDto<TKey>`
        - `FullAuditedEntityDto<TKey>`
    - 聚合根dto应从  **extensible DTO ** 继承，如：
        - `ExtensibleAuditedEntityDto<TKey>`

1. `ApiController` 
    - 应为每一个应用接口实现**Controller**
    - 如果使用动态Api则不需要定义`ApiController`

1. 应为每个应用服务的业务定义集成测试方法
    - 测试方法应覆盖尽可能多的业务逻辑, 包括正常的和异常的
    - 测试方法使用的为内存数据库, 每次启动会重置数据(初始化数据对测试类有效)
    - 测试方法名应为 `Should_` 或 `Should_Not_` 开头, 采用`Pasical`命名并以下划线`_`隔开单词

1. 租户实体应实现多租户接口 `IMultiTenant`
    - **重要** TenantId 必须有设置器, 一般建议为私有设置器
        ```
        public virtual Guid? TenantId { get; private set; }
        ```

1. 关于日志器
    - 所有应用服务，领域服务，Controller服务基类已经包含日志器属性 `Logger`, 继承这些基类的类不需要自行注入日志器
    - 如需要日志器, 应通过属性注入而非构造方法注入的方式注入日志器, 日志器属性应为 protect, 名称为 Logger
        ```
        protected ILogger<MyClass> Logger { get; set; }
        public MyClass()
        {            
            Logger = NullLogger<MyClass>.Instance;
        }
        ```

## 多租户
1. 在设计时应明确 领域实体 是否为需要实现多租户
    - 隔离性: 不同的租户创建的实体实例不会被其他租户查看和操作
    - 原则上所有的业务实体都应设计为多租户实体
1. 实体实现多租户
    - 聚合根 实现 `IMultiTenant` 接口
    - 非聚合根实体 不要实现 `IMultiTenant` 接口
        - 非聚合根独立仓储的需要实现 `IMultiTenant` 接口
    - 输入Dto 不包含 `TenantId`
    - 输出Dto 根据需要确定 是否包含 `TenantId`
        - 原则上不包含 `TenantId`
1. 应用启用多租户
    - 在 Domain.Shared 项目中 将常量 MultiTenancyConsts 初始化为 true
        ```
        public const bool IsEnabled = true;
        ```
    - 在启动模块类的 OnApplicationInitialization 方法增加判断
        ```
        if (MultiTenancyConsts.IsEnabled)
        {
            app.UseMultiTenancy();
        }
        ```
    - 多租户多数据库
        - 为要拥有独立数据库 租户 配置数据库连接
        - 执行数据库迁移程序 DbMigrator
    
    - 数据迁移
        - 增加或修改租户数据库连接时
            - 执行数据库迁移程序 DbMigrator
        - 更新模块或实体发生变化时
            - 生成架构迁移信息 Add-Migration xxx
            - 执行数据库迁移程序 DbMigrator

1. 关于多租户多数据库
    - 必须拥有 `Host` 数据库
    - 如果租户没有配置数据库连接, 则会使用 `Host` 数据库
    - 不同租户的数据库连接可以指向同一个数据库, 即多个租户使用一个数据库
    - 禁用租户数据过滤时只会在租户连接的数据库上有效, 对于不同数据库的其他租户信息无效
        - 禁用租户数据过滤后可以查看租户连接数据库中其他租户的数据，但不能查看其他数据库中其他租户的数据
        - 如果确实有这种跨数据库需求, 应自己实现
1. 关于数据库连接名称和连接字符串取值规则
    - 每个模块都定义了模块的连接字符串名称
    - Host:
        - 模块连接字符串取值规则(**依次判断**)：
            1. 配置中存在连接字符串名称, 则取此配置连接值
            1. 配置中存在 Default, 则取此配置连接值
            1. 返回空
    - 多租户：
        - 模块连接字符串取值规则(**依次判断**)：
            1. 当前租户配置了连接字符串名称, 则取此配置连接值
            1. 当前租户配置了 Default, 则取此配置连接值
            1. 取 Host 连接字符串
1. 简化多租户多数据库配置
    - 按数据库来配置连接 **DbConnectionStringName**
        1. 配置一个 Default 数据库连接, 用来做为默认的数据库连接
        1. 为每个数据库定义一个数据库连接字符串, 名称根据业务命名
        1. 多个不同的 数据库连接字符串 可以指向同一个实际的数据库
    - 配置每个模块使用的数据库连接 **ConnectionStringName**
        1. 连接字符串名称: 模块中的 `DbContext` 类的 `ConnectionStringName` 特性名称, 可以有多个
        1. 为每个 数据库连接字符串名称 配置 模块连接字符串名称 映射
        1. 为每个 数据库指定是否给租户使用 `IsUsedByTenants`
    - 取值规则(参考 `MultiTenantConnectionStringResolver.Resolve`)
        1. 模块 **ConnectionStringName** 是否存在 租户 **DbConnectionStringName**, 如果存在, 则返回此连接字符串
        1. 模块 **ConnectionStringName** 是否存在映射的 **DbConnectionStringName**, 如果存在且此映射允许租户使用`IsUsedByTenants==true`
            - 是否存在 租户 **DbConnectionStringName**, 如果存在, 则返回此连接字符串
        1. 是否存在 Default 租户数据库连接, 如果存在, 则返回 租户 Default 数据库连接
        1. 模块 **ConnectionStringName** 是否存在 Host **DbConnectionStringName**, 如果存在, 则返回此连接字符串
        1. 模块 **ConnectionStringName** 是否存在映射的 Host **DbConnectionStringName**, 如果存在, 则返回映射的数据库连接字符串
        1. 是否有 Default Host **DbConnectionStringName**, 如果有, 则返回 Default 数据库连接
        1. 返回空
    - 特殊处理
        1. 模块中的 `DbContext` 类如果定义了 `IgnoreMultiTenancyAttribute` 特性, 则此DbContext不提供租户独立数据库(即直接使用Host数据库配置) (参考 `UnitOfWorkDbContextProvider<TDbContext>.ResolveConnectionString` `DbContextOptionsFactory.ResolveConnectionString`)
        1. 没有实现`IMultiTenant`接口的聚合根(以及有独立仓储的实体)始终保存在Host数据库中 (参考 `EfCoreRepository<TDbContext, TEntity>.GetDbContext`)
        1. 在新增租户实体时租户信息需要在应用层设置(如果继承自`CrudAppService`基类则已内置实现)
            - `EafApplicationService` 内置受保护方法 `BeforeCreate` 可在创建实体保存前调用用来设置 `Id` 和 `TenantId`
    - 示例
        1. `appsettings.json` 配置
            ```
            "ConnectionStrings": {
                "Default": "Server=localhost;Port=3306;Database=MultiDb;Uid=admin;Pwd=P@ssw0rd;",
                "Admin": "Server=localhost;Port=3306;Database=MultiDb_Common;Uid=admin;Pwd=P@ssw0rd;"
            },
            "ConnectionStringMaps": [
                {
                    "DatabaseName": "Admin",
                    "MappedConnections": [             
                        "AbpIdentity",
                        "AbpIdentityServer"
                    ],
                    "IsUsedByTenants": true
                }
            ]
            ```
            - `AbpIdentity` 和 `AbpIdentityServer` 使用 `Admin` 数据库连接字符串
            - 其他都使用 `Default` 数据库连接字符串
            - 在租户数据库连接配置中可以配置 `Admin` 数据库连接字符串 来代替配置 `AbpIdentity` 和 `AbpIdentityServer`两个数据库连接
        1. Eaf 框架已内置支持配置


1. 多租户管理员账号
    - 没有配置单独数据库的租户 为创建时租户时提供的管理账号信息
    - 配置单独数据库的租户管理员为 admin/1q2w3E*

## 基础服务
1. Wiz.Eaf.SwaggerGenOptionsHelper (Wiz.Eaf.HttpApi)
    - 获取xml注释文档
        ```
        public static string[] GetXmlCommentFiles(string[] xmlSearchPatterns = null, bool replaceDefault = false, string basePath = null)
        ```
        - `xmlSearchPatterns` xml查找模式, 默认为 "*.HttpApi.xml", "*.Application.Contracts.xml", "*.Application.xml"
        - `replaceDefault` 是否替换 默认xml查找模式, 默认为false, 即 默认 + `xmlSearchPatterns`; 如为true, 则当 `xmlSearchPatterns` 有值时, 只使用 `xmlSearchPatterns` 的xml查找模式
        - `basePath` xml文件所在目录, 默认为 当前应用目录(`AppContext.BaseDirectory`)
    - 使用示例:
        ```
        // 包含XmlComments文件
        var xmlCommentFiles = SwaggerGenOptionsHelper.GetXmlCommentFiles();
        foreach(var xmlCommentFile in xmlCommentFiles)
        {
            options.IncludeXmlComments(xmlCommentFile, true);
        }
        ```
1. Wiz.Eaf.MiniProfiler 模块 (0.2.0)
    - 在 HttpApi.Host 项目 引用 Wiz.Eaf.MiniProfiler 模块
    - 在 HttpApi.Host 项目 模块类 中加入依赖特性 `DependsOn`
        ```
        [DependsOn(
        typeof(EafMiniProfilerModule)
        )]
        ```
    - 在配置文件 appsettings.json 中添加配置
        ```
        "MiniProfiler": {
            "Enabled": false,
            "EFCoreEnabled": true,
            "RouteBasePath": "/profiler"
        }
        ```
        - MiniProfiler.Enabled: 是否启用 MiniProfiler, 默认为false
        - MiniProfiler.EFCoreEnabled: 是否启用 EFCore 分析, 默认为false
        - MiniProfiler.RouteBasePath: MiniProfiler 路由基础路径, 默认为 /profiler, 必须以 / 开头
        - **配置修改后必须重启服务才会生效**
    - 如果要添加更多Mini配置, 在 HttpApi.Host 项目 模块类 的 PreConfigureServices(ServiceConfigurationContext context) 方法中增加代码
        ```
        PreConfigure<PreMiniProfilerOptions>(options =>
        {
            options.ConfigureMiniProfilerOptions = options =>
            {
                // 访问分析结果URL的路由基地址, 默认为 /profiler
                // 分析结果路由:
                // /<base>/results-index: 最近分析profile列表, 
                // /<base>/results: 最近(或通过?id={guid}指定的)分析profile视图
                options.RouteBasePath = "/myprofiler";
                (options.Storage as MemoryCacheStorage).CacheDuration = TimeSpan.FromMinutes(60);
                //More options see https://miniprofiler.com/dotnet/AspDotNetCore
            };
        });         
        ```
    - 在HttpApi.Host 项目 模块类的 `OnApplicationInitialization` 方法中添加 `app.UseMiniProfiler();`
        ```
        app.UseMiniProfiler();
        app.UseStaticFiles();            
        ```
        - 设置在 `app.UseStaticFiles();`  之前
    - 使用(启用时)
        1. 在Web页面的左上角显示耗时
        1. http://localhost:58117/profiler/results-index
            - 以UI的方法返回最近的分析列表
        1. http://localhost:58117/profiler/results-list
            - 以json格式返回最近的分析列表
        1. http://localhost:58117/profiler/results
            - 以UI的方式显示最后一次分析的视图
        1. http://localhost:58117/profiler/results?id=ae0da733-3762-476a-958d-a23b2668c35e
            - 以UI的方式显示指定分析的视图

### 错误处理
#### 使用方法(弃用)
1. 在启动模块的 `ConfigureServices` 方法的最后添加
    ```
    context.Services.AddEafExceptionHandler(options => options.StopFailedRedirect = true);
    ```
    - `options.StopFailedRedirect` 表示在授权失败时是否停止跳转(如跳转到登录页面), 默认为 `false`, 表示按标准流程. 设置为`true`时停止跳转。
    - `AddEafExceptionHandler` 方法可以不传参数, 则 `StopFailedRedirect = false`
1. 在启动模块的 `OnApplicationInitialization` 方法中添加
    ```
    `app.UseEafExceptionHandling();`
    ```
    - 必须设置在Abp错误处理消息中间件之后
    - 必须设置在 `app.UseIdentityServer();` 和 `app.UseAuthorization();` 之前   

#### 说明
1. 检查授权结果并根据设置是否停止跳转
1. 判断 `Action` 返回是否为对象, 如果为对象则返回友好提示, 示例: 
    ```
    {
        "error": {
            "code": "Volo.Authorization:010001",
            "message": "Authorization failed! Given policy has not granted.",
            "details": null,
            "data": {
                "statusCode": 403
            },
            "validationErrors": null
        }
    }
    ```
    - error.data.statusCode 为错误对应的http status code
1. 如果返回不为对象, 则直接抛出异常

## 基类
### 应用层
1. EafCrudAppService 继承 CrudAppService    
    1. 新增实体
        ```
        public override async Task<TGetOutputDto> CreateAsync(TCreateInput input)
        {
            // 检查权限
            await CheckCreatePolicyAsync();

            // 映射输入
            
            // 创建前校验
            await ValidateEntityBeforeCreateAsync(entity);

            // 实体校验
            await ValidateEntityAsync(entity);

            // 保存前处理
            await OnEntitySaveAsync(entity, EntitySaveType.Insert);

            // 调用仓储插入数据
            await Repository.InsertAsync(entity, autoSave: true);

            // 映射输出
            return await MapToGetOutputDtoAsync(entity);
        }

        ```
    1. 修改实体
        ```
        public override async Task<TGetOutputDto> UpdateAsync(TKey id, TUpdateInput input)
        {
            // 检查权限
            await CheckUpdatePolicyAsync();

            // 获取要更新的实体数据
            var entity = await GetEntityByIdAsync(id);

            // 更新前校验
            await ValidateEntityBeforeUpdateAsync(entity, input);

            // 映射输入到分类数据
            await MapToEntityAsync(input, entity);

            // 验证实体
            await ValidateEntityAsync(entity);

            // 保存前处理
            await OnEntitySaveAsync(entity, EntitySaveType.Update);

            await Repository.UpdateAsync(entity, autoSave: true);

            // 映射分类数据到输出
            return await MapToGetOutputDtoAsync(entity);
        }
        ```
    1. 删除实体
        ```
        public override async Task DeleteAsync(TKey id)
        {
            // 检查权限
            await CheckDeletePolicyAsync();
            // 删除前验证
            await ValidateEntityBeforeDeleteAsync(id);
            // 删除
            await DeleteByIdAsync(id);
        }
        ```
    1. 判断实体是否发生变化
        ```
        protected virtual async Task<bool> IsEntityChangedAsync(TUpdateInput input, TEntity entity)
        {
            return await Task.FromResult<bool>(true);
        }
        ```
1. EafPathTreeCrudAppService 继承 EafCrudAppService
    1. 移动实体
        ```
        public virtual async Task MoveAsync(Guid id, Guid? parentId)
        {
            // 验证权限
            await CheckUpdatePolicyAsync();

            var entity = await PathTreeRepository.GetAsync(id);
            if (entity.ParentId == parentId)
            {
                return;
            }

            // 移动前校验
            await ValidateEntityBeforeMoveAsync(entity, parentId);

            //在变更Path之前先查找所有的子实体
            var children = await GetChildrenAsync(id, true, false);

            // ... 逻辑处理
            

            // 校验实体
            await ValidateEntityAsync(entity);

            // 子对象保存前处理
            await OnMoveChildrenSaveAsync(entity, children);

            // ... 子对象逻辑处理

            // 保存前处理
            await OnEntitySaveAsync(entity, EntitySaveType.Move);

            // 更新实体
            await PathTreeRepository.UpdateAsync(entity);
        }
        ```
    1. 查找子树形实体列表
        ```
        public async Task<List<TGetListOutputDto>> FindChildrenAsync(Guid? parentId, bool recursive = false, bool includeDetails = true)
        ```
    1. 递归查找父树形实体列表
        ```
        public async Task<List<TGetListOutputDto>> FindParentsAsync(Guid id, bool includeDetails = false)
        ```
    1. 删除实体
        ```
        public override async Task DeleteAsync(Guid id)
        {
            // 检查权限
            await CheckDeletePolicyAsync();

            // 如果实体不存在，退出
            var entity = await PathTreeRepository.FindAsync(id);
            if (entity == null)
            {
                return;
            }

            // 删除前验证实体
            await ValidateEntityBeforeDeleteAsync(entity);

            var children = await GetChildrenAsync(id, true);
            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    await ValidateEntityBeforeDeleteAsync(child);
                    await PathTreeRepository.DeleteAsync(child);
                }
            }

            await DeleteByIdAsync(id);
        }
        ```

    1. EafHasLeafPathTreeCrudAppService
        1. 设置实体是否叶子实体
            ```
            public async Task UpdateSetIsLeaf(Guid id, bool isLeaf)
            ```
1. EafHasLeafPathTreeCrudAppService  继承 EafPathTreeCrudAppService
    1. 设置实体是否为叶子实体   
        ```    
        public async Task SetIsLeafAsync(Guid id, bool isLeaf)
        ``` 
### HttpApi 层
1. EafController 继承 AbpController 为 抽象基类
1. EafCrudController 继承 EafController 为 抽象基类
    1. 获取实体
        ```
        [HttpGet]
        [Route("{id}")]
        public virtual async Task<TGetOutputDto> GetAsync(TKey id)
        ```
    1. 获取实体列表
        ```
        [HttpGet]
        public virtual async Task<PagedResultDto<TGetListOutputDto>> GetListAsync(TGetListInput input)
        ```
    1. 新增实体
        ```
        [HttpPost]
        public virtual async Task<TGetOutputDto> CreateAsync(TCreateInput input)
        ```
    1. 编辑实体
        ```
        [HttpPut]
        [Route("{id}")]
        public virtual async Task<TGetOutputDto> UpdateAsync(TKey id, TUpdateInput input)
        ```
    1. 删除实体
        ```
        [HttpDelete]
        [Route("{id}")]
        public virtual async Task DeleteAsync(TKey id)
        ```
    1. 批量新增实体
        ```
        [HttpPost]
        [Route("many")]
        public virtual async Task InsertManyAsync(IEnumerable<TCreateInput> input)
        ```
    1. 批量更新实体
        ```
        [HttpPut]
        [Route("many")]
        public virtual async Task UpdateManyAsync(IEnumerable<TUpdateInput> input)
        ```
    1. 批量删除实体
        ```
        [HttpDelete]
        [Route("many")]
        public virtual async Task DeleteManyAsync(IEnumerable<TKey> ids)
        ```
    1. 
1. EafPathTreeCrudController 继承 EafCrudController 为 抽象基类
    1. 移动实体(修改实体的父级实体)
        ```
        [HttpPut]
        [Route("move")]
        public virtual async Task MoveAsync(Guid id, Guid? parentId)
        ```
    1. 查找子树形实体列表
        ```
        [HttpGet]
        [Route("children")]
        [Route("{id}/children")]
        public virtual async Task<List<TGetListOutputDto>> FindChildrenAsync(Guid? id, bool recursive = false, bool includeDetails = true)
        ```
    1. 递归查找父树形实体列表 (从顶级到父级的列表, 不含自身)
        ```
        [HttpGet]
        [Route("{id}/parents")]
        public virtual async Task<List<TGetListOutputDto>> FindParentsAsync(Guid id, bool includeDetails = false)
        ```
    1. 获取根实体列表(不递归)
        ```
        [HttpGet]
        [Route("roots")]
        public virtual async Task<List<TGetListOutputDto>> GetRootListAsync(bool includeDetails = false)
        ```
    1. 获取树形结构Dto(实体及其所有递归子集以树形结构的单实体返回)
        ```
        [HttpGet]
        [Route("{id}/tree")]
        public virtual async Task<TreeDto<TTreeElement>> GetTreeAsync(Guid id)
        ```
    1. 获取子树结构列表(实体所有的递归子集以树形结构实体列表返回，不含实体本身)
        ```
        [HttpGet]
        [Route("{sub-trees")]
        [Route("{id}/sub-trees")]
        public virtual async  Task<List<TreeDto<TTreeElement>>> GetSubTreesAsync(Guid? id)
        ```
    1. 获取顶级到父级的树形结构(从顶级到父级的以顶级为根节点的单个树形结构, 不含自身)
        ```
         [HttpGet]
        [Route("{id}/parent-tree")]
        public virtual async Task<TreeDto<TTreeElement>> GetParentTreeAsync(Guid id)
        ```
    1. 获取实体某个属性的完整路径
        ```
        [HttpGet]
        [Route("{id}/property-path")]
        public virtual async Task<string> GetPropertyPathAsync(Guid id, string propertyName, string splitChar = "/")
        ```
        - 说明
            1. 完整路径 = 父级完整路径 + 路径分隔符 + 属性值
                - 如果没有父实体, 则完整路径 = 属性值
                - 如果实体不存在, 返回 ""
            1. 示例
                - 假设
                    1. myId对应的树形实体的 Name 属性为 N111
                    1. myId父级对应的树形实体 Name 属性为 N11, Id 属性为 pId
                    1. myId父级对应父级的树形实体 Name 属性为 N1，且为根节点, Id 属性为 rId
                - 调用
                    1. var namePath = await service.GetPropertyPathAsync(myId, "Name");
                        - namePath 的值为 N1/N11/N111
                    1. var namePath = await service.GetPropertyPathAsync(pId, "Name", ".");
                        - namePath 的值为 N1.N11 
                    1. var namePath = await service.GetPropertyPathAsync(rId, "Name", ".");
                        - namePath 的值为 N1 
                    1. var namePath = await service.GetPropertyPathAsync(notExistsId, "Name", ".");
                        - namePath 的值为 ""
1. EafHasLeafPathTreeCrudController 继承 EafPathTreeCrudController 为 抽象基类
    1. 设置实体是否为叶子实体
        ```
        [HttpPut]
        [Route("{id}/set-is-leaf")]
        public virtual async Task SetIsLeafAsync(Guid id, bool isLeaf)
        ```
1. **重要**
    1. 原则上所有的业务控制器都应继承控制器基类，根据实体分类选择相应的基类
    1. 业务控制器的受保护构造方法中, 应注入正确的应用服务和设置正确资源类型   
        ```
        private readonly IMenuGroupAppService _menuGroupAppService;
        /// <summary>
        /// 受保护构造方法
        /// </summary>
        /// <param name="menuGroupAppService">菜单组应用服务</param>
        protected MenuGroupController(IMenuGroupAppService menuGroupAppService):base(menuGroupAppService)
        {
            this._menuGroupAppService = menuGroupAppService;
            LocalizationResource = typeof(MenuManagementResource);
        }
        ```

## 树形实体使用规范
### 定义
1. 所有的树形实体应实现 `IPathTree` 接口
    - 如果实体有 是否叶子节点 需求还应实现 `IHasLeaf` 接口
1. 所有的树形实体应定义仓储, 并实现 `IPathTreeRepository` 接口 (`PathTreeRepository` 基类)
1. 所有的树形实体定义应用服务，并实现 `IEafPathTreeCrudAppService` (`EafPathTreeCrudAppService` 基类)
    - 如果实体有 是否叶子节点 需求则应实现 `IEafHasLeafPathTreeCrudAppService` 接口 (`EafHasLeafPathTreeCrudAppService` 基类)
### 约束
1. 应用服务所有方法的输入输出 不包含 Path, 即Path 对 调用方 不可知
1. 应用服务方法 UpdateAsync 的输入中不包含 ParentId, IsLeaf, 即不允许直接修改这几个项
1. 修改 `ParentId` 由 `IPathTreeRepository.MoveAsync` 方法实现
1. 修改 `IsLeaf` 由 `IEafHasLeafPathTreeCrudAppService.UpdateSetIsLeaf` 方法实现
### 将树形实体列表转换为树形层级结构(列表)
1. 列表中的实体必须实现 `IPathTree` 接口
1. 定义树节点元素: 以 TreeElement 为后缀
    - 树节点元素定义树节点需要的属性
    - 如果以实体已有的输出Dto为树节点元素, 可以跳过. (EafPathTreeCrudAppService 服务如果没有设置TTreeElement, 默认以 TGetOutputDto 为树节点元素)
1. 树形结构为泛形Dto: `TreeDto<TTreeElement>`
    ```
    /// <summary>
    /// 树形结构Dto
    /// </summary>
    /// <typeparam name="TTreeElement"></typeparam>
    public class TreeDto<TTreeElement>
    {
        /// <summary>
        /// 自身元素
        /// </summary>
        public TTreeElement Current { get; set; }
        /// <summary>
        /// 子级集合
        /// </summary>
        public List<TreeDto<TTreeElement>> Children { get; set; } = new List<TreeDto<TTreeElement>>();
    }
    ```
1. 定义从 树形实体 到 树节点元素 的映射配置
    - 如果树节点元素包含明细项
        1. 应定义明细项Dto以及明细项到明细项dto的映射配置
        1. 明细项列表名称应完全相同(或在映射中配置)

## 跨模块引用
1. 跨模块引用时应考虑到单体部署和微服务部署
    - 各层都需要引用, 以便支持单体部署
    - 各层不应使用引用模块的持久服务(仓储相关)方法
    - 应用层/HttpApi层应只通过应用接口使用引用模块
    -

## 关于AutoMapper映射
1. 所有需要自动转换的类型必须在 xxxApplicationAutoMapperProfile 添加映射
    ```
    CreateMap<TSource, TDestination>();
    ```
1. 需要自动转换的类型可以为任意类型(不一定只是实体和Dto)
1. CreateMap 方法校验, 根据参数确定
    - 不带参数: CreateMap<TSource, TDestination>()
        1. 双向校验: 源和目标的所有属性都必须匹配(一一对应)
    - MemberList.Source：CreateMap<TSource, TDestination>(MemberList.Source), 一般用于输入性Dto 到实体的映射
        1. 只校验源: 源中的所有属性都必须被匹配(即源中的所有属性都必须在目标中存在)
    - MemberList.Destination：CreateMap<TSource, TDestination>(MemberList.Destination), 一般用于 实体 到输出性Dto的映射
        1. 只校验目标：目标中的所有属性都必须被匹配(即目标中的所有属性都必须在源中存在)
    - MemberList.None: CreateMap<TSource, TDestination>(MemberList.None), 用于不需要校验的场景
        1. 对源和目标都不做校验: 即不做校验, 只要源和目标中都存在的属性才会被映射

## 关于分页参数说明:

排序: 
    - 应包含要排序的 属性 和 方向(可选)
    - 可以包含多个排序属性, 以逗号(,)隔开
    - 示例:
        1. Name
        1. Name Desc
        1. Name Desc, Code Asc

## MultiSite 隔离
[参考](eaf/multi-site.md)

## 业务模块引用其他业务模块规范
### 注意**重要**
1. 适用于模块不是应用(应用可以理解为承载站点)
1. 业务模块应只依赖被引用模块的接口，不依赖实现
1. 被依赖模块可以独立部署, 也可以和其他模块一起部署
1. 部署被依赖模块的站点需要引用被依赖模块的 Application, EntityFrameworkCore 和 HttpApi 包
    - Application 业务应用服务实现
    - EntityFrameworkCore 业务持久层实现, 生成数据架构迁移
    - HttpApi 控制器实现

### 引用 业务模块
1. 只引用被依赖模块的 `Application.Contracts` 和 `HttpApi.Client`
1. 以依赖 `SiteManagement` 模块为例, 其他模块可参照实现
    - `Application.Contracts` 层引用 `Wiz.SiteManagement.Application.Contracts`
    - `HttpApi` 层引用 `Wiz.SiteManagement.HttpApi.Client`
    - `SiteManagement` 模块服务部署位置：
        1. 由本模块的`HttpApi.Host`承载
            - `HttpApi.Host` 层引用:
                - `Wiz.SiteManagement.Application`
                - `Wiz.SiteManagement.EntityFrameworkCore`
                - `Wiz.SiteManagement.HttpApi`
            - `HttpApiHostMigrationsDbContext` 的 `OnModelCreating` 方法中添加
                ```
                modelBuilder.ConfigureSiteManagement();
                ```
        1. 部署在其它**Host**, 假设为: *https://localhost:37880*
            - `HttpApi.Host` 的 `appsettings.json` 添加远程服务端点配置
                ```
                "RemoteServices": {
                    "Default": {
                    "BaseUrl": "https://localhost:37880/"
                    }                    
                }
                ```               
                或
                ```
                "RemoteServices": {
                    "SiteManagement": {
                        "BaseUrl": "https://localhost:37880/"
                    }
                }
                ```
1. 使用被依赖模块功能方法
    - 直接注入被依赖模块的应用服务接口, 不用管是否部署在本地还是远程
        ```
        private readonly ISiteAppService _siteAppService;
        public MyService(ISiteAppService siteAppService)
        {
            _siteAppService = siteAppService;            
        }
        ```
    

## 关于并发冲突解决方案
### 后端
#### 实体
1. 实现 `IHasConcurrencyStamp` 接口(聚合根对象基类已实现此接口)
    - 具有属性 `string ConcurrencyStamp { get; set; }`
#### Dto
    - 输出Dto 和 编辑Dto
        1. 实现 `IHasConcurrencyStamp` 接口
    - 新增Dto
        1. 禁止拥有 `ConcurrencyStamp` 属性
### 前端
1. 在调用编辑API时用 输出Dto中 ConcurrencyStamp 设置 编辑Dto ConcurrencyStamp


## 关于扩展属性映射
###  要求
1. 源和目标必须实现 `IHasExtraProperties` 接口
1. 配置映射定义
    ```
    CreateMap<Site, SiteDto>()
    ```
1. 效果: 源对象 `ExtraProperties` 属性 完整映射到 目标对象 `ExtraProperties` 属性中
    - 源对象 ExtraProperties 属性的 每个Key都会 映射到 目标对象 `ExtraProperties` 属性中
### 约束
1. 映射定义调用 `MapExtraProperties` 方法
    ```
    CreateMap<Site, SiteDto>()
            .MapExtraProperties(definitionChecks:null,ignoredProperties:null,mapToRegularProperties:false);
    ```
    1. definitionChecks: 属性定义检查(`MapExtraProperties`属性中的每一个Key)
        - null 默认值
            - Key 在源和目标中都存在扩展属性定义, 成功
            - Key 在源中存在扩展属性定义, 且此定义的 CheckPairDefinitionOnMapping 为 false, 成功
            - Key 在目标中存在扩展属性定义, 且此定义的 CheckPairDefinitionOnMapping 为 false, 成功
            - 其他 失败
        - None 
            - 不验证，始终成功
        - Source
            - Key 在源中存在扩展属性定义, 成功
            - 其他 失败
        - Destination
            - Key 在目标中存在扩展属性定义, 成功
            - 其他 失败
    1. ignoredProperties: 定义属性是否忽略(`MapExtraProperties`属性中的每一个Key)
        - null 默认值
            - 继续其他验证
        - 集合
            - 如果 key 存在集合中, 失败
            - 其他 继续其他验证
    1. mapToRegularProperties 是否设置到正常属性值
        - false 默认值
            - 不设置
        - true
            - 对于映射成功的所有属性(`MapExtraProperties`属性中的每一个Key)
                - 目标中如果有同名的可设置正常属性, 则将扩展属性值设置给同名的正常属性, 并从目标对象 `MapExtraProperties` 属性中移除
    
1. 在源对象调用 `MapExtraPropertiesTo`
    ```
    src.MapExtraPropertiesTo(dest, definitionChecks: null, ignoredProperties: null);
    ```
    - src: 源对象
    - dest: 目标对象
    - definitionChecks 同映射定义
    - ignoredProperties 同映射定义

## 模块取消动态API
### 微服务部署网关Api需要
1. 通过网关代理微服务, 不应包含实现
1. 需要Api网关显示代理的Api Swagger 信息
1. 通过引入 HttpApi 项目
    - HttpApi 包含服务控制器元数据, 但不包含服务的具体实现
### 实现
1. 在 Application.Contracts 层定义应用服务接口
    ```
    /// <summary>
    /// XXX 应用服务接口
    /// </summary>
    public interface IXXXAppService : IApplicationService
    ```
1. 在 Application.Contracts 层定义远程服务名称和区域常量
    ```
    /// <summary>
    /// XXX模块远程服务常量类
    /// </summary>
    public class XXXModuleNameRemoteServiceConsts
    {
        /// <summary>
        /// 远程服务名称
        /// </summary>
        public const string RemoteServiceName = "XXXModuleName";
        /// <summary>
        /// 模块名称
        /// </summary>
        public const string ModuleName = "XXXModuleName";
    }
    ```
1. 在 Application 层实现应用服务接口, 并标注 RemoteService 为 false 
    ```
    /// <summary>
    /// XXX应用服务
    /// </summary>
    [RemoteService(false)]
    [Authorize(XXXPermissions.XXX.Default)]
    public class XXXAppService : ApplicationService, IXXXAppService
    ```
1. 在 HttpApi 层实现应用服务接口
    - 注入应用服务接口
    - 服务实现直接调用 应用服务接口 的方法
    ```
    /// <summary>
    /// XXX控制器
    /// </summary>
    [RemoteService(Name = XXXModuleNameRemoteServiceConsts.RemoteServiceName)]
    [Area(XXXModuleNameRemoteServiceConsts.ModuleName)]
    [Route("api/xxx-xxx/xxx-xxx")]
    public class XXXController : AbpController, IXXXAppService
    {
        private readonly IXXXAppService _xxxAppService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="xxxAppService">XXX应用服务</param>
        public XXXController(IXXXAppService xxxAppService)
        {
            this._xxxAppService = xxxAppService;
        }
        /// <summary>
        /// 新增XXX
        /// </summary>
        /// <param name="input">创建XXXDto</param>
        /// <returns>XXXDto</returns>
        [HttpPost]
        public async Task<XXXDto> CreateAsync(CreateXXXDto input)
        {
            return await this._xxxAppService.CreateAsync(input);
        }
    ```
## 待改进/确认项
1. 框架相关模块以Eaf开头(含AppService)
1. 业务相关模块以Wiz开头(含AppService)
1. 不应提供依赖模块的API

## Excel 导入导出组件使用说明
1. 提供从实体类列表导出为Excel文件的方法
1. 提供从Excel文件导入为实体列表的方法
1. 文件以 byte[] 数组方式表示
### 组件结构
1. 抽象层(接口层) Wiz.Eaf.ExcelIO    
    - 特性
        1. ExcelTableAttribute, 用于定义单个Excel Sheet 对应的数据类
            - Name Sheet名称
            - Title 标题, 暂未用到，扩展用
        1. ExcelColumnAttribute, 用于定义 Excel Sheet 中要导入/导出的列 匹配的数据类属性
            - Name 列名称, 如果为空则默认为属性名称
            - Index 列位置, 从0开始, 暂未用到
            - Required 是否必须, 校验用, 默认为 false
            - DateTimeFormat 日期类型格式化字符串, C# Format格式
    - 封装数据类 DataPackage
        - 用于封装多个Sheet页的数据类型
        - 为泛型类型, 共5个, 即最多支持5个Sheet页导入导出
            1. DataPackage
            1. DataPackage<TData1>: DataPackage
            1. DataPackage<TData1, TData2>: DataPackage<TData1>
            1. DataPackage<TData1, TData2, TData3>: DataPackage<TData1, TData2>
            1. DataPackage<TData1, TData2, TData3, TData4>: DataPackage<TData1, TData2, TData3>
            1. DataPackage<TData1, TData2, TData3, TData4, TData5>: DataPackage<TData1, TData2, TData3, TData4>
        - 封装数据类只包含泛型类型参数列表属性, 为每一个类型参数定义一个属性
            1. List<TData1> Data1 { get; set; }
            1. List<TData2> Data2 { get; set; }
            1. List<TData3> Data3 { get; set; }
            1. List<TData4> Data4 { get; set; }
            1. List<TData5> Data5 { get; set; }
    - 导入接口 IExcelImportService         
        - 为泛形接口, 共5个, 即最多支持5个Sheet页导入
        - 泛型参数类型为要导入的 Sheet 页对应的 数据类型
        - 返回类型为 封装数据类
        - 示例：2个sheet页导入接口
            ```
            /// <summary>
            /// 导入Excel服务
            /// </summary>
            /// <typeparam name="TImportData1">导入数据类型1</typeparam>
            /// <typeparam name="TImportData2">导入数据类型2</typeparam>
            public interface IExcelImportService<TImportData1, TImportData2>
            {

                /// <summary>
                /// 从Excel导入数据
                /// </summary>
                /// <param name="inputExcelFile">Excel文件</param>
                /// <returns>集合数据</returns>
                Task<DataPackage<TImportData1, TImportData2>> ImportFromExcelAsync(byte[] inputExcelFile);
            }
            ```
            - 泛型数据类型要导入的属性必须以 ExcelColumn 特性定义
    - 导出接口 IExcelExportService        
        - 为泛形接口, 共5个, 即最多支持5个Sheet页导出
        - 泛型参数类型为要导入的 Sheet 页对应的 数据类型
        - 输入参数为 
            - input 封装数据类
            - templateExcelFile 模板文件, 暂未用上
            - isNeedSeq 是否需要序号列, 默认为 false, 如果为 true, 则在第一列生成递增的序号列
        - 示例：2个sheet页导出接口
            ```
            /// <summary>
            /// 导出Excel服务
            /// </summary>
            /// <typeparam name="TExportData1">导入数据类型1</typeparam>
            /// <typeparam name="TExportData2">导入数据类型2</typeparam>
            public interface IExcelExportService<TExportData1, TExportData2>
            {
                /// <summary>
                /// 导出到Excel
                /// </summary>
                /// <param name="input">要转为Excel的数据列表</param>
                /// <param name="templateExcelFile">模板文件内容</param>
                /// <param name="isNeedSeq">
                /// 是否需要排序列 
                /// 如果是, 则以从1开始的整数递增1生成序号到Excel
                /// 如果有排序列特性的字段 则覆盖递增规则，并按特性约定TExportData数据显示
                /// 否则 不显示
                /// </param>
                /// <returns>Excel文件内容</returns>
                /// <remarks>可以通过特性约定TExportData哪些属性需要导出并指定显示列名</remarks>
                Task<byte[]> ExportToExcelAsync(DataPackage<TExportData1, TExportData2> input, byte[] templateExcelFile = null, bool isNeedSeq = true);
            }
            ```
            - 泛型数据类型要导入的属性必须以 ExcelColumn 特性定义
    - 使用说明：
        1. 在需要使用的层中引用, 一般为 应用层
            - nuget 包
            - 模块类 依赖 
                ```
                [DependsOn(typeof(ExcelIOModule))]
                ```
        1. 在服务中注入 导入导出接口服务
            - 根据需要注入接口
                ```
                private readonly IExcelExportService<TrendAlarmExportOutputDto, TrendAlarmExportOutputDto> _excelExportService;
                public TrendAlarmExcelExportService(IExcelExportService<TrendAlarmExportOutputDto, TrendAlarmExportOutputDto> excelExportService)
                {
                    _excelExportService = excelExportService;
                }
                ```
            - 在方法中调用导入导出服务方法
                ```
                public async Task<byte[]> ExportToExcelAsync(DataPackage<TrendAlarmExportOutputDto, TrendAlarmExportOutputDto> input, byte[] templateExcelFile = null, bool isNeedSeq = true)
                {
                    return await _excelExportService.ExportToExcelAsync(input);
                }
                ```
        

1. 实现层 Wiz.Eaf.ExcelIO.OpenXml
    - OpenXml 导出Excel实现 ExcelExportService
        - 实现 IExcelExportService 接口
    - OpenXml 导入Excel实现 ExcelImportService
        - 实现 IExcelImportService 接口
    - 使用说明
        1. 在需要使用的层中引用, 一般为 Web或HttpApi层
            - nuget 包
            - 模块类 依赖 
                ```
                [DependsOn(typeof(ExcelIOOpenXmlModule))]
                ```

## IoT读写组件使用说明
1. 提供从IoT的读取测点值的服务
1. 提供从IoT的写入测点值的服务
### 组件结构
1. 抽象层(接口层) Wiz.Eaf.IoT   
    - 接口和类说明     
        - 读IoT接口 IIoTReadOnlyService
            - 用于从IoT中读取测点值
            - 所有服务方法返回值都为 IoTReadResult 类型
            - 所有方法输入参数中的时间戳都为 ulong 类型, 参考
                ```
                DateTimeOffset.ToUnixTimeSeconds().To<ulong>();
                ```
            - 服务方法
                1. 获取单个测点最新的值
                    ```
                    /// <summary>
                    /// 获取单个测点最新的值
                    /// </summary>
                    /// <param name="id">测点Id</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值</returns>
                    Task<IoTReadResult> GetValueAsync(string id, string extraData = null);
                    ```
                1. 获取单个测点指定时间的值
                    ```
                    /// <summary>
                    /// 获取单个测点指定时间的值
                    /// </summary>
                    /// <param name="id">测点Id</param>
                    /// <param name="timeStamp">时间戳</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值</returns>
                    Task<IoTReadResult> GetTimeValueAsync(string id, ulong timeStamp, string extraData = null);

                    ```
                1. 获取单个测点指定时间范围的值列表
                    ```
                    /// <summary>
                    /// 获取单个测点指定时间范围的值列表
                    /// </summary>
                    /// <param name="id">测点Id</param>
                    /// <param name="startTimeStamp">开始时间戳</param>
                    /// <param name="endTimeStamp">结束时间戳</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值列表</returns>
                    Task<IoTReadResult> GetValueListAsync(string id, ulong startTimeStamp, ulong endTimeStamp, string extraData = null);
                    ```
                1. 获取多个测点最新的值
                    ```
                    /// <summary>
                    /// 获取多个测点最新的值
                    /// </summary>
                    /// <param name="ids">测点Id列表</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值</returns>
                    Task<IoTReadResult> GetValueForMeasuringPointsAsync(List<string> ids, string extraData = null);
                    ```
                1. 获取多个测点指定时间的值
                    ```
                    /// <summary>
                    /// 获取多个测点指定时间的值
                    /// </summary>
                    /// <param name="ids">测点Id列表</param>
                    /// <param name="timeStamp">时间戳</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值</returns>
                    Task<IoTReadResult> GetTimeValueForMeasuringPointsAsync(List<string> ids, ulong timeStamp, string extraData = null);
                    ```
                1. 获取多个测点指定时间范围的值列表
                    ```
                    /// <summary>
                    /// 获取多个测点指定时间范围的值列表
                    /// </summary>
                    /// <param name="ids">测点Id列表</param>
                    /// <param name="startTimeStamp">开始时间戳</param>
                    /// <param name="endTimeStamp">结束时间戳</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值列表</returns>
                    Task<IoTReadResult> GetValueListForMeasuringPointsAsync(List<string> ids, ulong startTimeStamp, ulong endTimeStamp, string extraData = null);
                    ```
                1. 获取单个测点指定时间戳集合的值列表
                    ```
                    /// <summary>
                    /// 获取单个测点指定时间戳集合的值列表
                    /// </summary>
                    /// <param name="id">测点Id</param>
                    /// <param name="timeStamps">时间戳集合</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值列表</returns>
                    Task<IoTReadResult> GetTimeValuesAsync(string id, ulong[] timeStamps, string extraData = null);
                    ```
                1. 获取多个测点指定时间戳集合的值列表
                    ```
                    /// <summary>
                    /// 获取多个测点指定时间戳集合的值列表
                    /// </summary>
                    /// <param name="ids">测点Id列表</param>
                    /// <param name="timeStamps">时间戳集合</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>测点值列表</returns>
                    Task<IoTReadResult> GetTimeValuesForMeasuringPointsAsync(List<string> ids, ulong[] timeStamps, string extraData = null);
                    ```

        - 读写IoT接口 IIoTWriteService
            - 此接口继承了 IIoTReadOnlyService 接口
            - 服务方法
                1. 设置测点运行时的值
                    ```
                    /// <summary>
                    /// 设置测点运行时的值
                    /// </summary>
                    /// <param name="values">要设置的测点运行值列表</param>
                    /// <param name="extraData">附加信息, 具体实现可能需要，默认为 null</param>
                    /// <returns>操作结果</returns>
                    Task<IoTResult> SetRuntimeValueAsync(List<MeasuringValue> values, string extraData = null);
                    ```    
        - IoT操作结果类 IoTResult   
            ```
            /// <summary>
            /// 操作结果
            /// </summary>
            public class IoTResult
            {
                /// <summary>
                /// 操作结果
                /// 0 成功
                /// 1 部分成功
                /// 其他 发生错误
                /// </summary>
                public int Result { get; set; }
                /// <summary>
                /// 错误消息
                /// </summary>
                public String Message { get; set; }
                /// <summary>
                /// 部分不成功的测点Id列表
                /// 如查询时部分无数据的测点Id列表或保存时部分不成功的测点Id列表
                /// </summary>
                public List<string> FailedMessuringPointIds { get; set; } = new List<string>();
            }
            ```
            - 用于写入操作        
        - 读取操作结果类 IoTReadResult
            ```
            /// <summary>
            /// 读取操作结果
            /// </summary>
            public class IoTReadResult:IoTResult
            {        
                /// <summary>
                /// 获取到的值
                /// </summary>
                public List<MeasuringValues> Data { get; set; }
            }
            ```
        - 测点值列表类 MeasuringValues
            ```
            /// <summary>
            /// 测点值列表
            /// </summary>
            public class MeasuringValues
            {        
                /// <summary>
                /// 测点Id
                /// </summary>
                public virtual string MeasuringPointId { get; set; }
                /// <summary>
                /// 值列表
                /// </summary>
                public virtual List<MeasuringValue> Values { get; set; }
            }
            ```
        - 测点值类 MeasuringValue
            ```
            /// <summary>
            /// 测点值
            /// </summary>
            public class MeasuringValue
            {
                /// <summary>
                /// 测点Id
                /// </summary>
                public virtual string MeasuringPointId { get; set; }
                /// <summary>
                /// 时间戳（10为的时间戳格式 ToUnixTimeSeconds）
                /// </summary>
                public virtual ulong TimeStamp { get; set; }
                /// <summary>
                /// 值
                /// </summary>
                public virtual double Value { get; set; }

                /// <summary>
                /// 值质量
                /// |GOOD|1|正常| 
                /// |BAD|2|坏点|
                /// |FAIR|2|坏点|
                /// |POOR|2|坏点|
                /// </summary>
                public virtual int Quality { get; set; }
            }
            ```
        - 测点类 MeasuringPoint
        - 暂时未使用
        ```
        /// <summary>
        /// 测点
        /// </summary>
        public class MeasuringPoint
        {
            /// <summary>
            /// Id
            /// </summary>
            public virtual string Id { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public virtual string Name { get; set; }


            /// <summary>
            /// 类型
            ///     |AI|1|模拟量|
            ///     |DI|2|开关量|
            ///     |PI|3|包量点
            /// </summary>
            public virtual int Type { get; set; }

            /// <summary>
            /// 单位
            /// 一般值类型才会有单位
            /// </summary>
            public virtual string Unit { get; set; }
        }
        ```
    - 使用说明：
        1. 在需要使用的层中引用, 一般为 应用层
            - nuget 包
            - 模块类 依赖 (仅Abp项目需要)
                ```
                [DependsOn(typeof(EafIoTModule))]
                ```
        1. 在服务中注入 读IoT接口 IIoTReadOnlyService 或 读写IoT接口 IIoTWriteService 服务
            - 根据需要注入接口
                ```
                private readonly IIoTReadOnlyService _ioTReadOnlyService;
                public IotAppService(IIoTReadOnlyService ioTReadOnlyService)
                {
                    _ioTReadOnlyService = ioTReadOnlyService;
                }
                ```
            - 在方法中调用 IoT接口服务方法
                ```
                public virtual async Task<IoTReadResult> GetTimeValueAsync(string id, ulong timeStamp)
                {
                    return await _ioTReadOnlyService.GetTimeValueAsync(id, timeStamp);
                }
                ``` 
1. 实现层 Wiz.Eaf.IoT.Default
    - 接口和类说明
        - WiZIoT读取测点值服务类 IoTReadOnlyService
            - 实现 IIoTReadOnlyService 接口
        - WiZIoT读写测点值服务类 IoTWriteService
            - 实现 IIoTWriteService 接口
        - WizIoT配置选项类 DefaultIoTOptions
        ```
        /// <summary>
        /// IoT配置选项
        /// </summary>
        public class DefaultIoTOptions
        {
            /// <summary>
            /// 配置节点名称
            /// </summary>
            public const string SectionPosition = "DefaultIoT";
            /// <summary>
            /// IoT 地址
            /// </summary>
            public string Address { get; set; }
            /// <summary>
            /// 应用Id
            /// </summary>
            public uint AppId { get; set; }
            /// <summary>
            /// 集团编号
            /// </summary>
            public uint GroupId { get; set; }
            /// <summary>
            /// 电厂编号
            /// </summary>
            public uint FactoryId { get; set; }
        }
        ```
    - `extraData` 参数
        ```
        /// <param name="extraData">
        /// 附加信息: groupId|factoryId|appId
        /// 如果没有提供，则从配置中读取默认值
        /// 示例:
        ///     1. 空或者忽略, groupId,factoryId,appId 都从默认配置中读取
        ///     1. 三者都提供 1|2|3, 表示 groupId=1, factoryId=2,appId
        ///     1. 只提供groupId 1,表示groupId=1,factoryId,appId 从默认配置中读取
        ///     1. 只提供appId ||3, 表示appId=3,groupId,factoryId从默认配置中读取
        ///     1. 只提供groupId和appId 1||3, 表示groupId=1,appId=3, factoryId从默认配置中读取
        /// </param>
        ```

    - 使用说明
        1. 在启动项目中引用, 一般为 HttpApi.Host 或 Web 层 
            - nuget 包
            - 模块类 依赖 (仅Abp项目需要)
                ```
                [DependsOn(typeof(EafIoTDefaultModule))]
                ```
        1. 在启动项目中的模块类的 ConfigureServices 方法中, 调用 AddDefaultIoT() 扩展方法
            ```
            context.Services.AddDefaultIoT();
            ```
            - 如果非 Abp 项目, 在 Startup 类的 ConfigureServices 方法, 调用 AddNonAbpDefaultIoT() 扩展方法
                ```                
                services.AddNonAbpDefaultIoT();
                ```

        1. 在启动项目的配置文件 appsettings.json 中, 添加 配置节, 并配置为正确的信息
            ```
            "DefaultIoT": {
                "Address": "http://192.168.66.206:5050/",
                "AppId": "1",
                "GroupId": "1",
                "FactoryId": "4"
            }
            ```
