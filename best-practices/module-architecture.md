# 模块架构最佳实践和约定
## 解决方案结构
- **推荐** 为每个模块创建一个单独的 Visual Studio 解决方案.
- **推荐** 将解决方案命名为CompanyName.ModuleName(对于ABP核心模块来说,它的命名方式是Volo.Abp.ModuleName).
- **推荐** 分层开发模块，这样会有几个相互关联的包（项目）.
    - 每个包都有自己的模块定义文件，并明确声明依赖包/模块的依赖关系.
## 层和包
下图显示了分层模块的包以及这些包之间的依赖关系：
![avatar](images/module-layers-and-packages.jpg)


最终目标是允许应用程序以灵活的方式使用模块. 应用程序部署方式示例：

- A）`单体`应用程序;
    - 添加对`Web`和`Application`包的引用.
    - 根据首选项添加对EF Core或MongoDB包之一的引用.
    - 效果:
        - 应用程序可以显示`模块的UI`.
        - 它在同一进程中承载`应用层`和`领域层`（这就是为什么它需要引用数据库集成包）.
        - 此应用程序还提供模块的`HTTP API`服务（因为它通过 Web 包包含了 HttpApi 包）.
- B)仅将模块作为`微服务`的应用程序；
    - 添加对`HttpApi`和`Application`包的引用.
    - 根据需要添加对`EF Core`或`MongoDB`包的引用.
    - 效果:
        - 应用程序`不显示模块的UI`，因为它没有对 Web 包的引用.
        - 它在同一进程中承载`应用层`和`领域层`（这就是为什么它需要引用数据库集成包）.
        - 此应用程序提供了模块的`HTTP API`(它的主要目标).
- C)作为应用程序 A 或 B 托管的`远程服务应用程序`(显示模块UI但不托管应用程序的)；
    - 添加对`Web`和`HttpApi.Client`包的引用.
    - 配置 `HttpApi.Client` 包的远程端点.
    - 效果:
        - 应用程序可以显示`模块的UI`.
        - 它`不在同一进程`中承载模块的应用程序层和领域层.而是将其用作`远程服务`.
        - 此应用程序还提供模块的`HTTP API`服务（因为它通过 Web 包包含了 HttpApi 包）.
- D)`客户端应用程序`（或微服务），它只是将模块用作`远程服务`（由应用程序 A、B 或 C 托管）；
    - 添加对`HttpApi.Client`包的引用.
    - 配置 `HttpApi.Client` 包的远程端点.
    - 效果:
        - 应用程序可以将模块的所有功能用作`远程客户端`.
        - 应用程序只是一个客户端，`不能提供`模块的`HTTP API`服务.
        - 应用程序只是一个客户端，`不能显示模块的UI`.
- E ) `代理应用程序`，它托管模块的 `HTTP API`，但只是将所有请求转发到另一个应用程序（由应用程序 A、B 或 C 托管）；
    - 添加对`HttpApi`和`HttpApi.Client`包的引用.
    - 配置 `HttpApi.Client` 包的远程端点.
    - 效果:
        - 应用程序可以将模块的所有功能用作`远程客户端`.
        - 应用程序还提供模块的`HTTP API`服务，但实际上就像一个代理一样，将所有请求（针对模块）重定向到另一个远程服务器.

下一节将更详细地介绍这些包.

## 领域层
- **推荐** 将领域层分为两个项目：
    - `Domain.Shared`包，命名为CompanyName.ModuleName.Domain.Shared，其中包含常量、枚举和其他类型，这些是可以与模块的所有层安全共享的类型.它不能包含实体、存储库、领域服务或任何其他业务对象.此包也可以共享给 第三方 客户端.
    - `Domain`包，命名为CompanyName.ModuleName.Domain，包含实体、存储库接口、领域服务接口及其实现和其他领域对象.
        - Domain包依赖于`Domain.Shared`包.
## 应用层
- **推荐** 将应用层分成两个项目：
    - `Application.Contracts`包，命名为CompanyName.ModuleName.Application.Contracts，包含应用服务接口和相关数据传输对象.
        - Application.Contracts包依赖于`Domain.Shared`包.
    - `Application`包，命名为CompanyName.ModuleName.Application，其中包含应用程序服务实现.
        - Application包依赖于`Domain`和`Application.Contracts`包.

## 基础设施层
- **推荐** 为每个orm/数据库集成创建一个独立的集成包, 比如Entity Framework Core 和 MongoDB.
    - **推荐** 例如，创建一个抽象Entity Framework Core集成的CompanyName.ModuleName.EntityFrameworkCore 包. 
        - ORM 集成包依赖于`Domain`包.
    - **不推荐**  ORM/数据库集成包依赖`其他层`.
- 为每个主要库创建一个单独的集成包，单独的集成包可以互相替换而不影响其他包.

## HTTP层
- **推荐** 创建`HTTP API`包，命名为CompanyName.ModuleName.HttpApi，为模块开发REST风格的HTTP API.
    - HTTP API 包仅依赖于`Application.Contracts`包. `不依赖于Application包`.
    -**推荐** 为每个应用服务创建一个Controller (通常通过实现其接口). 这些控制器使用应用服务接口来委托操作. 它根据需要配置路由, HTTP方法和其他与Web相关的东西.
- **推荐** 创建一个为HTTP API包提供客户端服务的`HTTP API Client`包, 命名为Companyname.ModuleName.HttpApi.Client. 这些客户端服务为应用服务接口实现远程端点的客户端.
    - HTTP API Client 包仅依赖于`Application.Contracts`包.
    - **推荐** 使用ABP框架提供的动态代理HTTP C＃客户端的功能.
## Web 层(不需要)
- 推荐 创建命名为CompanyName.ModuleName.Web的 Web包. 包含页面,视图,脚本,样式,图像和其他UI组件.
    - Web 包仅依赖于HttpApi包.