# 模块化开发最佳实践和规范
## 简介
本文档为那些想要开发满足以下规范的`模块`的人员描述了`最佳实践和规范`：

- 开发符合`领域驱动设计`模式和最佳实践的模块。
- 开发具有`DBMS 和 ORM 独立性`的模块。
- 开发可用作`远程服务/微服务`以及与`单体`应用程序兼容的模块。

此外，本指南主要用于一般应用程序开发。

## 指南
- 整体
    - [模块架构](module-architecture.md)
- 领域层
    - [实体](entities.md)
    - [存储](repositories.md)
    - [领域服务](domain-services.md)
- 应用层
    - [应用服务](application-services.md)
    - [数据传输对象](data-transfer-objects.md)
- 数据访问
    - [Entity Framework Core 集成](entity-framework-core-integration.md)
    - [MongoDB 集成](mongodb-integration.md)

## 参考
- 电子书：[实现领域驱动设计](https://abp.io/books/implementing-domain-driven-design)