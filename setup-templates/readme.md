# 启动模板
虽然你可以从一个空项目开始并手动添加所需的包,但启动模板可以非常轻松,舒适地使用ABP框架启动新的解决方案.

单击下面列表中的名称以查看相关启动模板的文档:

- [app](application.md): 应用程序模板.
- [module](module.md): 模块/服务模板.
- [console](console.md): 控制台模板.
- [WPF](wpf.md): WPF模板.