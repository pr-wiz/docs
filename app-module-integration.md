# 应用集成模块
应用为部署模块的宿主程序, Abp模块应部署到应用中提供服务, 根据应用开发的层次, 可以分为两种应用
1. 多层应用, 按Abp标准分层创建的应用, 建议使用, 支持应用级别的标准定制开发, 包含以下层:
    - xxx.Domain.Shared
    - xxx.Domain
    - xxx.Application.Contracts
    - xxx.Application
    - xxx.EntityFrameworkCore
    - xxx.HttpApi
    - xxx.HttpApi.Client
    - xxx.HttpApi.Host
    - xxx.DbMigrator
1. 单层应用, 一般用于部署微服务, 不支持应用级别的定制开发, 只包含Web应用层

以下集成模块特指集成模块到多层应用中

## 集成方法
假定:
- 应用: Wiz.Portal
- 要集成的模块: Wiz.OptimizationManagement, 版本为 0.3.0
### 非插件式集成
1. 引用模块
    - 有两种方法
        - 在Wiz.Portal应用的解决方案所在目录下执行以下命令
            ```
            abphelper module add Wiz.OptimizationManagement -v 0.3.0 -soalch
            abphelper module add Wiz.OptimizationManagement -v 0.3.0 -e
            ```
            - 在执行 abphelper module add Wiz.OptimizationManagement -v 0.3.0 -e 时会报错, 可以忽略错误
        - 另一种方法通过代码生成工具在UI界面上操作实现(略)    
1. 生成集成模块的数据架构迁移
    - 在 Wiz.Portal.EntityFrameworkCore 中 EntityFrameworkCore 目录下的 PortalDbContext 类中添加 OptimizationManagement 的迁移数据上下文
        ```
        builder.ConfigureOptimizationManagement();
        ```
    - 生成 OptimizationManagement 模块的数据架构迁移
        - 在VS中设置启动项目为 Wiz.Portal.DbMigrator
        - 在PMC(程序包管理器控制台)
            - 选择默认项目为 Wiz.Portal.EntityFrameworkCore
            - 执行添加迁移命令
                ```
                Add-Migration Add_OptimizationManagement_Module
                ```
                - Add_OptimizationManagement_Module 为迁移的名称
    - 更新发布环境和数据库
        1. 代码推送并合并构建分支(dev), 并同步本地dev        
        1. 更新数据库(在dev分支下进行)
            - 确保 Wiz.Portal.DbMigrator 的 appsettings.json中配置的数据库为要更新的数据库
            - 运行 Wiz.Portal.DbMigrator (调试=>调动新实例)
        1. 在jenkins上发布应用到需要的环境
1. 依赖模块的领域模型发生变化后更新
    - 确认依赖模块已发布新的nuget包
    - 刷新应用依赖的模块nuget缓存(以引用更新后的模块)
        - 在vs中清理解决方案
        - 执行应用解决方案目录下的 restore_wiz_packages.bat
        - 在vs中生成应用
    - 生成数据架构迁移
        - 在VS中设置启动项目为 Wiz.Portal.DbMigrator
        - 在PMC(程序包管理器控制台)
            - 选择默认项目为 Wiz.Portal.EntityFrameworkCore
            - 执行添加迁移命令
                ```
                Add-Migration Update_OptimizationManagement_xxx
                ```
                - Update_OptimizationManagement_xxx 为迁移的名称, 根据实际情况编写, xxx一般为更新的内容, 如果比较多, 则可使用当前时间(到分钟,如:202204141210)
    - 更新发布环境和数据库
        1. 代码推送并合并构建分支(dev), 并同步本地dev        
        1. 更新数据库(在dev分支下进行)
            - 确保 Wiz.Portal.DbMigrator 的 appsettings.json中配置的数据库为要更新的数据库
            - 运行 Wiz.Portal.DbMigrator (调试=>调动新实例)
        1. 在jenkins上发布应用到需要的环境
### 插件式集成
略